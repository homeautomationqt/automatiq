cmake_minimum_required(VERSION 3.3.2)

project(Automatiq)

include(FeatureSummary)
find_package(ECM 5.14.0 REQUIRED NO_MODULE)
set_package_properties(ECM PROPERTIES TYPE REQUIRED DESCRIPTION "Extra CMake Modules." URL "https://projects.kde.org/projects/kdesupport/extra-cmake-modules")
feature_summary(WHAT REQUIRED_PACKAGES_NOT_FOUND FATAL_ON_MISSING_REQUIRED_PACKAGES)

set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR})

include(KDEInstallDirs)
include(KDEFrameworkCompilerSettings)
include(KDECMakeSettings)

include(GenerateExportHeader)

set(REQUIRED_QT_VERSION "5.6.0")
find_package(Qt5 ${REQUIRED_QT_VERSION} CONFIG REQUIRED Core Network WebSockets)

find_package(Qt5Test ${REQUIRED_QT_VERSION} CONFIG)
find_package(Qt5Quick ${REQUIRED_QT_VERSION} CONFIG)
find_package(Qt5Widgets ${REQUIRED_QT_VERSION} CONFIG)
find_package(Qt5Qml ${REQUIRED_QT_VERSION} CONFIG)

include(ECMSetupVersion)
include(ECMGenerateHeaders)

ecm_setup_version(0.1.0 VARIABLE_PREFIX AUTOMATIQ
                        VERSION_HEADER "${CMAKE_CURRENT_BINARY_DIR}/automatiq_version.h"
                        PACKAGE_VERSION_FILE "${CMAKE_CURRENT_BINARY_DIR}/AutomatiqConfigVersion.cmake")
 
add_subdirectory(src)
add_subdirectory(tests)
add_subdirectory(autotests)

# create a Config.cmake and a ConfigVersion.cmake file and install them
set(CMAKECONFIG_INSTALL_DIR "${KDE_INSTALL_CMAKEPACKAGEDIR}/Automatiq")

include(CMakePackageConfigHelpers)

configure_package_config_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/AutomatiqConfig.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/AutomatiqConfig.cmake"
    INSTALL_DESTINATION ${CMAKECONFIG_INSTALL_DIR}
)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/automatiq_version.h
        DESTINATION ${KDE_INSTALL_INCLUDEDIR_KF5}
        COMPONENT Devel)

install(FILES
            "${CMAKE_CURRENT_BINARY_DIR}/AutomatiqConfig.cmake"
            "${CMAKE_CURRENT_BINARY_DIR}/AutomatiqConfigVersion.cmake"
        DESTINATION "${CMAKECONFIG_INSTALL_DIR}"
        COMPONENT Devel)

install(EXPORT AutomatiqTargets
        DESTINATION "${CMAKECONFIG_INSTALL_DIR}"
        FILE AutomatiqTargets.cmake
        NAMESPACE KF5::)

feature_summary(WHAT ALL   FATAL_ON_MISSING_REQUIRED_PACKAGES)
