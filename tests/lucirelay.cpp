/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "lucirelay.h"

#include <QtCore/QPointer>
#include <QtWebSockets/QWebSocketServer>
#include <QtCore/QDebug>

class LuciRelayPrivate
{
public:

    QPointer<QWebSocketServer> mSocketServer;
};

LuciRelay::LuciRelay(QObject *parent) : AutomatiqProgramObject(parent), d(new LuciRelayPrivate)
{
    d->mSocketServer = new QWebSocketServer(QStringLiteral("light"), QWebSocketServer::NonSecureMode);

    connect(d->mSocketServer.data(), &QWebSocketServer::acceptError, this, &LuciRelay::acceptError);
    connect(d->mSocketServer.data(), &QWebSocketServer::closed, this, &LuciRelay::closed);
    connect(d->mSocketServer.data(), &QWebSocketServer::newConnection, this, &LuciRelay::newConnection);
    connect(d->mSocketServer.data(), &QWebSocketServer::originAuthenticationRequired, this, &LuciRelay::originAuthenticationRequired);
    connect(d->mSocketServer.data(), &QWebSocketServer::peerVerifyError, this, &LuciRelay::peerVerifyError);
    connect(d->mSocketServer.data(), &QWebSocketServer::serverError, this, &LuciRelay::serverError);
    connect(d->mSocketServer.data(), &QWebSocketServer::sslErrors, this, &LuciRelay::sslErrors);

    d->mSocketServer->listen(QHostAddress::Any, 22443);
}

LuciRelay::~LuciRelay()
{
}

void LuciRelay::run()
{
    qDebug() << "LuciRelay::run";
}

void LuciRelay::acceptError(QAbstractSocket::SocketError socketError)
{
    Q_UNUSED(socketError);

    qDebug() << "LuciRelay::acceptError";
}

void LuciRelay::closed()
{
    qDebug() << "LuciRelay::closed";
}

void LuciRelay::newConnection()
{
    qDebug() << "LuciRelay::newConnection";
}

void LuciRelay::originAuthenticationRequired(QWebSocketCorsAuthenticator *authenticator)
{
    authenticator->setAllowed(true);
}

void LuciRelay::peerVerifyError(const QSslError &error)
{
    Q_UNUSED(error);
    qDebug() << "LuciRelay::peerVerifyError";
}

void LuciRelay::serverError(QWebSocketProtocol::CloseCode closeCode)
{
    Q_UNUSED(closeCode);
    qDebug() << "LuciRelay::serverError" << closeCode << d->mSocketServer->errorString();
}

void LuciRelay::sslErrors(const QList<QSslError> &errors)
{
    Q_UNUSED(errors);
    qDebug() << "LuciRelay::sslErrors";
}


#include "moc_lucirelay.cpp"
