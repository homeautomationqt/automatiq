/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef AUTOMATIQ_SINGLEINPUTOBJECT_H
#define AUTOMATIQ_SINGLEINPUTOBJECT_H

#include "automatiqprogramobject.h"

class SingleInputObject : public AutomatiqProgramObject
{

    Q_OBJECT

    Q_PROPERTY(InterfaceVariable<int> input
               READ input
               WRITE setInput
               NOTIFY inputChanged)

public:
    explicit SingleInputObject(QObject *parent = 0);

    virtual ~SingleInputObject();

    void run() override;

    InterfaceVariable<int>& input();

    void setInput(const InterfaceVariable<int> &inputVariable);

Q_SIGNALS:

    void inputChanged();

private:

    InterfaceVariable<int> mInput;

};

#endif // AUTOMATIQ_SINGLEINPUTOBJECT_H
