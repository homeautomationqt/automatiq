/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

import QtQuick 2.4
import QtQuick.Extras 1.4
import QtQuick.Controls 1.4
import QtWebSockets 1.0

ApplicationWindow {
    id: mainApp

    visible: true
    minimumWidth: 800
    minimumHeight: 400
    title: 'light'

    ToggleButton {
        anchors.centerIn: parent
        width: 100
        height: 100

        onCheckedChanged:
        {

        }

        WebSocket {
            id: connectionToServer
            url: 'ws://localhost:22443'
            active: true

            onStatusChanged: {
                console.log(status)
                console.log(errorString)
            }
        }
    }
}
