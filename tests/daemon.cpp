/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "testautomatiqprogramobject.h"

#include "automatiqprogrammanager.h"
#include "automatiqprogram.h"
#include "automatiqprogramrunner.h"
#include "automatiqprogramobject.h"

#include "singleoutputobject.h"
#include "singleinputobject.h"
#include "lucirelay.h"

#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlEngine>
#include <QtQml/QQmlFileSelector>
#include <QtQml>

#include <QtCore/QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication myApp(argc, argv);

    qmlRegisterType<TestAutomatiqProgramObject>("org.mgallien.QmlExtension.Test", 1, 0, "TestAutomatiqProgramObject");
    qmlRegisterType<SingleOutputObject>("org.mgallien.QmlExtension.Test", 1, 0, "SingleOutputObject");
    qmlRegisterType<SingleInputObject>("org.mgallien.QmlExtension.Test", 1, 0, "SingleInputObject");
    qmlRegisterType<LuciRelay>("org.mgallien.QmlExtension.Test", 1, 0, "LuciRelay");
    qRegisterMetaType<InterfaceVariable<int> >();
    qRegisterMetaType<InterfaceVariable<double> >();
    qRegisterMetaType<InterfaceVariable<bool> >();

    qmlRegisterType<AutomatiqProgramManager>("org.mgallien.QmlExtension", 1, 0, "AutomatiqProgramManager");
    qmlRegisterType<AutomatiqProgramRunner>("org.mgallien.QmlExtension", 1, 0, "AutomatiqProgramRunner");
    qmlRegisterType<AutomatiqProgram>("org.mgallien.QmlExtension", 1, 0, "AutomatiqProgram");
    qmlRegisterType<AutomatiqProgramObject>("org.mgallien.QmlExtension", 1, 0, "AutomatiqProgramObject");

    QQmlApplicationEngine engine;
    QQmlFileSelector selector(&engine);

    engine.load(QUrl(QStringLiteral("./testProgram.qml")));

    return myApp.exec();;
}
