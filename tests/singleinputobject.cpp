/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "singleinputobject.h"

#include <QtCore/QDebug>
#include <QtCore/QDateTime>

#include <functional>

SingleInputObject::SingleInputObject(QObject *parent)
    : AutomatiqProgramObject({&mInput}, parent),
      mInput(QStringLiteral("input"))
{
}

SingleInputObject::~SingleInputObject()
{
}

void SingleInputObject::run()
{
    qDebug() << QDateTime::currentDateTime().toString(QStringLiteral("hh:mm:ss.zzz")) << "SingleInputObject::run" << mInput.data();
}

InterfaceVariable<int> &SingleInputObject::input()
{
    return mInput;
}

void SingleInputObject::setInput(const InterfaceVariable<int> &inputVariable)
{
    mInput = inputVariable;
    Q_EMIT inputChanged();
}

#include "moc_singleinputobject.cpp"
