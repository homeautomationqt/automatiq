/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef AUTOMATIQ_SINGLE_OUTPUT_OBJECT_H
#define AUTOMATIQ_SINGLE_OUTPUT_OBJECT_H

#include "automatiqprogramobject.h"

class SingleOutputObject : public AutomatiqProgramObject
{

    Q_OBJECT

    Q_PROPERTY(InterfaceVariable<int> output
               READ output
               NOTIFY outputChanged)

public:
    explicit SingleOutputObject(QObject *parent = 0);

    SingleOutputObject(const QString &objectName, const QString &variableName);

    const InterfaceVariable<int>& output() const;

    virtual ~SingleOutputObject();

    void run() override;

Q_SIGNALS:

    void outputChanged();

private:

    InterfaceVariable<int> mOutput;

    VariableHolder<int> mData;

};

#endif // AUTOMATIQ_SINGLEOutputOBJECT_H
