/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "peermanager.h"
#include "protocolmanager.h"
#include "synchronizationmanager.h"
#include "synchronizationprotocol.h"
#include "peerprotocol.h"

#include <QtNetwork/QTcpServer>

#include <QtTest/QtTest>

class AutomatiqClientTests: public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void connectAndSynchronize();

    void connectAndSynchronizeAndDisconnect();
};

void AutomatiqClientTests::connectAndSynchronize()
{
    qRegisterMetaType<SynchronizationProtocol*>("SynchronizationProtocol*");
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");
    qRegisterMetaType<Automatiq::DataProcessingSteps>("Automatiq::DataProcessingSteps");

    PeerManager myServerPeerManager1;
    ProtocolManager myServerProtocolManager1(QStringLiteral("TestServer1"));
    SynchronizationManager myServerSynchronizationManager1(QStringLiteral("TestServer1"));

    QObject::connect(&myServerPeerManager1, &PeerManager::newPeer, &myServerProtocolManager1, &ProtocolManager::handleNewConnection);
    QObject::connect(&myServerProtocolManager1, &ProtocolManager::newInitializedProtocol, &myServerSynchronizationManager1, &SynchronizationManager::newConnectedProtocol);

    QTcpServer myServer;

    myServer.listen(QHostAddress::Any, 4243);

    myServerPeerManager1.setServer(&myServer);
    QObject::connect(&myServer, &QTcpServer::newConnection, &myServerPeerManager1, &PeerManager::newPeerHasConnected);

    PeerManager myServerPeerManager2;
    ProtocolManager myServerProtocolManager2(QStringLiteral("TestServer2"));
    SynchronizationManager myServerSynchronizationManager2(QStringLiteral("TestServer2"));

    QObject::connect(&myServerPeerManager2, &PeerManager::newPeer, &myServerProtocolManager2, &ProtocolManager::handleNewConnection);
    QObject::connect(&myServerProtocolManager2, &ProtocolManager::newInitializedProtocol, &myServerSynchronizationManager2, &SynchronizationManager::newConnectedProtocol);

    QSignalSpy myNewProtocolSignalSpy(&myServerProtocolManager2, SIGNAL(newInitializedProtocol(Automatiq::peerId,QSharedPointer<AbstractPeerProtocol>)));
    QSignalSpy myNewSynchronizationDoneSignalSpy(&myServerSynchronizationManager2, SIGNAL(synchronizationDone(Automatiq::DataProcessingSteps)));

    myServerPeerManager2.connectToPeer(QStringLiteral("server1"), QHostAddress::LocalHost, 4243);

    myNewProtocolSignalSpy.wait();

    QVERIFY(myNewProtocolSignalSpy.isValid());
    QVERIFY(myNewProtocolSignalSpy.size() == 1);

    myServerSynchronizationManager1.requestSynchronization(Automatiq::DataProcessingSteps::BeforeSynchronization);
    myServerSynchronizationManager2.requestSynchronization(Automatiq::DataProcessingSteps::BeforeSynchronization);

    myNewSynchronizationDoneSignalSpy.wait();

    QVERIFY(myNewSynchronizationDoneSignalSpy.isValid());
    QVERIFY(myNewSynchronizationDoneSignalSpy.size() == 1);
}

void AutomatiqClientTests::connectAndSynchronizeAndDisconnect()
{
    qRegisterMetaType<SynchronizationProtocol*>("SynchronizationProtocol*");
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");
    qRegisterMetaType<Automatiq::DataProcessingSteps>("Automatiq::DataProcessingSteps");

    PeerManager myServerPeerManager1;
    ProtocolManager myServerProtocolManager1(QStringLiteral("TestServer1"));
    SynchronizationManager myServerSynchronizationManager1(QStringLiteral("TestServer1"));

    QObject::connect(&myServerPeerManager1, &PeerManager::newPeer, &myServerProtocolManager1, &ProtocolManager::handleNewConnection);
    QObject::connect(&myServerProtocolManager1, &ProtocolManager::newInitializedProtocol, &myServerSynchronizationManager1, &SynchronizationManager::newConnectedProtocol);

    QTcpServer myServer;

    myServer.listen(QHostAddress::Any, 4243);

    myServerPeerManager1.setServer(&myServer);
    QObject::connect(&myServer, &QTcpServer::newConnection, &myServerPeerManager1, &PeerManager::newPeerHasConnected);

    PeerManager myServerPeerManager2;
    ProtocolManager myServerProtocolManager2(QStringLiteral("TestServer2"));
    SynchronizationManager myServerSynchronizationManager2(QStringLiteral("TestServer2"));

    QObject::connect(&myServerPeerManager2, &PeerManager::newPeer, &myServerProtocolManager2, &ProtocolManager::handleNewConnection);
    QObject::connect(&myServerProtocolManager2, &ProtocolManager::newInitializedProtocol, &myServerSynchronizationManager2, &SynchronizationManager::newConnectedProtocol);

    myServerPeerManager2.connectToPeer(QStringLiteral("server1"), QHostAddress::LocalHost, 4243);

    QSignalSpy myNewProtocolSignalSpy(&myServerProtocolManager2, SIGNAL(newInitializedProtocol(Automatiq::peerId,QSharedPointer<AbstractPeerProtocol>)));
    QSignalSpy myNewSynchronizationDoneSignalSpy1(&myServerSynchronizationManager1, SIGNAL(synchronizationDone(Automatiq::DataProcessingSteps)));
    QSignalSpy myNewSynchronizationDoneSignalSpy2(&myServerSynchronizationManager2, SIGNAL(synchronizationDone(Automatiq::DataProcessingSteps)));

    myNewProtocolSignalSpy.wait();

    QVERIFY(myNewProtocolSignalSpy.isValid());
    QVERIFY(myNewProtocolSignalSpy.size() == 1);

    myServerSynchronizationManager1.requestSynchronization(Automatiq::DataProcessingSteps::ProcessingData);
    myServerProtocolManager2.disconnectFromPeer(0);

    myNewSynchronizationDoneSignalSpy1.wait();

    QVERIFY(myNewSynchronizationDoneSignalSpy1.isValid());
    QVERIFY(myNewSynchronizationDoneSignalSpy1.size() == 1);

    myServerSynchronizationManager2.requestSynchronization(Automatiq::DataProcessingSteps::ProcessingData);

    myNewSynchronizationDoneSignalSpy2.wait();

    QVERIFY(myNewSynchronizationDoneSignalSpy2.isValid());
    QVERIFY(myNewSynchronizationDoneSignalSpy2.size() == 1);
}

QTEST_MAIN(AutomatiqClientTests)

#include "synchronizationtests.moc"
