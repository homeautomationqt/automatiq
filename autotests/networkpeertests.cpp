/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagehello.h"
#include "messagecontainer.h"

#include "networkpeer.h"

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

#include <QtTest/QtTest>

class NetworkPeerTests: public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void receiveMessageHello();

    void badReceiveMessageHelloV1();

    void badReceiveMessageHelloV2();

    void badReceiveMessageHelloV3();

    void receiveMessageTwoHello();

};

void NetworkPeerTests::receiveMessageHello()
{
    QTcpServer myServer;
    myServer.listen(QHostAddress::Any, 12345);

    QTcpSocket myClientSocket;
    myClientSocket.connectToHost(QHostAddress::LocalHost, 12345);
    myClientSocket.waitForConnected(-1);

    NetworkPeer myClientPeer;
    myClientPeer.setPeerSocket(&myClientSocket);

    qRegisterMetaType<QSharedPointer<MessageContainer> >("QSharedPointer<MessageContainer>");
    QSignalSpy signalSpyNewMessage(&myClientPeer, SIGNAL(receivedNewMessage(QSharedPointer<MessageContainer>)));
    QSignalSpy signalSpyNewError(&myClientPeer, SIGNAL(newReadError(QAbstractSocket::SocketError)));
    QSignalSpy signalSpyDisconnected(&myClientPeer, SIGNAL(peerIsDisconnected(QAbstractSocket::SocketError)));

    myServer.waitForNewConnection();
    QTcpSocket *myServerConnectedSocket = myServer.nextPendingConnection();

    QSharedPointer<MessageContainer> myMessageToSend(new MessageContainer);
    myMessageToSend->version() = 1;
    myMessageToSend->sequenceNumber() = 1;

    MessageHello myHello;
    myHello.setValid(true);
    myHello.setTimestamp(QDateTime::currentDateTime());
    myHello.peerName() = QStringLiteral("TestPeer");
    myMessageToSend->setSpecificMessage(myHello);

    NetworkPeer myServerPeer;
    myServerPeer.setPeerSocket(myServerConnectedSocket);
    myServerPeer.sendMessage(myMessageToSend);
    myServerConnectedSocket->flush();

    myClientSocket.waitForReadyRead(-1);
    QVERIFY(myMessageToSend->length() >= myClientSocket.bytesAvailable());

    QVERIFY(signalSpyNewMessage.count() == 1);
    QVERIFY(signalSpyNewError.count() == 0);
    QVERIFY(signalSpyDisconnected.count() == 0);

    auto listArgs = signalSpyNewMessage.at(0);

    QSharedPointer<MessageContainer> newMessageFromSignal = listArgs.at(0).value<QSharedPointer<MessageContainer> >();
    QVERIFY(newMessageFromSignal);
    QVERIFY(newMessageFromSignal->protocolIdentifier() == myHello.protocolId);
    auto messageFromSignal(newMessageFromSignal->specificMessage<MessageHello>());
    QVERIFY(*messageFromSignal == myHello);

    delete messageFromSignal;
}

void NetworkPeerTests::badReceiveMessageHelloV1()
{
    QTcpServer myServer;
    myServer.listen(QHostAddress::Any, 12345);

    QTcpSocket myClientSocket;
    myClientSocket.connectToHost(QHostAddress::LocalHost, 12345);
    myClientSocket.waitForConnected(-1);

    NetworkPeer myClientPeer;
    myClientPeer.setPeerSocket(&myClientSocket);

    QSignalSpy signalSpyNewMessage(&myClientPeer, SIGNAL(receivedNewMessage(QSharedPointer<MessageContainer>)));
    QSignalSpy signalSpyNewError(&myClientPeer, SIGNAL(newReadError(QAbstractSocket::SocketError)));
    QSignalSpy signalSpyDisconnected(&myClientPeer, SIGNAL(peerIsDisconnected(QAbstractSocket::SocketError)));

    myServer.waitForNewConnection();
    QTcpSocket *myServerConnectedSocket = myServer.nextPendingConnection();
    QDataStream myServerStream(myServerConnectedSocket);

    quint32 messageLength = 30, messageVersion = 1;
    quint64 messageIdentifier = 1, messageSequenceNumber = 1;

    myServerStream << messageLength << messageVersion << messageSequenceNumber << messageIdentifier;
    myServerConnectedSocket->flush();

    myClientSocket.waitForReadyRead(-1);

    myServerConnectedSocket->disconnectFromHost();
    if (myServerConnectedSocket->state() != QAbstractSocket::UnconnectedState) {
        myServerConnectedSocket->waitForDisconnected();
    }
    myServerConnectedSocket->close();

    myClientSocket.waitForDisconnected();

    QVERIFY(signalSpyNewMessage.count() == 0);
    QVERIFY(signalSpyNewError.count() == 1);
    QVERIFY(signalSpyDisconnected.count() == 1);
}

void NetworkPeerTests::badReceiveMessageHelloV2()
{
    QTcpServer myServer;
    myServer.listen(QHostAddress::Any, 12345);

    QTcpSocket myClientSocket;
    myClientSocket.connectToHost(QHostAddress::LocalHost, 12345);
    myClientSocket.waitForConnected(-1);

    NetworkPeer myClientPeer;
    myClientPeer.setPeerSocket(&myClientSocket);

    QSignalSpy signalSpyNewMessage(&myClientPeer, SIGNAL(receivedNewMessage(QSharedPointer<MessageContainer>)));
    QSignalSpy signalSpyNewError(&myClientPeer, SIGNAL(newReadError(QAbstractSocket::SocketError)));
    QSignalSpy signalSpyDisconnected(&myClientPeer, SIGNAL(peerIsDisconnected(QAbstractSocket::SocketError)));

    myServer.waitForNewConnection();
    QTcpSocket *myServerConnectedSocket = myServer.nextPendingConnection();
    QDataStream myServerStream(myServerConnectedSocket);

    quint32 messageLength = 28, messageVersion = 1, specificLength = 0;
    quint64 messageIdentifier = 1, protocolIdentifier = 1, messageSequenceNumber = 1;

    myServerStream << messageLength << messageVersion << messageSequenceNumber << messageIdentifier << protocolIdentifier << specificLength;
    myServerConnectedSocket->flush();

    myClientSocket.waitForReadyRead(-1);

    myServerConnectedSocket->disconnectFromHost();
    if (myServerConnectedSocket->state() != QAbstractSocket::UnconnectedState) {
        myServerConnectedSocket->waitForDisconnected();
    }
    myServerConnectedSocket->close();

    myClientSocket.waitForDisconnected();

    QVERIFY(signalSpyNewMessage.count() == 0);
    QVERIFY(signalSpyNewError.count() == 1);
    QVERIFY(signalSpyDisconnected.count() == 1);
}

void NetworkPeerTests::badReceiveMessageHelloV3()
{
    QTcpServer myServer;
    myServer.listen(QHostAddress::Any, 12345);

    QTcpSocket myClientSocket;
    myClientSocket.connectToHost(QHostAddress::LocalHost, 12345);
    myClientSocket.waitForConnected(-1);

    NetworkPeer myClientPeer;
    myClientPeer.setPeerSocket(&myClientSocket);

    QSignalSpy signalSpyNewMessage(&myClientPeer, SIGNAL(receivedNewMessage(QSharedPointer<MessageContainer>)));
    QSignalSpy signalSpyNewError(&myClientPeer, SIGNAL(newReadError(QAbstractSocket::SocketError)));
    QSignalSpy signalSpyDisconnected(&myClientPeer, SIGNAL(peerIsDisconnected(QAbstractSocket::SocketError)));

    myServer.waitForNewConnection();
    QTcpSocket *myServerConnectedSocket = myServer.nextPendingConnection();
    QDataStream myServerStream(myServerConnectedSocket);

    quint32 messageLength = 24, messageVersion = 1, specificLength = 0;
    quint64 messageIdentifier = 1, protocolIdentifier = 1, messageSequenceNumber = 1;

    myServerStream << messageLength << messageVersion << messageSequenceNumber << messageIdentifier << protocolIdentifier << specificLength;
    myServerStream << quint32(59);
    myServerConnectedSocket->flush();

    myClientSocket.waitForReadyRead(-1);

    myServerConnectedSocket->disconnectFromHost();
    if (myServerConnectedSocket->state() != QAbstractSocket::UnconnectedState) {
        myServerConnectedSocket->waitForDisconnected();
    }
    myServerConnectedSocket->close();

    myClientSocket.waitForDisconnected();

    QVERIFY(signalSpyNewMessage.count() == 0);
    QVERIFY(signalSpyNewError.count() == 2);
    QVERIFY(signalSpyDisconnected.count() == 1);
}

void NetworkPeerTests::receiveMessageTwoHello()
{
    QTcpServer myServer;
    myServer.listen(QHostAddress::Any, 12345);

    QTcpSocket myClientSocket;
    myClientSocket.connectToHost(QHostAddress::LocalHost, 12345);
    myClientSocket.waitForConnected(-1);

    NetworkPeer myClientPeer;
    myClientPeer.setPeerSocket(&myClientSocket);

    qRegisterMetaType<QSharedPointer<MessageContainer> >("QSharedPointer<MessageContainer>");
    QSignalSpy signalSpyNewMessage(&myClientPeer, SIGNAL(receivedNewMessage(QSharedPointer<MessageContainer>)));
    QSignalSpy signalSpyNewError(&myClientPeer, SIGNAL(newReadError(QAbstractSocket::SocketError)));
    QSignalSpy signalSpyDisconnected(&myClientPeer, SIGNAL(peerIsDisconnected(QAbstractSocket::SocketError)));

    myServer.waitForNewConnection();
    QTcpSocket *myServerConnectedSocket = myServer.nextPendingConnection();

    QSharedPointer<MessageContainer> myFirstMessageToSend(new MessageContainer);
    myFirstMessageToSend->version() = 1;
    myFirstMessageToSend->sequenceNumber() = 1;

    MessageHello myFirstHello;
    myFirstHello.setValid(true);
    myFirstHello.setTimestamp(QDateTime::currentDateTime());
    myFirstHello.peerName() = QStringLiteral("TestFirstPeer");
    myFirstMessageToSend->setSpecificMessage(myFirstHello);

    QSharedPointer<MessageContainer> mySecondMessageToSend(new MessageContainer);
    mySecondMessageToSend->version() = 1;
    mySecondMessageToSend->sequenceNumber() = 1;

    MessageHello mySecondHello;
    mySecondHello.setValid(true);
    mySecondHello.setTimestamp(QDateTime::currentDateTime());
    mySecondHello.peerName() = QStringLiteral("TestSecondPeer");
    mySecondMessageToSend->setSpecificMessage(mySecondHello);

    NetworkPeer myServerPeer;
    myServerPeer.setPeerSocket(myServerConnectedSocket);
    myServerPeer.sendMessage(myFirstMessageToSend);
    myServerPeer.sendMessage(mySecondMessageToSend);
    myServerConnectedSocket->flush();

    myClientSocket.waitForReadyRead(-1);
    QVERIFY((myFirstMessageToSend->length() + mySecondMessageToSend->length()) >= myClientSocket.bytesAvailable());

    qDebug() << signalSpyNewMessage.count();
    QVERIFY(signalSpyNewMessage.count() == 2);
    QVERIFY(signalSpyNewError.count() == 0);
    QVERIFY(signalSpyDisconnected.count() == 0);

    auto firstListArgs = signalSpyNewMessage.at(0);

    QSharedPointer<MessageContainer> firstNewMessageFromSignal = firstListArgs.at(0).value<QSharedPointer<MessageContainer> >();
    QVERIFY(firstNewMessageFromSignal->protocolIdentifier() == myFirstHello.protocolId);
    auto firstMessageFromSignal(firstNewMessageFromSignal->specificMessage<MessageHello>());
    QVERIFY(*firstMessageFromSignal == myFirstHello);

    QVERIFY(signalSpyNewError.count() == 0);
    QVERIFY(signalSpyDisconnected.count() == 0);

    delete firstMessageFromSignal;

    auto secondListArgs = signalSpyNewMessage.at(1);

    QSharedPointer<MessageContainer> secondNewMessageFromSignal = secondListArgs.at(0).value<QSharedPointer<MessageContainer> >();
    QVERIFY(secondNewMessageFromSignal->protocolIdentifier() == mySecondHello.protocolId);
    auto secondMessageFromSignal(secondNewMessageFromSignal->specificMessage<MessageHello>());
    QVERIFY(*secondMessageFromSignal == mySecondHello);

    delete secondMessageFromSignal;
}

QTEST_MAIN(NetworkPeerTests)

#include "networkpeertests.moc"
