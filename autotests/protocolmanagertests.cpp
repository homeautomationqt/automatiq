/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "protocolmanager.h"
#include "peermanager.h"
#include "peerprotocol.h"
#include "networkpeer.h"

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

#include <QtCore/QScopedPointer>

#include <QtTest/QtTest>

class ProtocolManagerTests: public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void connectAndDisconnectToRemotePeer();
};

void ProtocolManagerTests::connectAndDisconnectToRemotePeer()
{
    QTcpServer myServer;
    myServer.listen(QHostAddress::Any, 12345);

    PeerManager myManager;

    ProtocolManager myServerProtocolManager(QStringLiteral("TestServer"));

    QObject::connect(&myManager, &PeerManager::newPeer, &myServerProtocolManager, &ProtocolManager::handleNewConnection);

    qRegisterMetaType<QSharedPointer<AbstractPeerProtocol> >("QSharedPointer<AbstractPeerProtocol>");
    qRegisterMetaType<PeerManager::ConnectionOrigin>("PeerManager::ConnectionOrigin");
    QSignalSpy myNewPeerSignalSpy(&myManager, SIGNAL(newPeer(QSharedPointer<NetworkPeer>, PeerManager::ConnectionOrigin)));
    QSignalSpy myNewProtocolSignalSpy(&myServerProtocolManager, SIGNAL(newConnectedProtocol(QSharedPointer<AbstractPeerProtocol>)));

    myManager.connectToPeer(QStringLiteral("server"), QHostAddress::LocalHost, 12345);

    myServer.waitForNewConnection();

    myNewPeerSignalSpy.wait();

    QVERIFY(myNewPeerSignalSpy.count() == 1);
    QVERIFY(myNewProtocolSignalSpy.count() == 1);

    auto newPeerArgs(myNewPeerSignalSpy.at(0));
    auto newPeer = newPeerArgs.at(0).value<QSharedPointer<NetworkPeer> >();
    QVERIFY(newPeer);

    QSignalSpy myPeerDestroySignalSpy(newPeer.data(), SIGNAL(destroyed(QObject*)));
    QSignalSpy myPeerReadErrorSignalSpy(newPeer.data(), SIGNAL(newReadError(QAbstractSocket::SocketError)));
    QSignalSpy myPeerDiconnectSignalSpy(newPeer.data(), SIGNAL(peerIsDisconnected(QAbstractSocket::SocketError)));

    auto newProtocolArgs(myNewProtocolSignalSpy.at(0));
    auto newProtocol = newProtocolArgs.at(0).value<QSharedPointer<AbstractPeerProtocol> >();
    QVERIFY(newProtocol);

    QSignalSpy myProtocolDestroySignalSpy(newProtocol.data(), SIGNAL(destroyed(QObject*)));

    auto clientSocket = myServer.nextPendingConnection();
    clientSocket->waitForConnected();

    myServer.close();
    clientSocket->close();

    myPeerReadErrorSignalSpy.wait();

    QVERIFY(myPeerReadErrorSignalSpy.count() == 1);
    QVERIFY(myPeerDiconnectSignalSpy.count() == 1);
    QVERIFY(myPeerDestroySignalSpy.count() == 0);
    QVERIFY(myProtocolDestroySignalSpy.count() == 0);

    newPeer.clear();
    newPeerArgs.clear();
    myNewPeerSignalSpy.clear();

    myPeerDestroySignalSpy.wait();


    newProtocol.clear();
    newProtocolArgs.clear();
    myNewProtocolSignalSpy.clear();

    myProtocolDestroySignalSpy.wait();

    qDebug() << myPeerDestroySignalSpy.count();
    QVERIFY(myPeerDestroySignalSpy.count() == 1);
    QVERIFY(myProtocolDestroySignalSpy.count() == 1);
}

QTEST_MAIN(ProtocolManagerTests)

#include "protocolmanagertests.moc"
