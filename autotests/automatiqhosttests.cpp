/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "automatiqclient.h"

#include "peermanager.h"
#include "protocolmanager.h"
#include "serverconfigmanager.h"
#include "peerprotocol.h"
#include "configprotocol.h"
#include "automatiqhost.h"
#include "subscribedvariablerecord.h"
#include "datamanager.h"
#include "runtimemanager.h"

#include <QtNetwork/QTcpServer>

#include <QtTest/QtTest>

class AutomatiqHostTests: public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void connectAndDestroy();

    void connectExchangeDisconnectAndReconnect();

    void connectExchangeDisconnectAndReconnectWithoutSubscribe();
};

void AutomatiqHostTests::connectAndDestroy()
{
    qRegisterMetaType<ConfigProtocol*>("ConfigProtocol*");
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");

    PeerManager myServerPeerManager;

    ProtocolManager myServerProtocolManager(QStringLiteral("TestServer"));

    ServerConfigManager myServerConfigurationManager(QStringLiteral("TestServer"));

    QObject::connect(&myServerPeerManager, &PeerManager::newPeer, &myServerProtocolManager, &ProtocolManager::handleNewConnection);

    QObject::connect(&myServerProtocolManager, &ProtocolManager::newInitializedProtocol, &myServerConfigurationManager, &ServerConfigManager::newConnectedProtocol);

    QTcpServer myServer;

    myServer.listen(QHostAddress::Any, 4243);

    myServerPeerManager.setServer(&myServer);
    QObject::connect(&myServer, &QTcpServer::newConnection, &myServerPeerManager, &PeerManager::newPeerHasConnected);

    AutomatiqClient myHost;
    myHost.setHostPeerName(QStringLiteral("TestClient"));
    myHost.initialize();
    myHost.connectToPeer(QStringLiteral("server"), QStringLiteral("127.0.0.1"), 4243);

    QSignalSpy mySpy(&myHost, SIGNAL(newConnectedPeer(const QString&, Automatiq::peerId)));

    mySpy.wait();

    QVERIFY(mySpy.count() == 1);
}

class TestObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString testVariable READ testVariable WRITE setTestVariable NOTIFY variableChanged())
public:
    explicit TestObject(QObject *parent = 0);

    const QString &testVariable() const;

    void setTestVariable(const QString &value);

Q_SIGNALS:

    void variableChanged(const QString &objectName, const char *propertyName, const QVariant &value);

public Q_SLOTS:

    void changeValue();

private:

    QString mTestProperty;
};

TestObject::TestObject(QObject *parent)
    : QObject(parent), mTestProperty(QStringLiteral("TestValue"))
{
    QTimer::singleShot(15000, this, SLOT(changeValue()));
}

const QString &TestObject::testVariable() const
{
    return mTestProperty;
}

void TestObject::setTestVariable(const QString &value)
{
    mTestProperty = value;
    Q_EMIT variableChanged(objectName(), "testVariable", mTestProperty);
}

void TestObject::changeValue()
{
    setTestVariable(QStringLiteral("newValue"));
}

void AutomatiqHostTests::connectExchangeDisconnectAndReconnect()
{
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");

    AutomatiqHost *host1 = new AutomatiqHost;
    host1->setHostPeerName(QStringLiteral("host1"));
    host1->initialize(QHostAddress::Any, 4567);

    RuntimeManager *hostRunTime1 = new RuntimeManager(host1);
    hostRunTime1->setHostPeerName(QStringLiteral("host1"));
    hostRunTime1->initialize();

    TestObject variableHost1;
    variableHost1.setObjectName(QStringLiteral("variableHost1"));
    hostRunTime1->registerVariable(&variableHost1, QStringLiteral("testVariable"));

    AutomatiqHost host2;
    host2.setHostPeerName(QStringLiteral("host2"));
    host2.initialize(QHostAddress::Any, 7654);

    RuntimeManager hostRunTime2(&host2);
    hostRunTime2.setHostPeerName(QStringLiteral("host2"));
    hostRunTime2.initialize();

    QSharedPointer<SubscribedVariableRecord> variableFromHost1(hostRunTime2.subscribeVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable")));

    QVERIFY(variableFromHost1->sourcePeerId() == Automatiq::INVALID_PEER_ID);

    QSignalSpy variableFromHost1PeerIdSignal(variableFromHost1.data(), SIGNAL(peerIdChanged(Automatiq::peerId)));
    QSignalSpy variableFromHost1SubscribedSignal(variableFromHost1.data(), SIGNAL(subscriptionChanged(bool)));
    QSignalSpy variableFromHost1ValueChangedSignal(variableFromHost1.data(), SIGNAL(valueChanged(const QString&, const QString&, const QVariant&)));

    host2.connectToPeer(QStringLiteral("host1"), QHostAddress::LocalHost, 4567);

    variableFromHost1PeerIdSignal.wait();

    QVERIFY(variableFromHost1->sourcePeerId() != Automatiq::INVALID_PEER_ID);
    QVERIFY(variableFromHost1PeerIdSignal.count() == 1);

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 1);

    const QVariant oldValue = variableFromHost1->value();

    variableFromHost1ValueChangedSignal.wait(17000);

    QVERIFY(variableFromHost1ValueChangedSignal.count() == 2);
    QVERIFY(variableFromHost1->value() != oldValue);

    delete host1;
    delete hostRunTime1;

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 2);

    variableFromHost1PeerIdSignal.wait();

    QVERIFY(variableFromHost1PeerIdSignal.count() == 2);

    host1 = new AutomatiqHost;
    host1->setHostPeerName(QStringLiteral("host1"));
    host1->initialize(QHostAddress::Any, 4567);

    hostRunTime1 = new RuntimeManager(host1);
    hostRunTime1->setHostPeerName(QStringLiteral("host1"));
    hostRunTime1->initialize();

    hostRunTime1->registerVariable(&variableHost1, QStringLiteral("testVariable"));

    QSignalSpy host2NewConnectionSignal(&host2, SIGNAL(newConnectedPeer(const QString&, Automatiq::peerId)));

    host2NewConnectionSignal.wait(10000);

    QVERIFY(host2NewConnectionSignal.count() == 1);

    variableFromHost1PeerIdSignal.wait(10000);

    QVERIFY(variableFromHost1PeerIdSignal.count() == 3);
}

void AutomatiqHostTests::connectExchangeDisconnectAndReconnectWithoutSubscribe()
{
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");

    AutomatiqHost *host1 = new AutomatiqHost;
    host1->setHostPeerName(QStringLiteral("host1"));
    host1->initialize(QHostAddress::Any, 4567);

    RuntimeManager *hostRunTime1 = new RuntimeManager(host1);
    hostRunTime1->setHostPeerName(QStringLiteral("host1"));
    hostRunTime1->initialize();

    TestObject variableHost1;
    variableHost1.setObjectName(QStringLiteral("variableHost1"));
    hostRunTime1->registerVariable(&variableHost1, QStringLiteral("testVariable"));

    AutomatiqHost host2;
    host2.setHostPeerName(QStringLiteral("host2"));
    host2.initialize(QHostAddress::Any, 7654);

    RuntimeManager hostRunTime2(&host2);
    hostRunTime2.setHostPeerName(QStringLiteral("host2"));
    hostRunTime2.initialize();

    QSignalSpy myNewVariableSignal(hostRunTime2.dataManager(), SIGNAL(rowsInserted(QModelIndex,int,int)));

    host2.connectToPeer(QStringLiteral("host1"), QHostAddress::LocalHost, 4567);

    myNewVariableSignal.wait();

    QVERIFY(myNewVariableSignal.count() == 1);

    QSharedPointer<SubscribedVariableRecord> variableFromHost1(hostRunTime2.dataManager()->getVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable")));

    QVERIFY(variableFromHost1 && variableFromHost1->sourcePeerId() != Automatiq::INVALID_PEER_ID);

    QSignalSpy variableFromHost1PeerIdSignal(variableFromHost1.data(), SIGNAL(peerIdChanged(Automatiq::peerId)));
    QSignalSpy variableFromHost1SubscribedSignal(variableFromHost1.data(), SIGNAL(subscriptionChanged(bool)));
    QSignalSpy variableFromHost1ValueChangedSignal(variableFromHost1.data(), SIGNAL(valueChanged(const QString&, const QString&, const QVariant&)));

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 0);

    const QVariant oldValue = variableFromHost1->value();

    variableFromHost1ValueChangedSignal.wait(17000);

    QVERIFY(variableFromHost1ValueChangedSignal.count() == 0);
    QVERIFY(variableFromHost1->value() == oldValue);

    delete host1;
    delete hostRunTime1;

    variableFromHost1SubscribedSignal.wait();

    qDebug() << variableFromHost1SubscribedSignal.count();
    QVERIFY(variableFromHost1SubscribedSignal.count() == 0);

    variableFromHost1PeerIdSignal.wait();

    QVERIFY(variableFromHost1PeerIdSignal.count() == 1);

    host1 = new AutomatiqHost;
    host1->setHostPeerName(QStringLiteral("host1"));
    host1->initialize(QHostAddress::Any, 4567);

    hostRunTime1 = new RuntimeManager(host1);
    hostRunTime1->setHostPeerName(QStringLiteral("host1"));
    hostRunTime1->initialize();

    hostRunTime1->registerVariable(&variableHost1, QStringLiteral("testVariable"));

    QSignalSpy host2NewConnectionSignal(&host2, SIGNAL(newConnectedPeer(const QString&, Automatiq::peerId)));

    host2NewConnectionSignal.wait(10000);

    QVERIFY(host2NewConnectionSignal.count() == 1);

    variableFromHost1PeerIdSignal.wait(10000);

    QVERIFY(variableFromHost1PeerIdSignal.count() == 2);
}

QTEST_MAIN(AutomatiqHostTests)

#include "automatiqhosttests.moc"
