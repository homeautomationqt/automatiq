/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "peermanager.h"
#include "protocolmanager.h"
#include "automatiqhost.h"
#include "runtimemanager.h"
#include "datamanager.h"
#include "dataprotocol.h"
#include "peerprotocol.h"
#include "subscribedvariablerecord.h"

#include <QtNetwork/QTcpServer>

#include <QtTest/QtTest>

class DataManagerTests: public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void connectExchangeAndDisconnect1();

    void connectExchangeAndDisconnect2();

    void connectExchangeAndDisconnect3();

    void connectExchangeAndDisconnect2Variables();

    void connectSubscribeUpdateUnsubscribeDisconnect();

    void connectSubscribePeriodicUpdateUnsubscribeDisconnect();

    void connectExchangeAndDisconnectWithoutSubscribe();
};

class TestObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString testVariable READ testVariable WRITE setTestVariable NOTIFY variableChanged())
public:
    explicit TestObject(QObject *parent = 0);

    const QString &testVariable() const;

    void setTestVariable(const QString &value);

Q_SIGNALS:

    void variableChanged(const QString &objectName, const char *propertyName, const QVariant &value);

public Q_SLOTS:

    void changeValue();

private:

    QString mTestProperty;
};

TestObject::TestObject(QObject *parent)
    : QObject(parent), mTestProperty(QStringLiteral("TestValue"))
{
    QTimer::singleShot(5000, this, SLOT(changeValue()));
}

const QString &TestObject::testVariable() const
{
    return mTestProperty;
}

void TestObject::setTestVariable(const QString &value)
{
    mTestProperty = value;
    Q_EMIT variableChanged(objectName(), "testVariable", mTestProperty);
}

void TestObject::changeValue()
{
    setTestVariable(QStringLiteral("newValue"));
}

void DataManagerTests::connectExchangeAndDisconnect1()
{
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");

    AutomatiqHost host1;
    host1.setHostPeerName(QStringLiteral("host1"));
    host1.initialize(QHostAddress::Any, 4567);

    RuntimeManager hostRunTime1(&host1);
    hostRunTime1.setHostPeerName(QStringLiteral("host1"));

    TestObject variableHost1;
    variableHost1.setObjectName(QStringLiteral("variableHost1"));
    hostRunTime1.registerVariable(&variableHost1, QStringLiteral("testVariable"));

    AutomatiqHost host2;
    host2.setHostPeerName(QStringLiteral("host1"));
    host2.initialize(QHostAddress::Any, 7654);

    RuntimeManager hostRunTime2(&host2);
    hostRunTime2.setHostPeerName(QStringLiteral("host2"));

    QSharedPointer<SubscribedVariableRecord> variableFromHost1(hostRunTime2.subscribeVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable")));

    QVERIFY(variableFromHost1->sourcePeerId() == Automatiq::INVALID_PEER_ID);

    QSignalSpy variableFromHost1PeerIdSignal(variableFromHost1.data(), SIGNAL(peerIdChanged(Automatiq::peerId)));
    QSignalSpy variableFromHost1SubscribedSignal(variableFromHost1.data(), SIGNAL(subscriptionChanged(bool)));

    host2.connectToPeer(QStringLiteral("host1"), QHostAddress::LocalHost, 4567);

    variableFromHost1PeerIdSignal.wait();

    QVERIFY(variableFromHost1->sourcePeerId() != Automatiq::INVALID_PEER_ID);
    QVERIFY(variableFromHost1PeerIdSignal.count() == 1);

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 1);

    host1.disconnectFromPeer(0);

    variableFromHost1PeerIdSignal.wait();

    QVERIFY(variableFromHost1PeerIdSignal.count() == 2);

    qDebug() << "end of test";
}

void DataManagerTests::connectExchangeAndDisconnect2()
{
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");

    AutomatiqHost host1;
    host1.setHostPeerName(QStringLiteral("host1"));
    host1.initialize(QHostAddress::Any, 4567);

    RuntimeManager hostRunTime1(&host1);
    hostRunTime1.setHostPeerName(QStringLiteral("host1"));

    TestObject variableHost1;
    variableHost1.setObjectName(QStringLiteral("variableHost1"));
    hostRunTime1.registerVariable(&variableHost1, QStringLiteral("testVariable"));

    AutomatiqHost host2;
    host2.setHostPeerName(QStringLiteral("host2"));
    host2.initialize(QHostAddress::Any, 7654);

    RuntimeManager hostRunTime2(&host2);
    hostRunTime2.setHostPeerName(QStringLiteral("host2"));

    TestObject variableHost2;
    variableHost2.setObjectName(QStringLiteral("variableHost2"));
    hostRunTime2.registerVariable(&variableHost2, QStringLiteral("testVariable"));

    host2.connectToPeer(QStringLiteral("host1"), QHostAddress::LocalHost, 4567);

    QSignalSpy host2ConnectSpy(&host2, SIGNAL(newConnectedPeer(const QString&, Automatiq::peerId)));

    host2ConnectSpy.wait();
    host2ConnectSpy.wait();

    QVERIFY(host2ConnectSpy.count() == 1);

    QSharedPointer<SubscribedVariableRecord> variableFromHost1(hostRunTime2.subscribeVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable")));

    QVERIFY(variableFromHost1->sourcePeerId() != Automatiq::INVALID_PEER_ID);

    QSignalSpy variableFromHost1PeerIdSignal(variableFromHost1.data(), SIGNAL(peerIdChanged(Automatiq::peerId)));
    QSignalSpy variableFromHost1SubscribedSignal(variableFromHost1.data(), SIGNAL(subscriptionChanged(bool)));

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 1);

    hostRunTime1.subscribeVariable(QStringLiteral("variableHost2"), QStringLiteral("testVariable"));

    hostRunTime2.unsubscribeVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable"));

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 2);

    host1.disconnectFromPeer(0);

    variableFromHost1PeerIdSignal.wait();

    QVERIFY(variableFromHost1PeerIdSignal.count() == 1);

    qDebug() << "end of test";
}

void DataManagerTests::connectExchangeAndDisconnect3()
{
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");

    AutomatiqHost *host1 = new AutomatiqHost;
    host1->setHostPeerName(QStringLiteral("host1"));
    host1->initialize(QHostAddress::Any, 4567);

    RuntimeManager *hostRunTime1 = new RuntimeManager(host1);
    hostRunTime1->setHostPeerName(QStringLiteral("host1"));

    TestObject variableHost1;
    variableHost1.setObjectName(QStringLiteral("variableHost1"));
    hostRunTime1->registerVariable(&variableHost1, QStringLiteral("testVariable"));

    AutomatiqHost host2;
    host2.setHostPeerName(QStringLiteral("host1"));
    host2.initialize(QHostAddress::Any, 4567);

    RuntimeManager hostRunTime2(&host2);
    host2.setHostPeerName(QStringLiteral("host2"));

    TestObject variableHost2;
    variableHost2.setObjectName(QStringLiteral("variableHost2"));
    hostRunTime2.registerVariable(&variableHost2, QStringLiteral("testVariable"));

    host2.connectToPeer(QStringLiteral("host1"), QHostAddress::LocalHost, 4567);

    QSignalSpy host2ConnectSpy(&host2, SIGNAL(newConnectedPeer(const QString&, Automatiq::peerId)));

    host2ConnectSpy.wait();
    host2ConnectSpy.wait();

    QVERIFY(host2ConnectSpy.count() == 1);

    QSharedPointer<SubscribedVariableRecord> variableFromHost1(hostRunTime2.subscribeVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable")));

    QVERIFY(variableFromHost1->sourcePeerId() != Automatiq::INVALID_PEER_ID);

    QSignalSpy variableFromHost1PeerIdSignal(variableFromHost1.data(), SIGNAL(peerIdChanged(Automatiq::peerId)));
    QSignalSpy variableFromHost1SubscribedSignal(variableFromHost1.data(), SIGNAL(subscriptionChanged(bool)));

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 1);

    hostRunTime1->subscribeVariable(QStringLiteral("variableHost2"), QStringLiteral("testVariable"));

    hostRunTime2.unsubscribeVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable"));

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 2);

    QSignalSpy modelChangedSignal(hostRunTime2.dataManager(), SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)));

    host1->disconnectFromPeer(0);

    delete host1;

    host1 = nullptr;

    variableFromHost1PeerIdSignal.wait();

    QVERIFY(variableFromHost1PeerIdSignal.count() == 1);

    modelChangedSignal.wait();
    qDebug() << modelChangedSignal.count();
    QVERIFY(modelChangedSignal.count() == 1);

    qDebug() << "end of test";
}

void DataManagerTests::connectExchangeAndDisconnect2Variables()
{
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");

    AutomatiqHost host1;
    host1.setHostPeerName(QStringLiteral("host1"));
    host1.initialize(QHostAddress::Any, 4567);

    RuntimeManager hostRunTime1(&host1);
    hostRunTime1.setHostPeerName(QStringLiteral("host1"));

    TestObject variable1Host1;
    variable1Host1.setObjectName(QStringLiteral("variable1Host1"));
    hostRunTime1.registerVariable(&variable1Host1, QStringLiteral("testVariable"));

    TestObject variable2Host1;
    variable2Host1.setObjectName(QStringLiteral("variable2Host1"));
    hostRunTime1.registerVariable(&variable2Host1, QStringLiteral("testVariable"));

    AutomatiqHost host2;
    host2.setHostPeerName(QStringLiteral("host2"));
    host2.initialize(QHostAddress::Any, 4567);

    RuntimeManager hostRunTime2(&host2);
    hostRunTime2.setHostPeerName(QStringLiteral("host2"));

    QSharedPointer<SubscribedVariableRecord> variable1FromHost1(hostRunTime2.subscribeVariable(QStringLiteral("variable1Host1"), QStringLiteral("testVariable")));

    QVERIFY(variable1FromHost1->sourcePeerId() == Automatiq::INVALID_PEER_ID);

    QSignalSpy variable1FromHost1PeerIdSignal(variable1FromHost1.data(), SIGNAL(peerIdChanged(Automatiq::peerId)));
    QSignalSpy variable1FromHost1SubscribedSignal(variable1FromHost1.data(), SIGNAL(subscriptionChanged(bool)));

    QSharedPointer<SubscribedVariableRecord> variable2FromHost1(hostRunTime2.subscribeVariable(QStringLiteral("variable2Host1"), QStringLiteral("testVariable")));

    QVERIFY(variable2FromHost1->sourcePeerId() == Automatiq::INVALID_PEER_ID);

    QVERIFY(variable2FromHost1.data() != variable1FromHost1.data());

    QSignalSpy variable2FromHost1PeerIdSignal(variable2FromHost1.data(), SIGNAL(peerIdChanged(Automatiq::peerId)));
    QSignalSpy variable2FromHost1SubscribedSignal(variable2FromHost1.data(), SIGNAL(subscriptionChanged(bool)));

    host2.connectToPeer(QStringLiteral("host1"), QHostAddress::LocalHost, 4567);

    variable1FromHost1PeerIdSignal.wait();

    QVERIFY(variable1FromHost1->sourcePeerId() != Automatiq::INVALID_PEER_ID);
    QVERIFY(variable1FromHost1PeerIdSignal.count() == 1);

    variable2FromHost1PeerIdSignal.wait();

    QVERIFY(variable2FromHost1->sourcePeerId() != Automatiq::INVALID_PEER_ID);
    QVERIFY(variable2FromHost1PeerIdSignal.count() == 1);

    variable1FromHost1SubscribedSignal.wait();

    QVERIFY(variable1FromHost1SubscribedSignal.count() == 1);

    variable2FromHost1SubscribedSignal.wait();

    QVERIFY(variable2FromHost1SubscribedSignal.count() == 1);

    host1.disconnectFromPeer(0);

    variable1FromHost1PeerIdSignal.wait();

    QVERIFY(variable1FromHost1PeerIdSignal.count() == 2);

    variable2FromHost1PeerIdSignal.wait();

    QVERIFY(variable2FromHost1PeerIdSignal.count() == 2);

    qDebug() << "end of test";
}

void DataManagerTests::connectSubscribeUpdateUnsubscribeDisconnect()
{
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");

    AutomatiqHost host1;
    host1.setHostPeerName(QStringLiteral("host1"));
    host1.initialize(QHostAddress::Any, 4567);

    RuntimeManager hostRunTime1(&host1);
    hostRunTime1.setHostPeerName(QStringLiteral("host1"));
    hostRunTime1.initialize();

    TestObject variableHost1;
    variableHost1.setObjectName(QStringLiteral("variableHost1"));
    hostRunTime1.registerVariable(&variableHost1, QStringLiteral("testVariable"));

    AutomatiqHost host2;
    host2.setHostPeerName(QStringLiteral("host2"));
    host2.initialize(QHostAddress::Any, 4567);

    RuntimeManager hostRunTime2(&host2);
    hostRunTime2.setHostPeerName(QStringLiteral("host2"));
    hostRunTime2.initialize();

    QSharedPointer<SubscribedVariableRecord> variableFromHost1(hostRunTime2.subscribeVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable")));

    QVERIFY(variableFromHost1->sourcePeerId() == Automatiq::INVALID_PEER_ID);

    QSignalSpy variableFromHost1PeerIdSignal(variableFromHost1.data(), SIGNAL(peerIdChanged(Automatiq::peerId)));
    QSignalSpy variableFromHost1SubscribedSignal(variableFromHost1.data(), SIGNAL(subscriptionChanged(bool)));
    QSignalSpy variableFromHost1ValueChangedSignal(variableFromHost1.data(), SIGNAL(valueChanged(const QString&, const QString&, const QVariant&)));

    host2.connectToPeer(QStringLiteral("host1"), QHostAddress::LocalHost, 4567);

    variableFromHost1PeerIdSignal.wait();

    QVERIFY(variableFromHost1->sourcePeerId() != Automatiq::INVALID_PEER_ID);
    QVERIFY(variableFromHost1PeerIdSignal.count() == 1);

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 1);

    const QVariant oldValue = variableFromHost1->value();

    QVERIFY(variableFromHost1ValueChangedSignal.count() == 1);

    variableFromHost1ValueChangedSignal.wait(16000);

    QVERIFY(variableFromHost1ValueChangedSignal.count() == 2);
    QVERIFY(variableFromHost1->value() != oldValue);

    hostRunTime2.unsubscribeVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable"));

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 2);

    host1.disconnectFromPeer(0);

    variableFromHost1PeerIdSignal.wait();

    QVERIFY(variableFromHost1PeerIdSignal.count() == 2);

    qDebug() << "end of test";
}

void DataManagerTests::connectSubscribePeriodicUpdateUnsubscribeDisconnect()
{
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");

    AutomatiqHost host1;
    host1.setHostPeerName(QStringLiteral("host1"));
    host1.initialize(QHostAddress::Any, 4567);

    RuntimeManager hostRunTime1(&host1);
    hostRunTime1.setHostPeerName(QStringLiteral("host1"));
    hostRunTime1.initialize();

    TestObject variableHost1;
    variableHost1.setObjectName(QStringLiteral("variableHost1"));
    hostRunTime1.registerVariable(&variableHost1, QStringLiteral("testVariable"));

    AutomatiqHost host2;
    host2.setHostPeerName(QStringLiteral("host2"));
    host2.initialize(QHostAddress::Any, 4567);

    RuntimeManager hostRunTime2(&host2);
    hostRunTime2.setHostPeerName(QStringLiteral("host2"));
    hostRunTime2.initialize();

    QSharedPointer<SubscribedVariableRecord> variableFromHost1(hostRunTime2.subscribeVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable")));

    QVERIFY(variableFromHost1->sourcePeerId() == Automatiq::INVALID_PEER_ID);

    QSignalSpy variableFromHost1PeerIdSignal(variableFromHost1.data(), SIGNAL(peerIdChanged(Automatiq::peerId)));
    QSignalSpy variableFromHost1SubscribedSignal(variableFromHost1.data(), SIGNAL(subscriptionChanged(bool)));
    QSignalSpy variableFromHost1ValueChangedSignal(variableFromHost1.data(), SIGNAL(valueChanged(const QString&, const QString&, const QVariant&)));

    host2.connectToPeer(QStringLiteral("host1"), QHostAddress::LocalHost, 4567);

    variableFromHost1PeerIdSignal.wait();

    QVERIFY(variableFromHost1->sourcePeerId() != Automatiq::INVALID_PEER_ID);
    QVERIFY(variableFromHost1PeerIdSignal.count() == 1);

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 1);

    const QVariant oldValue = variableFromHost1->value();

    variableFromHost1ValueChangedSignal.wait(7000);

    QVERIFY(variableFromHost1ValueChangedSignal.count() == 2);
    QVERIFY(variableFromHost1->value() != oldValue);

    hostRunTime2.unsubscribeVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable"));

    variableFromHost1SubscribedSignal.wait();

    QVERIFY(variableFromHost1SubscribedSignal.count() == 2);

    host1.disconnectFromPeer(0);

    variableFromHost1PeerIdSignal.wait();

    QVERIFY(variableFromHost1PeerIdSignal.count() == 2);

    qDebug() << "end of test";
}

void DataManagerTests::connectExchangeAndDisconnectWithoutSubscribe()
{
    qRegisterMetaType<Automatiq::peerId>("Automatiq::peerId");

    AutomatiqHost host1;
    host1.setHostPeerName(QStringLiteral("host1"));
    host1.initialize(QHostAddress::Any, 4567);

    RuntimeManager hostRunTime1(&host1);
    hostRunTime1.setHostPeerName(QStringLiteral("host1"));
    hostRunTime1.initialize();

    TestObject variableHost1;
    variableHost1.setObjectName(QStringLiteral("variableHost1"));
    hostRunTime1.registerVariable(&variableHost1, QStringLiteral("testVariable"));

    AutomatiqHost host2;
    host2.setHostPeerName(QStringLiteral("host2"));
    host2.initialize(QHostAddress::Any, 4567);

    RuntimeManager hostRunTime2(&host2);
    hostRunTime2.setHostPeerName(QStringLiteral("host2"));
    hostRunTime2.initialize();

    QSignalSpy myNewVariableSignal(hostRunTime2.dataManager(), SIGNAL(rowsInserted(QModelIndex,int,int)));

    host2.connectToPeer(QStringLiteral("host1"), QHostAddress::LocalHost, 4567);

    myNewVariableSignal.wait();

    QVERIFY(myNewVariableSignal.count() == 1);

    QSharedPointer<SubscribedVariableRecord> variableFromHost1 = hostRunTime2.dataManager()->getVariable(QStringLiteral("variableHost1"), QStringLiteral("testVariable"));

    QVERIFY(variableFromHost1);

    QSignalSpy variableFromHost1PeerIdSignal(variableFromHost1.data(), SIGNAL(peerIdChanged(Automatiq::peerId)));
    QSignalSpy variableFromHost1SubscriptionChangedSignal(variableFromHost1.data(), SIGNAL(subscriptionChanged(bool)));

    host1.disconnectFromPeer(0);

    variableFromHost1PeerIdSignal.wait();
    variableFromHost1SubscriptionChangedSignal.wait();

    QVERIFY(variableFromHost1PeerIdSignal.count() == 1);
    QVERIFY(variableFromHost1SubscriptionChangedSignal.count() == 0);


    qDebug() << "end of test";
}

QTEST_MAIN(DataManagerTests)

#include "datamanagertests.moc"
