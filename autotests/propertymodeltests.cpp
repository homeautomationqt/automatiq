/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "propertymodel.h"

#include <QtTest/QtTest>

class PropertyModelTests: public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void listDataEmptyModel();

    void listDataModelWithData();

    void removeLine();

    void insertLineInside();

    void insertLineAtEnd();

    void modifyOneLine();

    void testRowCount();

    void testRoleNames();

    void testRoleNamesAfterInsert();
};

void PropertyModelTests::listDataEmptyModel()
{
    QVariant testData;

    PropertyModel newModel(testData);

    QVERIFY(newModel.rowCount() == 0);
}

void PropertyModelTests::listDataModelWithData()
{
    QVariant testData;
    QList<QVariant> listTestData;
    QMap<QString, QVariant> oneLine;

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("hello world");
    oneLine[QStringLiteral("testVariable2")] = 1234;
    oneLine[QStringLiteral("testVariable3")] = true;
    listTestData.push_back(oneLine);

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("bienvenue");
    oneLine[QStringLiteral("testVariable2")] = 9856;
    oneLine[QStringLiteral("testVariable3")] = false;
    listTestData.push_back(oneLine);

    testData = listTestData;

    PropertyModel newModel(testData);

    QVERIFY(newModel.rowCount() == 2);

    QVERIFY(newModel.data(newModel.index(0), 0).canConvert<QString>());
    QVERIFY(newModel.data(newModel.index(0), 0).toString() == QStringLiteral("hello world"));
    QVERIFY(newModel.data(newModel.index(0), 1).canConvert<int>());
    QVERIFY(newModel.data(newModel.index(0), 1).toInt() == 1234);
    QVERIFY(newModel.data(newModel.index(0), 2).canConvert<bool>());
    QVERIFY(newModel.data(newModel.index(0), 2).toBool() == true);

    QVERIFY(newModel.data(newModel.index(1), 0).canConvert<QString>());
    QVERIFY(newModel.data(newModel.index(1), 0).toString() == QStringLiteral("bienvenue"));
    QVERIFY(newModel.data(newModel.index(1), 1).canConvert<int>());
    QVERIFY(newModel.data(newModel.index(1), 1).toInt() == 9856);
    QVERIFY(newModel.data(newModel.index(1), 2).canConvert<bool>());
    QVERIFY(newModel.data(newModel.index(1), 2).toBool() == false);
}

void PropertyModelTests::removeLine()
{
    QVariant testData;
    QList<QVariant> listTestData;
    QMap<QString, QVariant> oneLine;

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("hello world");
    oneLine[QStringLiteral("testVariable2")] = 1234;
    oneLine[QStringLiteral("testVariable3")] = true;
    listTestData.push_back(oneLine);

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("bienvenue");
    oneLine[QStringLiteral("testVariable2")] = 9856;
    oneLine[QStringLiteral("testVariable3")] = false;
    listTestData.push_back(oneLine);

    testData = listTestData;

    PropertyModel newModel(testData);

    QVERIFY(newModel.rowCount() == 2);

    QSignalSpy spyBeginRemoveRows(&newModel, SIGNAL(rowsAboutToBeRemoved(QModelIndex,int,int)));
    QSignalSpy spyEndRemoveRows(&newModel, SIGNAL(rowsRemoved(QModelIndex,int,int)));

    QVERIFY(newModel.removeRows(1, 1));

    QVERIFY(newModel.rowCount() == 1);
    QVERIFY(spyBeginRemoveRows.count() == 1);
    auto argsBegin(spyBeginRemoveRows.at(0));
    QVERIFY(!argsBegin.at(0).toModelIndex().isValid());
    QVERIFY(argsBegin.at(1).toInt() == 1);
    QVERIFY(argsBegin.at(2).toInt() == 1);
    QVERIFY(spyEndRemoveRows.count() == 1);
    auto argsEnd(spyEndRemoveRows.at(0));
    QVERIFY(!argsEnd.at(0).toModelIndex().isValid());
    QVERIFY(argsEnd.at(1).toInt() == 1);
    QVERIFY(argsEnd.at(2).toInt() == 1);

    QVERIFY(newModel.data(newModel.index(0), 0).canConvert<QString>());
    QVERIFY(newModel.data(newModel.index(0), 0).toString() == QStringLiteral("hello world"));
    QVERIFY(newModel.data(newModel.index(0), 1).canConvert<int>());
    QVERIFY(newModel.data(newModel.index(0), 1).toInt() == 1234);
    QVERIFY(newModel.data(newModel.index(0), 2).canConvert<bool>());
    QVERIFY(newModel.data(newModel.index(0), 2).toBool() == true);

    listTestData = testData.toList();

    QVERIFY(listTestData.count() == 1);
    bool result = listTestData.at(0).canConvert<QMap<QString, QVariant> >();
    QVERIFY(result);
    QMap<QString, QVariant> theData(listTestData.at(0).toMap());
    QVERIFY(theData.count() == 3);
    QVERIFY(theData[QStringLiteral("testVariable1")].canConvert<QString>());
    QVERIFY(theData[QStringLiteral("testVariable1")].toString() == QStringLiteral("hello world"));
    QVERIFY(theData[QStringLiteral("testVariable2")].canConvert<int>());
    QVERIFY(theData[QStringLiteral("testVariable2")].toInt() == 1234);
    QVERIFY(theData[QStringLiteral("testVariable3")].canConvert<bool>());
    QVERIFY(theData[QStringLiteral("testVariable3")].toBool() == true);
}

void PropertyModelTests::insertLineInside()
{
    QVariant testData;
    QList<QVariant> listTestData;
    QMap<QString, QVariant> oneLine;

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("hello world");
    oneLine[QStringLiteral("testVariable2")] = 1234;
    oneLine[QStringLiteral("testVariable3")] = true;
    listTestData.push_back(oneLine);

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("bienvenue");
    oneLine[QStringLiteral("testVariable2")] = 9856;
    oneLine[QStringLiteral("testVariable3")] = false;
    listTestData.push_back(oneLine);

    testData = listTestData;

    PropertyModel newModel(testData);

    QVERIFY(newModel.rowCount() == 2);

    QSignalSpy spyBeginInsertRows(&newModel, SIGNAL(rowsAboutToBeInserted(QModelIndex,int,int)));
    QSignalSpy spyEndInsertRows(&newModel, SIGNAL(rowsInserted(QModelIndex,int,int)));

    QVERIFY(newModel.insertRows(1, 1));

    QVERIFY(newModel.rowCount() == 3);
    QVERIFY(spyBeginInsertRows.count() == 1);
    auto argsBegin(spyBeginInsertRows.at(0));
    QVERIFY(!argsBegin.at(0).toModelIndex().isValid());
    QVERIFY(argsBegin.at(1).toInt() == 1);
    QVERIFY(argsBegin.at(2).toInt() == 1);
    QVERIFY(spyEndInsertRows.count() == 1);
    auto argsEnd(spyEndInsertRows.at(0));
    QVERIFY(!argsEnd.at(0).toModelIndex().isValid());
    QVERIFY(argsEnd.at(1).toInt() == 1);
    QVERIFY(argsEnd.at(2).toInt() == 1);

    QVERIFY(newModel.data(newModel.index(0), 0).canConvert<QString>());
    QVERIFY(newModel.data(newModel.index(0), 0).toString() == QStringLiteral("hello world"));
    QVERIFY(newModel.data(newModel.index(0), 1).canConvert<int>());
    QVERIFY(newModel.data(newModel.index(0), 1).toInt() == 1234);
    QVERIFY(newModel.data(newModel.index(0), 2).canConvert<bool>());
    QVERIFY(newModel.data(newModel.index(0), 2).toBool() == true);

    QVERIFY(!newModel.data(newModel.index(1), 0).canConvert<QString>());
    QVERIFY(!newModel.data(newModel.index(1), 1).canConvert<int>());
    QVERIFY(!newModel.data(newModel.index(1), 2).canConvert<bool>());

    QVERIFY(newModel.data(newModel.index(2), 0).canConvert<QString>());
    QVERIFY(newModel.data(newModel.index(2), 0).toString() == QStringLiteral("bienvenue"));
    QVERIFY(newModel.data(newModel.index(2), 1).canConvert<int>());
    QVERIFY(newModel.data(newModel.index(2), 1).toInt() == 9856);
    QVERIFY(newModel.data(newModel.index(2), 2).canConvert<bool>());
    QVERIFY(newModel.data(newModel.index(2), 2).toBool() == false);

    listTestData = testData.toList();

    QVERIFY(listTestData.count() == 3);

    bool result = listTestData.at(0).canConvert<QMap<QString, QVariant> >();
    QVERIFY(result);
    QMap<QString, QVariant> theData(listTestData.at(0).toMap());
    QVERIFY(theData.count() == 3);
    QVERIFY(theData[QStringLiteral("testVariable1")].canConvert<QString>());
    QVERIFY(theData[QStringLiteral("testVariable1")].toString() == QStringLiteral("hello world"));
    QVERIFY(theData[QStringLiteral("testVariable2")].canConvert<int>());
    QVERIFY(theData[QStringLiteral("testVariable2")].toInt() == 1234);
    QVERIFY(theData[QStringLiteral("testVariable3")].canConvert<bool>());
    QVERIFY(theData[QStringLiteral("testVariable3")].toBool() == true);

    result = listTestData.at(1).canConvert<QMap<QString, QVariant> >();
    QVERIFY(result);
    theData = listTestData.at(1).toMap();
    QVERIFY(theData.count() == 3);
    QVERIFY(!theData[QStringLiteral("testVariable1")].canConvert<QString>());
    QVERIFY(!theData[QStringLiteral("testVariable2")].canConvert<int>());
    QVERIFY(!theData[QStringLiteral("testVariable3")].canConvert<bool>());

    result = listTestData.at(2).canConvert<QMap<QString, QVariant> >();
    QVERIFY(result);
    theData = listTestData.at(2).toMap();
    QVERIFY(theData.count() == 3);
    QVERIFY(theData[QStringLiteral("testVariable1")].canConvert<QString>());
    QVERIFY(theData[QStringLiteral("testVariable1")].toString() == QStringLiteral("bienvenue"));
    QVERIFY(theData[QStringLiteral("testVariable2")].canConvert<int>());
    QVERIFY(theData[QStringLiteral("testVariable2")].toInt() == 9856);
    QVERIFY(theData[QStringLiteral("testVariable3")].canConvert<bool>());
    QVERIFY(theData[QStringLiteral("testVariable3")].toBool() == false);
}

void PropertyModelTests::insertLineAtEnd()
{
    QVariant testData;
    QList<QVariant> listTestData;
    QMap<QString, QVariant> oneLine;

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("hello world");
    oneLine[QStringLiteral("testVariable2")] = 1234;
    oneLine[QStringLiteral("testVariable3")] = true;
    listTestData.push_back(oneLine);

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("bienvenue");
    oneLine[QStringLiteral("testVariable2")] = 9856;
    oneLine[QStringLiteral("testVariable3")] = false;
    listTestData.push_back(oneLine);

    testData = listTestData;

    PropertyModel newModel(testData);

    QVERIFY(newModel.rowCount() == 2);

    QSignalSpy spyBeginInsertRows(&newModel, SIGNAL(rowsAboutToBeInserted(QModelIndex,int,int)));
    QSignalSpy spyEndInsertRows(&newModel, SIGNAL(rowsInserted(QModelIndex,int,int)));

    QVERIFY(newModel.insertRows(2, 1));

    QVERIFY(newModel.rowCount() == 3);
    QVERIFY(spyBeginInsertRows.count() == 1);
    auto argsBegin(spyBeginInsertRows.at(0));
    QVERIFY(!argsBegin.at(0).toModelIndex().isValid());
    QVERIFY(argsBegin.at(1).toInt() == 2);
    QVERIFY(argsBegin.at(2).toInt() == 2);
    QVERIFY(spyEndInsertRows.count() == 1);
    auto argsEnd(spyEndInsertRows.at(0));
    QVERIFY(!argsEnd.at(0).toModelIndex().isValid());
    QVERIFY(argsEnd.at(1).toInt() == 2);
    QVERIFY(argsEnd.at(2).toInt() == 2);

    listTestData = testData.toList();

    QVERIFY(listTestData.count() == 3);

    bool result = listTestData.at(0).canConvert<QMap<QString, QVariant> >();
    QVERIFY(result);
    QMap<QString, QVariant> theData(listTestData.at(0).toMap());
    QVERIFY(theData.count() == 3);
    QVERIFY(theData[QStringLiteral("testVariable1")].canConvert<QString>());
    QVERIFY(theData[QStringLiteral("testVariable1")].toString() == QStringLiteral("hello world"));
    QVERIFY(theData[QStringLiteral("testVariable2")].canConvert<int>());
    QVERIFY(theData[QStringLiteral("testVariable2")].toInt() == 1234);
    QVERIFY(theData[QStringLiteral("testVariable3")].canConvert<bool>());
    QVERIFY(theData[QStringLiteral("testVariable3")].toBool() == true);

    result = listTestData.at(1).canConvert<QMap<QString, QVariant> >();
    QVERIFY(result);
    theData = listTestData.at(1).toMap();
    QVERIFY(theData.count() == 3);
    QVERIFY(theData[QStringLiteral("testVariable1")].canConvert<QString>());
    QVERIFY(theData[QStringLiteral("testVariable1")].toString() == QStringLiteral("bienvenue"));
    QVERIFY(theData[QStringLiteral("testVariable2")].canConvert<int>());
    QVERIFY(theData[QStringLiteral("testVariable2")].toInt() == 9856);
    QVERIFY(theData[QStringLiteral("testVariable3")].canConvert<bool>());
    QVERIFY(theData[QStringLiteral("testVariable3")].toBool() == false);

    result = listTestData.at(2).canConvert<QMap<QString, QVariant> >();
    QVERIFY(result);
    theData = listTestData.at(2).toMap();
    QVERIFY(theData.count() == 3);
    QVERIFY(!theData[QStringLiteral("testVariable1")].canConvert<QString>());
    QVERIFY(!theData[QStringLiteral("testVariable2")].canConvert<int>());
    QVERIFY(!theData[QStringLiteral("testVariable3")].canConvert<bool>());

    QVERIFY(newModel.data(newModel.index(0), 0).canConvert<QString>());
    QVERIFY(newModel.data(newModel.index(0), 0).toString() == QStringLiteral("hello world"));
    QVERIFY(newModel.data(newModel.index(0), 1).canConvert<int>());
    QVERIFY(newModel.data(newModel.index(0), 1).toInt() == 1234);
    QVERIFY(newModel.data(newModel.index(0), 2).canConvert<bool>());
    QVERIFY(newModel.data(newModel.index(0), 2).toBool() == true);

    QVERIFY(newModel.data(newModel.index(1), 0).canConvert<QString>());
    QVERIFY(newModel.data(newModel.index(1), 0).toString() == QStringLiteral("bienvenue"));
    QVERIFY(newModel.data(newModel.index(1), 1).canConvert<int>());
    QVERIFY(newModel.data(newModel.index(1), 1).toInt() == 9856);
    QVERIFY(newModel.data(newModel.index(1), 2).canConvert<bool>());
    QVERIFY(newModel.data(newModel.index(1), 2).toBool() == false);

    QVERIFY(!newModel.data(newModel.index(2), 0).canConvert<QString>());
    QVERIFY(!newModel.data(newModel.index(2), 1).canConvert<int>());
    QVERIFY(!newModel.data(newModel.index(2), 2).canConvert<bool>());
}

void PropertyModelTests::modifyOneLine()
{
    QVariant testData;
    QList<QVariant> listTestData;
    QMap<QString, QVariant> oneLine;

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("hello world");
    oneLine[QStringLiteral("testVariable2")] = 1234;
    oneLine[QStringLiteral("testVariable3")] = true;
    listTestData.push_back(oneLine);

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("bienvenue");
    oneLine[QStringLiteral("testVariable2")] = 9856;
    oneLine[QStringLiteral("testVariable3")] = false;
    listTestData.push_back(oneLine);

    testData = listTestData;

    PropertyModel newModel(testData);

    QVERIFY(newModel.rowCount() == 2);

    QSignalSpy spyDataChanged(&newModel, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)));

    QVERIFY(newModel.setData(newModel.index(1), QStringLiteral("bye world"), 0));

    QVERIFY(newModel.rowCount() == 2);
    QVERIFY(spyDataChanged.count() == 1);
    auto argsBegin(spyDataChanged.at(0));
    QVERIFY(argsBegin.at(0).toModelIndex().isValid());
    QVERIFY(!argsBegin.at(0).toModelIndex().parent().isValid());
    QVERIFY(argsBegin.at(0).toModelIndex().row() == 1);
    QVERIFY(argsBegin.at(1).toModelIndex().isValid());
    QVERIFY(!argsBegin.at(1).toModelIndex().parent().isValid());
    QVERIFY(argsBegin.at(1).toModelIndex().row() == 1);

    QVERIFY(newModel.data(newModel.index(1), 0).toString() == QStringLiteral("bye world"));

    QVERIFY(newModel.setData(newModel.index(1), 7456, 1));

    QVERIFY(newModel.rowCount() == 2);
    QVERIFY(spyDataChanged.count() == 2);
    argsBegin = spyDataChanged.at(1);
    QVERIFY(argsBegin.at(0).toModelIndex().isValid());
    QVERIFY(!argsBegin.at(0).toModelIndex().parent().isValid());
    QVERIFY(argsBegin.at(0).toModelIndex().row() == 1);
    QVERIFY(argsBegin.at(1).toModelIndex().isValid());
    QVERIFY(!argsBegin.at(1).toModelIndex().parent().isValid());
    QVERIFY(argsBegin.at(1).toModelIndex().row() == 1);

    QVERIFY(newModel.data(newModel.index(1), 1).toInt() == 7456);

    QVERIFY(newModel.setData(newModel.index(1), true, 2));

    QVERIFY(newModel.rowCount() == 2);
    QVERIFY(spyDataChanged.count() == 3);
    argsBegin = spyDataChanged.at(2);
    QVERIFY(argsBegin.at(0).toModelIndex().isValid());
    QVERIFY(!argsBegin.at(0).toModelIndex().parent().isValid());
    QVERIFY(argsBegin.at(0).toModelIndex().row() == 1);
    QVERIFY(argsBegin.at(1).toModelIndex().isValid());
    QVERIFY(!argsBegin.at(1).toModelIndex().parent().isValid());
    QVERIFY(argsBegin.at(1).toModelIndex().row() == 1);

    QVERIFY(newModel.data(newModel.index(1), 0).toBool() == true);

    listTestData = testData.toList();

    QVERIFY(listTestData.count() == 2);

    bool result = listTestData.at(0).canConvert<QMap<QString, QVariant> >();
    QVERIFY(result);
    QMap<QString, QVariant> theData(listTestData.at(0).toMap());
    QVERIFY(theData.count() == 3);
    QVERIFY(theData[QStringLiteral("testVariable1")].canConvert<QString>());
    QVERIFY(theData[QStringLiteral("testVariable1")].toString() == QStringLiteral("hello world"));
    QVERIFY(theData[QStringLiteral("testVariable2")].canConvert<int>());
    QVERIFY(theData[QStringLiteral("testVariable2")].toInt() == 1234);
    QVERIFY(theData[QStringLiteral("testVariable3")].canConvert<bool>());
    QVERIFY(theData[QStringLiteral("testVariable3")].toBool() == true);

    result = listTestData.at(1).canConvert<QMap<QString, QVariant> >();
    QVERIFY(result);
    theData = listTestData.at(1).toMap();
    QVERIFY(theData.count() == 3);
    QVERIFY(theData[QStringLiteral("testVariable1")].canConvert<QString>());
    QVERIFY(theData[QStringLiteral("testVariable1")].toString() == QStringLiteral("bye world"));
    QVERIFY(theData[QStringLiteral("testVariable2")].canConvert<int>());
    QVERIFY(theData[QStringLiteral("testVariable2")].toInt() == 7456);
    QVERIFY(theData[QStringLiteral("testVariable3")].canConvert<bool>());
    QVERIFY(theData[QStringLiteral("testVariable3")].toBool() == true);
}

void PropertyModelTests::testRowCount()
{
    QVariant testData;
    QList<QVariant> listTestData;
    QMap<QString, QVariant> oneLine;

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("hello world");
    oneLine[QStringLiteral("testVariable2")] = 1234;
    oneLine[QStringLiteral("testVariable3")] = true;
    listTestData.push_back(oneLine);

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("bienvenue");
    oneLine[QStringLiteral("testVariable2")] = 9856;
    oneLine[QStringLiteral("testVariable3")] = false;
    listTestData.push_back(oneLine);

    testData = listTestData;

    PropertyModel newModel(testData);

    QVERIFY(newModel.rowCount() == 2);
    QVERIFY(newModel.rowCount(newModel.index(0)) == 0);
    QVERIFY(newModel.rowCount(newModel.index(0).parent()) == 2);
    QVERIFY(newModel.rowCount(newModel.index(1)) == 0);
    QVERIFY(newModel.rowCount(newModel.index(1).parent()) == 2);
}

void PropertyModelTests::testRoleNames()
{
    QVariant testData;
    QList<QVariant> listTestData;
    QMap<QString, QVariant> oneLine;

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("hello world");
    oneLine[QStringLiteral("testVariable2")] = 1234;
    oneLine[QStringLiteral("testVariable3")] = true;
    listTestData.push_back(oneLine);

    oneLine[QStringLiteral("testVariable1")] = QStringLiteral("bienvenue");
    oneLine[QStringLiteral("testVariable2")] = 9856;
    oneLine[QStringLiteral("testVariable3")] = false;
    listTestData.push_back(oneLine);

    testData = listTestData;

    PropertyModel newModel(testData);

    QVERIFY(newModel.roleNames().size() == 3);
    QVERIFY(newModel.roleNames()[0] == "testVariable1");
    QVERIFY(newModel.roleNames()[1] == "testVariable2");
    QVERIFY(newModel.roleNames()[2] == "testVariable3");
}

void PropertyModelTests::testRoleNamesAfterInsert()
{
    QVariant testData;

    PropertyModel newModel(testData);

    QList<QString> roles;

    roles.push_back(QStringLiteral("testVariable1"));
    roles.push_back(QStringLiteral("testVariable2"));
    roles.push_back(QStringLiteral("testVariable3"));

    newModel.setRoleNames(roles);

    newModel.insertRows(0, 1);

    QList<QVariant> listTestData = testData.toList();
    QMap<QString, QVariant> oneLine;

    newModel.setData(newModel.index(0), QStringLiteral("hello world"), 0);
    newModel.setData(newModel.index(0), 1234, 1);
    newModel.setData(newModel.index(0), true, 2);

    testData = listTestData;

    QVERIFY(newModel.roleNames().size() == 3);
    QVERIFY(newModel.roleNames()[0] == "testVariable1");
    QVERIFY(newModel.roleNames()[1] == "testVariable2");
    QVERIFY(newModel.roleNames()[2] == "testVariable3");
}

QTEST_MAIN(PropertyModelTests)

#include "propertymodeltests.moc"
