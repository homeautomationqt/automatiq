/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagehello.h"
#include "messagehelloacknowledge.h"
#include "messagegetvariables.h"
#include "messagelistvariables.h"
#include "messagelistvariablevalues.h"
#include "messageacknowledge.h"
#include "messageerror.h"
#include "messageconfiguration.h"
#include "messagegetconfiguration.h"
#include "messagesubscribevariables.h"
#include "messagerequestvariablesubscriptions.h"
#include "messagesynchronization.h"
#include "messagecontainer.h"
#include "datavariablesubscriptionrequest.h"
#include "datavariablesubscription.h"
#include "datavariabledescription.h"
#include "datavariableidentifier.h"
#include "datavariablewithvalue.h"

#include <QtTest/QtTest>
#include <QtCore/QBuffer>

class EncodeMessages: public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void encodeDecodeMessageHello();

    void encodeDecodeMessageHelloAcknowledge();

    void encodeDecodeMessageGetVariables();

    void encodeDecodeMessageListVariables();

    void encodeDecodeMessageListVariableValues();

    void encodeDecodeMessageRequestVariableSubscriptions();

    void encodeDecodeMessageSubscribeVariables();

    void encodeDecodeMessageAcknowledge();

    void encodeDecodeMessageError();

    void encodeDecodeMessageGetConfiguration();

    void encodeDecodeMessageConfiguration();

    void encodeDecodeMessageSynchronization();

    void encodeDecodeMessageHelloWithContainer();

    void encodeDecodeMessageHelloWithContainerWrongType1();

    void encodeDecodeMessageHelloWithContainerWrongType2();

    void encodeDecodeMessageHelloWithContainerWrongType3();

    void encodeDecodeMessageHelloWithContainerWrongType4();

    void encodeDecodeMessageHelloWithContainerWrongType5();

    void encodeDecodeMessageHelloWithContainerWrongType6();

    void encodeDecodeMessageHelloWithContainerWrongType7();

    void encodeDecodeMessageHelloWithContainerWrongType8();

    void encodeDecodeMessageHelloWithContainerWrongType9();

    void encodeDecodeMessageHelloWithContainerWrongType10();

    void encodeDecodeMessageHelloWithContainerWrongType11();

    void encodeDecodeMessageHelloAcknowledgeWithContainer();

    void encodeDecodeMessageHelloAcknowledgeWithContainerWrongType1();

    void encodeDecodeMessageHelloAcknowledgeWithContainerWrongType2();

    void encodeDecodeMessageHelloAcknowledgeWithContainerWrongType3();

    void encodeDecodeMessageHelloAcknowledgeWithContainerWrongType4();

    void encodeDecodeMessageHelloAcknowledgeWithContainerWrongType5();

    void encodeDecodeMessageHelloAcknowledgeWithContainerWrongType6();

    void encodeDecodeMessageHelloAcknowledgeWithContainerWrongType7();

    void encodeDecodeMessageHelloAcknowledgeWithContainerWrongType8();

    void encodeDecodeMessageHelloAcknowledgeWithContainerWrongType9();

    void encodeDecodeMessageHelloAcknowledgeWithContainerWrongType10();

    void encodeDecodeMessageHelloAcknowledgeWithContainerWrongType11();

    void encodeDecodeMessageGetVariablesWithContainer();

    void encodeDecodeMessageGetVariablesWithContainerWrongType1();

    void encodeDecodeMessageGetVariablesWithContainerWrongType2();

    void encodeDecodeMessageGetVariablesWithContainerWrongType3();

    void encodeDecodeMessageGetVariablesWithContainerWrongType4();

    void encodeDecodeMessageGetVariablesWithContainerWrongType5();

    void encodeDecodeMessageGetVariablesWithContainerWrongType6();

    void encodeDecodeMessageGetVariablesWithContainerWrongType7();

    void encodeDecodeMessageGetVariablesWithContainerWrongType8();

    void encodeDecodeMessageGetVariablesWithContainerWrongType9();

    void encodeDecodeMessageGetVariablesWithContainerWrongType10();

    void encodeDecodeMessageGetVariablesWithContainerWrongType11();

    void encodeDecodeMessageListVariablesWithContainer();

    void encodeDecodeMessageListVariablesWithContainerWrongType1();

    void encodeDecodeMessageListVariablesWithContainerWrongType2();

    void encodeDecodeMessageListVariablesWithContainerWrongType3();

    void encodeDecodeMessageListVariablesWithContainerWrongType4();

    void encodeDecodeMessageListVariablesWithContainerWrongType5();

    void encodeDecodeMessageListVariablesWithContainerWrongType6();

    void encodeDecodeMessageListVariablesWithContainerWrongType7();

    void encodeDecodeMessageListVariablesWithContainerWrongType8();

    void encodeDecodeMessageListVariablesWithContainerWrongType9();

    void encodeDecodeMessageListVariablesWithContainerWrongType10();

    void encodeDecodeMessageListVariablesWithContainerWrongType11();

    void encodeDecodeMessageListVariableValuesWithContainer();

    void encodeDecodeMessageListVariableValuesWithContainerWrongType1();

    void encodeDecodeMessageListVariableValuesWithContainerWrongType2();

    void encodeDecodeMessageListVariableValuesWithContainerWrongType3();

    void encodeDecodeMessageListVariableValuesWithContainerWrongType4();

    void encodeDecodeMessageListVariableValuesWithContainerWrongType5();

    void encodeDecodeMessageListVariableValuesWithContainerWrongType6();

    void encodeDecodeMessageListVariableValuesWithContainerWrongType7();

    void encodeDecodeMessageListVariableValuesWithContainerWrongType8();

    void encodeDecodeMessageListVariableValuesWithContainerWrongType9();

    void encodeDecodeMessageListVariableValuesWithContainerWrongType10();

    void encodeDecodeMessageListVariableValuesWithContainerWrongType11();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainer();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType1();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType2();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType3();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType4();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType5();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType6();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType7();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType8();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType9();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType10();

    void encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType11();

    void encodeDecodeMessageSubscribeVariablesWithContainer();

    void encodeDecodeMessageSubscribeVariablesWithContainerWrongType1();

    void encodeDecodeMessageSubscribeVariablesWithContainerWrongType2();

    void encodeDecodeMessageSubscribeVariablesWithContainerWrongType3();

    void encodeDecodeMessageSubscribeVariablesWithContainerWrongType4();

    void encodeDecodeMessageSubscribeVariablesWithContainerWrongType5();

    void encodeDecodeMessageSubscribeVariablesWithContainerWrongType6();

    void encodeDecodeMessageSubscribeVariablesWithContainerWrongType7();

    void encodeDecodeMessageSubscribeVariablesWithContainerWrongType8();

    void encodeDecodeMessageSubscribeVariablesWithContainerWrongType9();

    void encodeDecodeMessageSubscribeVariablesWithContainerWrongType10();

    void encodeDecodeMessageSubscribeVariablesWithContainerWrongType11();

    void encodeDecodeMessageAcknowledgeWithContainer();

    void encodeDecodeMessageAcknowledgeWithContainerWrongType1();

    void encodeDecodeMessageAcknowledgeWithContainerWrongType2();

    void encodeDecodeMessageAcknowledgeWithContainerWrongType3();

    void encodeDecodeMessageAcknowledgeWithContainerWrongType4();

    void encodeDecodeMessageAcknowledgeWithContainerWrongType5();

    void encodeDecodeMessageAcknowledgeWithContainerWrongType6();

    void encodeDecodeMessageAcknowledgeWithContainerWrongType7();

    void encodeDecodeMessageAcknowledgeWithContainerWrongType8();

    void encodeDecodeMessageAcknowledgeWithContainerWrongType9();

    void encodeDecodeMessageAcknowledgeWithContainerWrongType10();

    void encodeDecodeMessageAcknowledgeWithContainerWrongType11();

    void encodeDecodeMessageErrorWithContainer();

    void encodeDecodeMessageErrorWithContainerWrongType1();

    void encodeDecodeMessageErrorWithContainerWrongType2();

    void encodeDecodeMessageErrorWithContainerWrongType3();

    void encodeDecodeMessageErrorWithContainerWrongType4();

    void encodeDecodeMessageErrorWithContainerWrongType5();

    void encodeDecodeMessageErrorWithContainerWrongType6();

    void encodeDecodeMessageErrorWithContainerWrongType7();

    void encodeDecodeMessageErrorWithContainerWrongType8();

    void encodeDecodeMessageErrorWithContainerWrongType9();

    void encodeDecodeMessageErrorWithContainerWrongType10();

    void encodeDecodeMessageErrorWithContainerWrongType11();

    void encodeDecodeMessageGetConfigurationWithContainer();

    void encodeDecodeMessageGetConfigurationWithContainerWrongType1();

    void encodeDecodeMessageGetConfigurationWithContainerWrongType2();

    void encodeDecodeMessageGetConfigurationWithContainerWrongType3();

    void encodeDecodeMessageGetConfigurationWithContainerWrongType4();

    void encodeDecodeMessageGetConfigurationWithContainerWrongType5();

    void encodeDecodeMessageGetConfigurationWithContainerWrongType6();

    void encodeDecodeMessageGetConfigurationWithContainerWrongType7();

    void encodeDecodeMessageGetConfigurationWithContainerWrongType8();

    void encodeDecodeMessageGetConfigurationWithContainerWrongType9();

    void encodeDecodeMessageGetConfigurationWithContainerWrongType10();

    void encodeDecodeMessageGetConfigurationWithContainerWrongType11();

    void encodeDecodeMessageConfigurationWithContainer();

    void encodeDecodeMessageConfigurationWithContainerWrongType1();

    void encodeDecodeMessageConfigurationWithContainerWrongType2();

    void encodeDecodeMessageConfigurationWithContainerWrongType3();

    void encodeDecodeMessageConfigurationWithContainerWrongType4();

    void encodeDecodeMessageConfigurationWithContainerWrongType5();

    void encodeDecodeMessageConfigurationWithContainerWrongType6();

    void encodeDecodeMessageConfigurationWithContainerWrongType7();

    void encodeDecodeMessageConfigurationWithContainerWrongType8();

    void encodeDecodeMessageConfigurationWithContainerWrongType9();

    void encodeDecodeMessageConfigurationWithContainerWrongType10();

    void encodeDecodeMessageConfigurationWithContainerWrongType11();

    void encodeDecodeMessageSynchronizationWithContainer();

    void encodeDecodeMessageSynchronizationWithContainerWrongType1();

    void encodeDecodeMessageSynchronizationWithContainerWrongType2();

    void encodeDecodeMessageSynchronizationWithContainerWrongType3();

    void encodeDecodeMessageSynchronizationWithContainerWrongType4();

    void encodeDecodeMessageSynchronizationWithContainerWrongType5();

    void encodeDecodeMessageSynchronizationWithContainerWrongType6();

    void encodeDecodeMessageSynchronizationWithContainerWrongType7();

    void encodeDecodeMessageSynchronizationWithContainerWrongType8();

    void encodeDecodeMessageSynchronizationWithContainerWrongType9();

    void encodeDecodeMessageSynchronizationWithContainerWrongType10();

    void encodeDecodeMessageSynchronizationWithContainerWrongType11();
};

void EncodeMessages::encodeDecodeMessageHello()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageHello myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
    QVERIFY(myMessageToEncode.peerName() == myMessageToDecode.peerName());
    QVERIFY(myMessageToEncode.timestamp() == myMessageToDecode.timestamp());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledge()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageHelloAcknowledge myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
    QVERIFY(myMessageToEncode.peerName() == myMessageToDecode.peerName());
    QVERIFY(myMessageToEncode.timestamp() == myMessageToDecode.timestamp());
}

void EncodeMessages::encodeDecodeMessageGetVariables()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageGetVariables myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
    QVERIFY(myMessageToEncode.sendUpdateForChange() == myMessageToDecode.sendUpdateForChange());
}

void EncodeMessages::encodeDecodeMessageListVariables()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageListVariables myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
    QVERIFY(myMessageToEncode.variableDescriptors() == myMessageToDecode.variableDescriptors());
    QVERIFY(myMessageToEncode.variableDescriptors().at(0).sourceObjectName() == myMessageToDecode.variableDescriptors().at(0).sourceObjectName());
    QVERIFY(myMessageToEncode.variableDescriptors().at(0).sourceVariableName() == myMessageToDecode.variableDescriptors().at(0).sourceVariableName());
}

void EncodeMessages::encodeDecodeMessageListVariableValues()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageListVariableValues myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
    QVERIFY(myMessageToEncode.variableValues() == myMessageToDecode.variableValues());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptions()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Unsubscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageRequestVariableSubscriptions myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
    QVERIFY(myMessageToEncode.subscribeRequests() == myMessageToDecode.subscribeRequests());
    QVERIFY(myMessageToEncode.subscribeRequests().size() == 2);
    QVERIFY(myMessageToDecode.subscribeRequests().size() == 2);
    QVERIFY(myMessageToEncode.subscribeRequests().at(0).identifier() == myMessageToDecode.subscribeRequests().at(0).identifier());
    QVERIFY(myMessageToEncode.subscribeRequests().at(0).subscriptionRequestType() == myMessageToDecode.subscribeRequests().at(0).subscriptionRequestType());
    QVERIFY(myMessageToDecode.subscribeRequests().at(0).identifier() == DataVariableIdentifier(123456789));
    QVERIFY(myMessageToDecode.subscribeRequests().at(0).subscriptionRequestType() == DataVariableSubscriptionRequestType::Subscribe);
    QVERIFY(myMessageToEncode.subscribeRequests().at(1).identifier() == myMessageToDecode.subscribeRequests().at(1).identifier());
    QVERIFY(myMessageToEncode.subscribeRequests().at(1).subscriptionRequestType() == myMessageToDecode.subscribeRequests().at(1).subscriptionRequestType());
    QVERIFY(myMessageToDecode.subscribeRequests().at(1).identifier() == DataVariableIdentifier(987654321));
    QVERIFY(myMessageToDecode.subscribeRequests().at(1).subscriptionRequestType() == DataVariableSubscriptionRequestType::Unsubscribe);
}

void EncodeMessages::encodeDecodeMessageSubscribeVariables()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableIdentifier mySecondVariable(987654321);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back({myFirstVariable, true});
    mySubscriptions.push_back({mySecondVariable, false});

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageSubscribeVariables myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
    QVERIFY(myMessageToEncode.variableSubscriptions() == myMessageToDecode.variableSubscriptions());
    QVERIFY(myMessageToEncode.variableSubscriptions().size() == 2);
    QVERIFY(myMessageToDecode.variableSubscriptions().size() == 2);
    QVERIFY(myMessageToEncode.variableSubscriptions().at(0).identifier() == myMessageToDecode.variableSubscriptions().at(0).identifier());
    QVERIFY(myMessageToEncode.variableSubscriptions().at(0).isSubscribed() == myMessageToDecode.variableSubscriptions().at(0).isSubscribed());
    QVERIFY(myMessageToDecode.variableSubscriptions().at(0).identifier() == DataVariableIdentifier(123456789));
    QVERIFY(myMessageToDecode.variableSubscriptions().at(0).isSubscribed() == true);
    QVERIFY(myMessageToEncode.variableSubscriptions().at(1).identifier() == myMessageToDecode.variableSubscriptions().at(1).identifier());
    QVERIFY(myMessageToEncode.variableSubscriptions().at(1).isSubscribed() == myMessageToDecode.variableSubscriptions().at(1).isSubscribed());
    QVERIFY(myMessageToDecode.variableSubscriptions().at(1).identifier() == DataVariableIdentifier(987654321));
    QVERIFY(myMessageToDecode.variableSubscriptions().at(1).isSubscribed() == false);
}

void EncodeMessages::encodeDecodeMessageAcknowledge()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageAcknowledge myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
    QVERIFY(myMessageToEncode.acknowledge() == myMessageToDecode.acknowledge());
    QVERIFY(myMessageToEncode.sequenceNumber() == myMessageToDecode.sequenceNumber());
}

void EncodeMessages::encodeDecodeMessageError()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageError myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
    QVERIFY(myMessageToEncode.errorType() == myMessageToDecode.errorType());
    QVERIFY(myMessageToEncode.sequenceNumber() == myMessageToDecode.sequenceNumber());
}

void EncodeMessages::encodeDecodeMessageGetConfiguration()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageGetConfiguration myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
    QVERIFY(myMessageToEncode.sendUpdateForChange() == myMessageToDecode.sendUpdateForChange());
}

void EncodeMessages::encodeDecodeMessageConfiguration()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageConfiguration myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
    QVERIFY(myMessageToEncode.configuration() == myMessageToDecode.configuration());
}

void EncodeMessages::encodeDecodeMessageSynchronization()
{
    QBuffer myBuffer;
    myBuffer.open(QIODevice::ReadWrite);
    QDataStream myWriteStream(&myBuffer);

    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    myWriteStream << myMessageToEncode;

    QVERIFY(myWriteStream.status() == QDataStream::Ok);

    myBuffer.seek(0);
    QDataStream myReadStream(&myBuffer);

    MessageSynchronization myMessageToDecode;

    myReadStream >> myMessageToDecode;

    QVERIFY(myReadStream.status() == QDataStream::Ok);

    QVERIFY(myMessageToEncode == myMessageToDecode);
    QVERIFY(myMessageToEncode.isValid() == myMessageToDecode.isValid());
}

void EncodeMessages::encodeDecodeMessageHelloWithContainer()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageHello &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageHelloWithContainerWrongType1()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloWithContainerWrongType2()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloWithContainerWrongType3()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloWithContainerWrongType4()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloWithContainerWrongType5()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloWithContainerWrongType6()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloWithContainerWrongType7()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloWithContainerWrongType8()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloWithContainerWrongType9()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloWithContainerWrongType10()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloWithContainerWrongType11()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHello myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainer()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageHelloAcknowledge &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainerWrongType1()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainerWrongType2()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainerWrongType3()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainerWrongType4()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainerWrongType5()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainerWrongType6()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainerWrongType7()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainerWrongType8()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainerWrongType9()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainerWrongType10()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageHelloAcknowledgeWithContainerWrongType11()
{
    QDateTime testTime = QDateTime::fromMSecsSinceEpoch(1500);

    MessageHelloAcknowledge myMessageToEncode;
    myMessageToEncode.setTimestamp(testTime);
    myMessageToEncode.peerName() = QStringLiteral("TestPeer");
    myMessageToEncode.setValid(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainer()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageGetVariables &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainerWrongType1()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainerWrongType2()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainerWrongType3()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainerWrongType4()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainerWrongType5()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainerWrongType6()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainerWrongType7()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainerWrongType8()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainerWrongType9()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainerWrongType10()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetVariablesWithContainerWrongType11()
{
    MessageGetVariables myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainer()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageListVariables &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainerWrongType1()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainerWrongType2()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainerWrongType3()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainerWrongType4()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainerWrongType5()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainerWrongType6()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainerWrongType7()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainerWrongType8()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainerWrongType9()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainerWrongType10()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariablesWithContainerWrongType11()
{
    DataVariableDescription myFirstVariable(QStringLiteral("testObject1"), QStringLiteral("testVariable1"));
    DataVariableDescription mySecondVariable(QStringLiteral("testObject2"), QStringLiteral("testVariable2"));

    MessageListVariables::VariableDescriptors myVariables;
    myVariables.push_back(myFirstVariable);
    myVariables.push_back(mySecondVariable);

    MessageListVariables myMessageToEncode(myVariables);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainer()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageListVariableValues &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainerWrongType1()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainerWrongType2()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainerWrongType3()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainerWrongType4()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainerWrongType5()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainerWrongType6()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainerWrongType7()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainerWrongType8()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainerWrongType9()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainerWrongType10()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageListVariableValuesWithContainerWrongType11()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableWithValue myFirstValue(myFirstVariable, QStringLiteral("TestValue"));

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableWithValue mySecondValue(mySecondVariable, 123456);

    MessageListVariableValues::VariableValues myValues;
    myValues.push_back(myFirstValue);
    myValues.push_back(mySecondValue);

    MessageListVariableValues myMessageToEncode(myValues);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainer()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageRequestVariableSubscriptions &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType1()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType2()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType3()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType4()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType5()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType6()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType7()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType8()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType9()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType10()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageRequestVariableSubscriptionsWithContainerWrongType11()
{
    MessageRequestVariableSubscriptions::SubscribeRequests myRequests;
    myRequests.push_back({DataVariableIdentifier(123456789), DataVariableSubscriptionRequestType::Subscribe});
    myRequests.push_back({DataVariableIdentifier(987654321), DataVariableSubscriptionRequestType::Subscribe});

    MessageRequestVariableSubscriptions myMessageToEncode(myRequests);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainer()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageSubscribeVariables &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainerWrongType1()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainerWrongType2()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainerWrongType3()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainerWrongType4()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainerWrongType5()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainerWrongType6()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainerWrongType7()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainerWrongType8()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainerWrongType9()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainerWrongType10()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSubscribeVariablesWithContainerWrongType11()
{
    DataVariableIdentifier myFirstVariable(123456789);
    DataVariableSubscription myFirstSubscription(myFirstVariable);
    myFirstSubscription.setSubscribed(true);

    DataVariableIdentifier mySecondVariable(987654321);
    DataVariableSubscription mySecondSubscription(mySecondVariable);
    mySecondSubscription.setSubscribed(false);

    MessageSubscribeVariables::VariableSubscriptions mySubscriptions;
    mySubscriptions.push_back(myFirstSubscription);
    mySubscriptions.push_back(mySecondSubscription);

    MessageSubscribeVariables myMessageToEncode(mySubscriptions);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainer()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageAcknowledge &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainerWrongType1()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainerWrongType2()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainerWrongType3()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainerWrongType4()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainerWrongType5()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainerWrongType6()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainerWrongType7()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainerWrongType8()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainerWrongType9()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainerWrongType10()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageAcknowledgeWithContainerWrongType11()
{
    MessageAcknowledge myMessageToEncode;
    myMessageToEncode.setAcknowledge(ACKNOWLEDGE_TYPE::OK);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageErrorWithContainer()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageError &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageErrorWithContainerWrongType1()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageErrorWithContainerWrongType2()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageErrorWithContainerWrongType3()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageErrorWithContainerWrongType4()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageErrorWithContainerWrongType5()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageErrorWithContainerWrongType6()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageErrorWithContainerWrongType7()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageErrorWithContainerWrongType8()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageErrorWithContainerWrongType9()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageErrorWithContainerWrongType10()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageErrorWithContainerWrongType11()
{
    MessageError myMessageToEncode;
    myMessageToEncode.setErrorType(Automatiq::ERROR_TYPE::UnexpectedMessage);
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSequenceNumber(12345);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainer()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageGetConfiguration &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainerWrongType1()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainerWrongType2()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainerWrongType3()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainerWrongType4()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainerWrongType5()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainerWrongType6()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainerWrongType7()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainerWrongType8()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainerWrongType9()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainerWrongType10()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageGetConfigurationWithContainerWrongType11()
{
    MessageGetConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSendUpdateForChange(true);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainer()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageConfiguration &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainerWrongType1()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainerWrongType2()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainerWrongType3()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainerWrongType4()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainerWrongType5()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainerWrongType6()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainerWrongType7()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainerWrongType8()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainerWrongType9()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainerWrongType10()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageConfigurationWithContainerWrongType11()
{
    MessageConfiguration myMessageToEncode;
    myMessageToEncode.setValid(true);
    MessageConfiguration::configuration_type testConfig;
    myMessageToEncode.setConfiguration(testConfig);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainer()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSynchronization> myRawMessageToDecode(myContainer.specificMessage<MessageSynchronization>());

    QVERIFY(!myRawMessageToDecode.isNull());

    MessageSynchronization &myMessageToDecode(*myRawMessageToDecode.data());

    QVERIFY(myMessageToEncode == myMessageToDecode);
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainerWrongType1()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHello> myRawMessageToDecode(myContainer.specificMessage<MessageHello>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainerWrongType2()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariables> myRawMessageToDecode(myContainer.specificMessage<MessageListVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainerWrongType3()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageListVariableValues> myRawMessageToDecode(myContainer.specificMessage<MessageListVariableValues>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainerWrongType4()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetVariables> myRawMessageToDecode(myContainer.specificMessage<MessageGetVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainerWrongType5()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainerWrongType6()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageGetConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageGetConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainerWrongType7()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageConfiguration> myRawMessageToDecode(myContainer.specificMessage<MessageConfiguration>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainerWrongType8()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageHelloAcknowledge> myRawMessageToDecode(myContainer.specificMessage<MessageHelloAcknowledge>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainerWrongType9()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageSubscribeVariables> myRawMessageToDecode(myContainer.specificMessage<MessageSubscribeVariables>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainerWrongType10()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageRequestVariableSubscriptions> myRawMessageToDecode(myContainer.specificMessage<MessageRequestVariableSubscriptions>());

    QVERIFY(myRawMessageToDecode.isNull());
}

void EncodeMessages::encodeDecodeMessageSynchronizationWithContainerWrongType11()
{
    MessageSynchronization myMessageToEncode;
    myMessageToEncode.setValid(true);
    myMessageToEncode.setSynchronizationStep(Automatiq::DataProcessingSteps::ProcessingData);

    MessageContainer myContainer;
    myContainer.setSpecificMessage(myMessageToEncode);

    QVERIFY(myContainer.specificLength() > 0);

    QScopedPointer<MessageError> myRawMessageToDecode(myContainer.specificMessage<MessageError>());

    QVERIFY(myRawMessageToDecode.isNull());
}

QTEST_MAIN(EncodeMessages)

#include "encodemessages.moc"
