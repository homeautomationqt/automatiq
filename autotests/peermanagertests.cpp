/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "peermanager.h"
#include "networkpeer.h"

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

#include <QtCore/QScopedPointer>

#include <QtTest/QtTest>

class PeerManagerTests: public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void connectToRemotePeer();

    void cannotConnectToRemotePeer1();

    void cannotConnectToRemotePeer2();

    void connectFromRemotePeer();

    void connectDisconnectReconnect();

    void connectDisconnectWithModelTests();
};

void PeerManagerTests::connectToRemotePeer()
{
    qRegisterMetaType<PeerManager::ConnectionOrigin>("PeerManager::ConnectionOrigin");

    QTcpServer myServer;
    myServer.listen(QHostAddress::Any, 12345);

    PeerManager myManager;

    QSignalSpy mySignalSpy(&myManager, SIGNAL(newPeer(QSharedPointer<NetworkPeer>, PeerManager::ConnectionOrigin)));

    myManager.connectToPeer(QStringLiteral("server"), QHostAddress::LocalHost, 12345);

    myServer.waitForNewConnection();

    mySignalSpy.wait();

    QVERIFY(mySignalSpy.count() == 1);

    auto args(mySignalSpy.at(0));

    auto newPeer = args.at(0).value<QSharedPointer<NetworkPeer> >();
    QVERIFY(newPeer);

    QSignalSpy myDestroySignal(newPeer.data(), SIGNAL(destroyed(QObject*)));
    QSignalSpy myPeerSpyNewError(newPeer.data(), SIGNAL(newReadError(QAbstractSocket::SocketError)));
    QSignalSpy myPeerSpyDisconnected(newPeer.data(), SIGNAL(peerIsDisconnected(QAbstractSocket::SocketError)));

    auto clientSocket = myServer.nextPendingConnection();
    clientSocket->waitForConnected();

    myServer.close();
    clientSocket->close();

    myPeerSpyNewError.wait();

    QVERIFY(myPeerSpyNewError.count() == 1);
    QVERIFY(myPeerSpyDisconnected.count() == 1);
    QVERIFY(myDestroySignal.count() == 0);

    newPeer.clear();
    args.clear();
    mySignalSpy.clear();

    myDestroySignal.wait();

    QVERIFY(myDestroySignal.count() == 1);
}

void PeerManagerTests::cannotConnectToRemotePeer1()
{
    qRegisterMetaType<PeerManager::ConnectionOrigin>("PeerManager::ConnectionOrigin");

    PeerManager myManager;

    QSignalSpy mySignalSpy(&myManager, SIGNAL(newPeer(QSharedPointer<NetworkPeer>, PeerManager::ConnectionOrigin)));
    QSignalSpy myErrorSpy(&myManager, SIGNAL(peerDisconnected(QSharedPointer<NetworkPeer>)));

    myManager.connectToPeer(QStringLiteral("server"), QHostAddress::LocalHost, 12345);

    myErrorSpy.wait();

    QVERIFY(mySignalSpy.count() == 0);
    QVERIFY(myErrorSpy.count() == 1);
}

void PeerManagerTests::cannotConnectToRemotePeer2()
{
    qRegisterMetaType<PeerManager::ConnectionOrigin>("PeerManager::ConnectionOrigin");

    PeerManager myManager;

    QSignalSpy mySignalSpy(&myManager, SIGNAL(newPeer(QSharedPointer<NetworkPeer>, PeerManager::ConnectionOrigin)));
    QSignalSpy myErrorSpy(&myManager, SIGNAL(peerDisconnected(QSharedPointer<NetworkPeer>)));

    QScopedPointer<QTcpServer> myServer(new QTcpServer);
    myServer->listen(QHostAddress::Any, 12345);

    myManager.connectToPeer(QStringLiteral("server"), QHostAddress::LocalHost, 12345);

    mySignalSpy.wait();

    QVERIFY(mySignalSpy.count() == 1);

    auto argsNewPeer(mySignalSpy.at(0));

    auto newPeer = argsNewPeer.at(0).value<QSharedPointer<NetworkPeer> >();
    QVERIFY(newPeer);

    QSignalSpy myPeerSpyNewError(newPeer.data(), SIGNAL(newReadError(QAbstractSocket::SocketError)));
    QSignalSpy myPeerSpyDisconnected(newPeer.data(), SIGNAL(peerIsDisconnected(QAbstractSocket::SocketError)));
    QSignalSpy myDestroySignal(newPeer.data(), SIGNAL(destroyed(QObject*)));

    myServer->close();
    myServer.reset();

    myErrorSpy.wait();

    QVERIFY(myPeerSpyNewError.count() == 1);
    QVERIFY(myPeerSpyDisconnected.count() == 1);
    QVERIFY(myDestroySignal.count() == 0);
    QVERIFY(mySignalSpy.count() == 1);
    QVERIFY(myErrorSpy.count() == 1);

    auto argsPeerDisconnected(myErrorSpy.at(0));

    auto disconnectedPeer = argsPeerDisconnected.at(0).value<QSharedPointer<NetworkPeer> >();
    QVERIFY(disconnectedPeer);
    QVERIFY(disconnectedPeer == newPeer);

    newPeer.clear();
    argsNewPeer.clear();
    mySignalSpy.clear();
    disconnectedPeer.clear();
    argsPeerDisconnected.clear();
    myErrorSpy.clear();

    QVERIFY(myDestroySignal.count() == 1);
}

void PeerManagerTests::connectFromRemotePeer()
{
    qRegisterMetaType<PeerManager::ConnectionOrigin>("PeerManager::ConnectionOrigin");

    QTcpServer myServer;
    myServer.listen(QHostAddress::Any, 12345);

    PeerManager myManager;
    myManager.setServer(&myServer);
    QObject::connect(&myServer, &QTcpServer::newConnection, &myManager, &PeerManager::newPeerHasConnected);

    QSignalSpy mySignalSpy(&myManager, SIGNAL(newPeer(QSharedPointer<NetworkPeer>, PeerManager::ConnectionOrigin)));

    QTcpSocket mySocket;
    mySocket.connectToHost(QHostAddress::LocalHost, 12345);

    mySignalSpy.wait();

    QVERIFY(mySignalSpy.count() == 1);

    auto args(mySignalSpy.at(0));

    auto newPeer = args.at(0).value<QSharedPointer<NetworkPeer> >();
    QVERIFY(newPeer);

    QSignalSpy myPeerSpyNewError(newPeer.data(), SIGNAL(newReadError(QAbstractSocket::SocketError)));
    QSignalSpy myPeerSpyDisconnected(newPeer.data(), SIGNAL(peerIsDisconnected(QAbstractSocket::SocketError)));
    QSignalSpy myDestroySignal(newPeer.data(), SIGNAL(destroyed(QObject*)));

    mySocket.waitForConnected();

    myPeerSpyNewError.wait();

    mySocket.close();

    myPeerSpyNewError.wait();

    QVERIFY(myPeerSpyNewError.count() == 1);
    QVERIFY(myPeerSpyDisconnected.count() == 1);

    QVERIFY(myDestroySignal.count() == 0);

    newPeer.clear();
    args.clear();
    mySignalSpy.clear();

    myDestroySignal.wait();

    QVERIFY(myDestroySignal.count() == 1);
}

void PeerManagerTests::connectDisconnectReconnect()
{
    qRegisterMetaType<PeerManager::ConnectionOrigin>("PeerManager::ConnectionOrigin");

    QScopedPointer<QTcpServer> myServer(new QTcpServer);

    QScopedPointer<PeerManager> myServerManager(new PeerManager);
    myServerManager->setServer(myServer.data());
    QObject::connect(myServer.data(), &QTcpServer::newConnection, myServerManager.data(), &PeerManager::newPeerHasConnected);

    QVERIFY(myServer->listen(QHostAddress::AnyIPv4, 12345));

    PeerManager myClientManager;
    myClientManager.connectToPeer(QStringLiteral("myServer"), QHostAddress::LocalHost, 12345);

    QSignalSpy mySignalSpy(&myClientManager, SIGNAL(newPeer(QSharedPointer<NetworkPeer>, PeerManager::ConnectionOrigin)));
    QSignalSpy myErrorSpy(&myClientManager, SIGNAL(peerDisconnected(QSharedPointer<NetworkPeer>)));

    mySignalSpy.wait();

    QVERIFY(mySignalSpy.count() == 1);

    auto args(mySignalSpy.at(0));

    auto newPeer = args.at(0).value<QSharedPointer<NetworkPeer> >();
    QVERIFY(newPeer);

    myServer->close();
    myServerManager.reset();
    myServer.reset();

    myErrorSpy.wait();

    QVERIFY(myErrorSpy.count() == 1);

    myServer.reset(new QTcpServer);

    myServerManager.reset(new PeerManager);
    myServerManager->setServer(myServer.data());
    QObject::connect(myServer.data(), &QTcpServer::newConnection, myServerManager.data(), &PeerManager::newPeerHasConnected);

    QVERIFY(myServer->listen(QHostAddress::AnyIPv4, 12345));

    mySignalSpy.wait(10000);

    QVERIFY(mySignalSpy.count() == 2);
}

void PeerManagerTests::connectDisconnectWithModelTests()
{
    qRegisterMetaType<PeerManager::ConnectionOrigin>("PeerManager::ConnectionOrigin");

    QScopedPointer<QTcpServer> myServer(new QTcpServer);
    QVERIFY(myServer->listen(QHostAddress::Any, 12345));

    QScopedPointer<PeerManager> myServerManager(new PeerManager);
    myServerManager->setServer(myServer.data());
    QObject::connect(myServer.data(), &QTcpServer::newConnection, myServerManager.data(), &PeerManager::newPeerHasConnected);

    PeerManager myClientManager;

    QSignalSpy myNewLineSpy(&myClientManager, SIGNAL(rowsInserted(QModelIndex,int,int)));
    QSignalSpy modifiedLineSpy(&myClientManager, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)));

    myClientManager.connectToPeer(QStringLiteral("myServer"), QHostAddress::LocalHost, 12345);

    QVERIFY(myNewLineSpy.count() == 1);

    auto argsNewLine(myNewLineSpy.at(0));
    QVERIFY(!argsNewLine.at(0).value<QModelIndex>().isValid());
    QVERIFY(argsNewLine.at(1).value<int>() == 0);
    QVERIFY(argsNewLine.at(2).value<int>() == 0);

    QSignalSpy mySignalSpy(&myClientManager, SIGNAL(newPeer(QSharedPointer<NetworkPeer>, PeerManager::ConnectionOrigin)));
    QSignalSpy myErrorSpy(&myClientManager, SIGNAL(peerDisconnected(QSharedPointer<NetworkPeer>)));

    mySignalSpy.wait();

    QVERIFY(modifiedLineSpy.count() == 0);

    QVERIFY(mySignalSpy.count() == 1);

    auto args(mySignalSpy.at(0));

    auto newPeer = args.at(0).value<QSharedPointer<NetworkPeer> >();
    QVERIFY(newPeer);

    myServer->close();
    myServerManager.reset();
    myServer.reset();

    myErrorSpy.wait();

    QVERIFY(myErrorSpy.count() == 1);

    QVERIFY(modifiedLineSpy.count() == 1);

    auto argsModifiedLine(modifiedLineSpy.at(0));

    QVERIFY(argsModifiedLine.at(2).value<QVector<int> >().at(0) == PeerManager::PeerIdRole);
}

QTEST_MAIN(PeerManagerTests)

#include "peermanagertests.moc"
