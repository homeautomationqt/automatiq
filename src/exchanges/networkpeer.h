/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_NETWORK_PEER_H
#define AUTOMATIQ_NETWORK_PEER_H

#include "automatiqexchanges_export.h"

#include "commondeclarations.h"

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <QtNetwork/QAbstractSocket>

class QString;
class QTcpSocket;
class MessageContainer;

class NetworkPeerPrivate;

class AUTOMATIQEXCHANGES_EXPORT NetworkPeer : public QObject
{

    Q_OBJECT

    Q_PROPERTY(QString peerName
               READ peerName WRITE setPeerName
               NOTIFY peerNameChanged)

    Q_PROPERTY(Automatiq::peerId peerId
               READ peerId
               NOTIFY peerIdChanged)

    Q_PROPERTY(QTcpSocket* peerSocket
               READ peerSocket
               WRITE setPeerSocket
               NOTIFY peerSocketChanged)

public:
    explicit NetworkPeer(QObject *parent = 0);

    virtual ~NetworkPeer();

    bool isConnected() const;

    bool isDisconnected() const;

    Automatiq::peerId peerId() const;

    const QString& peerName() const;

    QTcpSocket *peerSocket();

    void setPeerSocket(QTcpSocket *aClientSocket);

    void disconnectFromPeer();

public Q_SLOTS:

    void setPeerName(const QString &aName);

    void setPeerId(Automatiq::peerId aId);

    void sendMessage(QSharedPointer<const MessageContainer> message);

Q_SIGNALS:

    void peerIsConnected();

    void peerNameChanged(const QString &newId);

    void peerIdChanged(Automatiq::peerId newValue);

    void peerSocketChanged();

    void peerNameWillChange(const QString &previousObjectName, const QString &newObjectName);

    void newReadError(QAbstractSocket::SocketError aError);

    void peerIsDisconnected(QAbstractSocket::SocketError aError);

    void receivedNewMessage(QSharedPointer<MessageContainer> receivedMessage);

private Q_SLOTS:

    void newDataToRead();

    void socketDisconnected();

    void socketError(QAbstractSocket::SocketError socketError);

private:

    NetworkPeerPrivate *d;
};

#endif
