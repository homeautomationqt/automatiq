/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagecontainer.h"

class MessageContainerPrivate
{
public:

    MessageContainerPrivate()
        : mIsValid(false), mVersion(), mSequenceNumber(), mIdentifier(), mProtocolIdentifier(), mSpecificBuffer()
    {
    }

    bool mIsValid;

    quint32 mVersion;

    quint64 mSequenceNumber;

    quint64 mIdentifier;

    quint64 mProtocolIdentifier;

    QByteArray mSpecificBuffer;

};

MessageContainer::MessageContainer() : d(new MessageContainerPrivate)
{
}

MessageContainer::MessageContainer(const MessageContainer &aOther) : d(new MessageContainerPrivate(*aOther.d))
{
}

MessageContainer::MessageContainer(MessageContainer &&aOther) : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageContainer::~MessageContainer()
{
    delete d;
}

MessageContainer& MessageContainer::operator=(const MessageContainer &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mVersion = aOther.d->mVersion;
        d->mSequenceNumber = aOther.d->mSequenceNumber;
        d->mIdentifier = aOther.d->mIdentifier;
        d->mProtocolIdentifier = aOther.d->mProtocolIdentifier;
        d->mSpecificBuffer = aOther.d->mSpecificBuffer;
    }

    return *this;
}

MessageContainer& MessageContainer::operator=(MessageContainer &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

bool MessageContainer::isValid() const
{
    return d->mIsValid;
}

quint32 MessageContainer::length() const
{
    return d->mSpecificBuffer.length() + sizeof(quint32) + headerLength;
}

quint32 MessageContainer::specificLength() const
{
    return d->mSpecificBuffer.length();
}

const QByteArray &MessageContainer::specificBuffer() const
{
    return d->mSpecificBuffer;
}

QByteArray &MessageContainer::specificBuffer()
{
    return d->mSpecificBuffer;
}

void MessageContainer::setVersion(quint32 aVersion) const
{
    d->mVersion = aVersion;
}

quint32 MessageContainer::version() const
{
    return d->mVersion;
}

quint32 &MessageContainer::version()
{
    return d->mVersion;
}

void MessageContainer::setSequenceNumber(quint64 aSequenceNumber) const
{
    d->mSequenceNumber = aSequenceNumber;
}

quint64 MessageContainer::sequenceNumber() const
{
    return d->mSequenceNumber;
}

quint64& MessageContainer::sequenceNumber()
{
    return d->mSequenceNumber;
}

quint64 MessageContainer::identifier() const
{
    return d->mIdentifier;
}

quint64 &MessageContainer::identifier()
{
    return d->mIdentifier;
}

void MessageContainer::setIdentifier(quint64 aIdentifier) const
{
    d->mIdentifier = aIdentifier;
}

quint64 MessageContainer::protocolIdentifier() const
{
    return d->mProtocolIdentifier;
}

quint64& MessageContainer::protocolIdentifier()
{
    return d->mProtocolIdentifier;
}

void MessageContainer::setProtocolIdentifier(quint64 aProtocolIdentifier) const
{
    d->mProtocolIdentifier = aProtocolIdentifier;
}
