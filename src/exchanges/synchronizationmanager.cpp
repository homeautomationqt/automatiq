/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "synchronizationmanager.h"

#include "abstractsynchronizationprotocol.h"
#include "abstractpeerprotocol.h"
#include "featuremanager.h"

#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QDateTime>
#include <QtCore/QDebug>

class SynchronizationManagerPrivate : public FeatureManager<AbstractSynchronizationProtocol>
{
public:

    SynchronizationManagerPrivate()
        : FeatureManager<AbstractSynchronizationProtocol>(), mSynchronizationState(), mCurrentSynchronizationStep(Automatiq::DataProcessingSteps::Invalid),
          mSynchronizationIsOnGoing(false), mPeerOwnName()
    {
    }

    QMap<Automatiq::peerId, bool> mSynchronizationState;

    Automatiq::DataProcessingSteps mCurrentSynchronizationStep;

    bool mSynchronizationIsOnGoing;

    QString mPeerOwnName;
};

SynchronizationManager::SynchronizationManager(const QString &peerOwnName, QObject *parent)
    : QObject(parent), d(new SynchronizationManagerPrivate)
{
    d->mPeerOwnName = peerOwnName;
}

SynchronizationManager::SynchronizationManager(QObject *parent)
    : QObject(parent), d(new SynchronizationManagerPrivate)
{
}

SynchronizationManager::~SynchronizationManager()
{
    delete d;
}

bool SynchronizationManager::isSynchronizationOnGoing() const
{
    return d->mSynchronizationIsOnGoing;
}

const QString &SynchronizationManager::peerOwnName() const
{
    return d->mPeerOwnName;
}

void SynchronizationManager::setPeerOwnName(const QString &peerName)
{
    d->mPeerOwnName = peerName;
}

void SynchronizationManager::newConnectedProtocol(Automatiq::peerId peerId, QSharedPointer<AbstractPeerProtocol> aProtocol)
{
    qDebug() << "SynchronizationManager::newConnectedProtocol peerId:" << peerId;

    d->mProtocols[peerId] = aProtocol->createSubSynchronizationProtocol(d->mPeerOwnName);

    if (d->mSynchronizationIsOnGoing) {
        d->mSynchronizationState[peerId] = true;
    } else {
        d->mSynchronizationState[peerId] = false;
    }

    QSharedPointer<AbstractSynchronizationProtocol> &newProtocol(d->mProtocols[peerId]);

    connect(newProtocol.data(), &AbstractSynchronizationProtocol::peerConnectionClosed, this, &SynchronizationManager::peerIsDisconnected);
    connect(newProtocol.data(), &AbstractSynchronizationProtocol::receivedSynchronization, this, &SynchronizationManager::receivedSynchronization);

    aProtocol->registerSubProtocol(newProtocol->protocolId(), newProtocol);
}

void SynchronizationManager::peerIsDisconnected()
{
    auto disconnectedPeerId = d->peerIsDisconnected(sender());

    if (disconnectedPeerId >= 0) {
        d->mSynchronizationState.remove(disconnectedPeerId);
        if (d->mSynchronizationIsOnGoing) {
            bool isSynchronizationDone = true;
            for(auto it:d->mSynchronizationState) {
                if (!it) {
                    isSynchronizationDone = false;
                }
            }
            if (isSynchronizationDone) {
                emitSynchronizationDone();
            }
        }
    }
}

void SynchronizationManager::receivedSynchronization(Automatiq::DataProcessingSteps step, Automatiq::peerId peerId)
{
    qDebug() << QDateTime::currentDateTime().toString(QStringLiteral("hh:mm:ss.zzz")) << "SynchronizationManager::receivedSynchronization()" << peerId;
    d->mSynchronizationState[peerId] = true;
    if (!d->mSynchronizationIsOnGoing) {
        d->mSynchronizationIsOnGoing = true;
    }
    checkSynchronizationIsDone(step);
}

void SynchronizationManager::requestSynchronization(Automatiq::DataProcessingSteps step)
{
    QString debugMsg = QDateTime::currentDateTime().toString(QStringLiteral("hh:mm:ss.zzz")) + QStringLiteral(" SynchronizationManager::requestSynchronization() ");
    switch(step)
    {
    case Automatiq::DataProcessingSteps::AfterProcessingDataSynchronization:
        debugMsg += QStringLiteral("AfterProcessingDataSynchronization");
        break;
    case Automatiq::DataProcessingSteps::AfterSendingDataSynchronization:
        debugMsg += QStringLiteral("AfterSendingDataSynchronization");
        break;
    case Automatiq::DataProcessingSteps::BeforeSynchronization:
        debugMsg += QStringLiteral("BeforeSynchronization");
        break;
    case Automatiq::DataProcessingSteps::Invalid:
        debugMsg += QStringLiteral("Invalid");
        break;
    case Automatiq::DataProcessingSteps::ProcessingData:
        debugMsg += QStringLiteral("ProcessingData");
        break;
    case Automatiq::DataProcessingSteps::SendingData:
        debugMsg += QStringLiteral("SendingData");
        break;
    case Automatiq::DataProcessingSteps::Waiting:
        debugMsg += QStringLiteral("Waiting");
        break;
    }
    qDebug() << debugMsg;

    d->mCurrentSynchronizationStep = step;

    if (!d->mProtocols.empty()) {
        for (auto it:d->mProtocols) {
            it->sendSynchronizationToRemotePeer(step);
        }
        d->mSynchronizationIsOnGoing = true;
        checkSynchronizationIsDone(step);
    } else {
        emitSynchronizationDone();
    }
}

void SynchronizationManager::checkSynchronizationIsDone(Automatiq::DataProcessingSteps step)
{
    QString helpMsg = QDateTime::currentDateTime().toString(QStringLiteral("hh:mm:ss.zzz")) + QStringLiteral(" SynchronizationManager::checkSynchronizationIsDone() ");
    switch(step)
    {
    case Automatiq::DataProcessingSteps::AfterProcessingDataSynchronization:
        helpMsg += QStringLiteral("AfterProcessingDataSynchronization");
        break;
    case Automatiq::DataProcessingSteps::AfterSendingDataSynchronization:
        helpMsg += QStringLiteral("AfterSendingDataSynchronization");
        break;
    case Automatiq::DataProcessingSteps::BeforeSynchronization:
        helpMsg += QStringLiteral("BeforeSynchronization");
        break;
    case Automatiq::DataProcessingSteps::Invalid:
        helpMsg += QStringLiteral("Invalid");
        break;
    case Automatiq::DataProcessingSteps::ProcessingData:
        helpMsg += QStringLiteral("ProcessingData");
        break;
    case Automatiq::DataProcessingSteps::SendingData:
        helpMsg += QStringLiteral("SendingData");
        break;
    case Automatiq::DataProcessingSteps::Waiting:
        helpMsg += QStringLiteral("Waiting");
        break;
    }
    qDebug() << helpMsg;

    bool isSynchronizationDone = (step == d->mCurrentSynchronizationStep);

    for(auto it = d->mSynchronizationState.begin(); it != d->mSynchronizationState.end(); ++it) {
        if (!it.value()) {
            qDebug() << QDateTime::currentDateTime().toString(QStringLiteral("hh:mm:ss.zzz")) << "missing:" << it.key();
            isSynchronizationDone = false;
        }
    }

    if (isSynchronizationDone) {
        emitSynchronizationDone();
    }
}

void SynchronizationManager::emitSynchronizationDone()
{
    QString debugMsg = QDateTime::currentDateTime().toString(QStringLiteral("hh:mm:ss.zzz")) + QStringLiteral(" SynchronizationManager::emitSynchronizationDone() ");
    switch(d->mCurrentSynchronizationStep)
    {
    case Automatiq::DataProcessingSteps::AfterProcessingDataSynchronization:
        debugMsg += QStringLiteral("AfterProcessingDataSynchronization");
        break;
    case Automatiq::DataProcessingSteps::AfterSendingDataSynchronization:
        debugMsg += QStringLiteral("AfterSendingDataSynchronization");
        break;
    case Automatiq::DataProcessingSteps::BeforeSynchronization:
        debugMsg += QStringLiteral("BeforeSynchronization");
        break;
    case Automatiq::DataProcessingSteps::Invalid:
        debugMsg += QStringLiteral("Invalid");
        break;
    case Automatiq::DataProcessingSteps::ProcessingData:
        debugMsg += QStringLiteral("ProcessingData");
        break;
    case Automatiq::DataProcessingSteps::SendingData:
        debugMsg += QStringLiteral("SendingData");
        break;
    case Automatiq::DataProcessingSteps::Waiting:
        debugMsg += QStringLiteral("Waiting");
        break;
    }
    qDebug() << debugMsg;

    d->mSynchronizationIsOnGoing = false;

    for(QMap<Automatiq::peerId, bool>::iterator it = d->mSynchronizationState.begin(); it != d->mSynchronizationState.end(); ++it) {
        it.value() = false;
    }

    Q_EMIT synchronizationDone(d->mCurrentSynchronizationStep);
    d->mCurrentSynchronizationStep = Automatiq::DataProcessingSteps::Invalid;
}

#include "moc_synchronizationmanager.cpp"
