/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_ABSTRACT_CONFIG_PROTOCOL_H
#define AUTOMATIQ_ABSTRACT_CONFIG_PROTOCOL_H

#include "automatiqexchanges_export.h"

#include "abstractprotocol.h"
#include "commondeclarations.h"

#include <QObject>

class AUTOMATIQEXCHANGES_EXPORT AbstractConfigProtocol : public AbstractProtocol
{
    Q_OBJECT
public:

    explicit AbstractConfigProtocol(Automatiq::peerId aRemotePeerId, QObject *parent = 0);

    virtual ~AbstractConfigProtocol();

Q_SIGNALS:

    void receivedConfiguration(Automatiq::peerId remotePeerId, const Automatiq::configuration_type &aConfiguration);

    void requestConfiguration(Automatiq::peerId remotePeerId, bool sendUpdateOnChange);

public Q_SLOTS:

    virtual void askRemotePeerConfiguration(bool sendUpdateOnChange) = 0;

    virtual void sendConfigurationToRemotePeer(const Automatiq::configuration_type &aConfiguration) = 0;

};

Q_DECLARE_METATYPE(AbstractConfigProtocol*)

#endif
