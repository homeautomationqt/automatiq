/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_AUTOMATIQ_CLIENT_H
#define AUTOMATIQ_AUTOMATIQ_CLIENT_H

#include "automatiqexchanges_export.h"

#include "automatiqnode.h"

#include "commondeclarations.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QSharedPointer>

class AutomatiqClientPrivate;

class ConfigProtocol;
class DataProtocol;
class ProtocolManager;
class PeerProtocol;
class DataManager;
class ClientConfigManager;

class AUTOMATIQEXCHANGES_EXPORT AutomatiqClient : public AutomatiqNode
{
    Q_OBJECT

    Q_PROPERTY(ClientConfigManager* configManager
               READ configManager
               NOTIFY configManagerChanged)

public:

    explicit AutomatiqClient(QObject *parent = 0);

    ~AutomatiqClient();

    void setHostPeerName(QString aHostPeerName) Q_DECL_OVERRIDE;

    Q_INVOKABLE void connectToPeer(const QString &hostName, const QString &peerAddress, quint16 port);

    ClientConfigManager* configManager();

Q_SIGNALS:

    void configManagerChanged();

private:

    AutomatiqClientPrivate *d;

};

#endif
