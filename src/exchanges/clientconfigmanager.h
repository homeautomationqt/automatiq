/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_CLIENT_CONFIG_MANAGER_H
#define AUTOMATIQ_CLIENT_CONFIG_MANAGER_H

#include "automatiqexchanges_export.h"

#include "clientmanager.h"

#include "commondeclarations.h"

#include <QtCore/QSharedPointer>

class ClientConfigManagerPrivate;
class AbstractPeerProtocol;
class AbstractConfigProtocol;
class AutomatiqClient;
class PropertyModel;

class AUTOMATIQEXCHANGES_EXPORT ClientConfigManager : public ClientManager
{
    Q_OBJECT

public:

    enum ColumnsRoles {
        NameRole = Qt::UserRole + 1,
        ValueRole,
        ReadOnlyRole,
    };

    typedef QMap<QString, QVariant> configuration_type;

    explicit ClientConfigManager(const QString &peerOwnName, QObject *parent = 0);

    explicit ClientConfigManager(QObject *parent = 0);

    virtual ~ClientConfigManager();

    QSharedPointer<AbstractConfigProtocol> getConfigProtocol(Automatiq::peerId peerId) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;

    void resetModel() Q_DECL_OVERRIDE;

    void newConnectedProtocol(Automatiq::peerId aPeerId, QSharedPointer<AbstractPeerProtocol> aProtocol) Q_DECL_OVERRIDE;

    Q_INVOKABLE QVariant dataByName(Automatiq::peerId selectedPeerId, const QString &name) const;

    Q_INVOKABLE PropertyModel* modelDataByName(Automatiq::peerId selectedPeerId, const QString &name) const;

Q_SIGNALS:

    void receivedRemotePeerConfiguration(Automatiq::peerId remotePeerId, const configuration_type &aConfiguration);

public Q_SLOTS:

    void receivedRemoteConfiguration(Automatiq::peerId remotePeerId, const configuration_type &aConfiguration);

    void remotePeerAskConfiguration(Automatiq::peerId remotePeerId, bool sendUpdateOnChange);

    void updateRemotePeerConfiguration(Automatiq::peerId aPeerId, const configuration_type &aConfiguration);

    void updateRemotePeerConfiguration(Automatiq::peerId aPeerId);

    void peerIsDisconnected();

private:

    ClientConfigManagerPrivate *d;
};

#endif
