/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "networkpeer.h"

#include "messagecontainer.h"

#include <QtNetwork/QTcpSocket>

#include <QtCore/QByteArray>
#include <QtCore/QDataStream>
#include <QtCore/QScopedPointer>
#include <QtCore/QDebug>

class NetworkPeerPrivate
{
public:

    NetworkPeerPrivate()
        : mPeerSocket(nullptr), mRemotePeerName(), mRemotePeerId(Automatiq::INVALID_PEER_ID), mMessageBeingRead(false),
          mFullMessageRead(false), mSizeOfMessage(), mSizeOfSpecificMessage(), mMessage(nullptr)
    {
    }

    ~NetworkPeerPrivate()
    {
        delete mMessage;
    }

    QTcpSocket *mPeerSocket;

    QString mRemotePeerName;

    Automatiq::peerId mRemotePeerId;

    bool mMessageBeingRead;
    bool mFullMessageRead;
    quint32 mSizeOfMessage;
    quint32 mSizeOfSpecificMessage;

    MessageContainer *mMessage;
};

NetworkPeer::NetworkPeer(QObject *parent)
    : QObject(parent), d(new NetworkPeerPrivate)
{
}

NetworkPeer::~NetworkPeer()
{
    delete d;
}

bool NetworkPeer::isConnected() const
{
    return d->mPeerSocket->state() == QAbstractSocket::ConnectedState;
}

bool NetworkPeer::isDisconnected() const
{
    return d->mPeerSocket->state() == QAbstractSocket::UnconnectedState;
}

Automatiq::peerId NetworkPeer::peerId() const
{
    return d->mRemotePeerId;
}

const QString& NetworkPeer::peerName() const
{
    return d->mRemotePeerName;
}

void NetworkPeer::setPeerName(const QString &aName)
{
    Q_EMIT peerNameWillChange(d->mRemotePeerName, aName);
    d->mRemotePeerName = aName;
    Q_EMIT peerNameChanged(d->mRemotePeerName);
}

void NetworkPeer::setPeerId(Automatiq::peerId aId)
{
    d->mRemotePeerId = aId;
    Q_EMIT peerIdChanged(d->mRemotePeerId);
}

QTcpSocket *NetworkPeer::peerSocket()
{
    return d->mPeerSocket;
}

void NetworkPeer::setPeerSocket(QTcpSocket *aClientSocket)
{
    if (d->mPeerSocket) {
        disconnect(d->mPeerSocket, 0, this, 0);
    }

    delete d->mPeerSocket;

    d->mPeerSocket = aClientSocket;
    connect(d->mPeerSocket, &QTcpSocket::readyRead,
            this, &NetworkPeer::newDataToRead);
    connect(d->mPeerSocket, &QTcpSocket::disconnected,
            this, &NetworkPeer::socketDisconnected);
    connect(d->mPeerSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(socketError(QAbstractSocket::SocketError)));
    connect(d->mPeerSocket, &QTcpSocket::connected,
            this, &NetworkPeer::peerIsConnected);

    Q_EMIT peerSocketChanged();
}

void NetworkPeer::disconnectFromPeer()
{
    if (d->mPeerSocket) {
        d->mPeerSocket->disconnectFromHost();
    }
}

void NetworkPeer::sendMessage(QSharedPointer<const MessageContainer> message)
{
    if (d->mPeerSocket) {
        QDataStream myOutStream(d->mPeerSocket);
        myOutStream << message->length() << message->version() << message->sequenceNumber() << message->identifier();
        myOutStream << message->protocolIdentifier() << message->specificLength() << message->specificBuffer();
    }
}

void NetworkPeer::newDataToRead()
{
    if (d->mPeerSocket) {
        if (d->mPeerSocket->state() != QAbstractSocket::ConnectedState) {
            return;
        }

        d->mMessageBeingRead = true;

        if (!d->mMessage && d->mPeerSocket->bytesAvailable() < MessageContainer::headerLength) {
            return;
        }

        if (d->mMessage && d->mPeerSocket->bytesAvailable() >= qint64(sizeof(quint32) + d->mSizeOfSpecificMessage)) {
            return;
        }

        QDataStream myOutStream(d->mPeerSocket);

        if (d->mMessage) {
            myOutStream >> d->mMessage->specificBuffer();
            if (myOutStream.status() == QDataStream::Ok) {
                d->mFullMessageRead = true;
                Q_EMIT receivedNewMessage(QSharedPointer<MessageContainer>(d->mMessage));
                d->mMessage = nullptr;
            } else {
                delete d->mMessage;
                d->mMessage = nullptr;
                Q_EMIT newReadError(d->mPeerSocket->error());
            }

            d->mMessageBeingRead = false;

            if (d->mPeerSocket->bytesAvailable() > 0) {
                newDataToRead();
            }
        } else {
            d->mMessage = new MessageContainer;

            quint64 newIdentifier;
            quint64 newProtocolIdentifier;
            quint64 newSequenceNumber;
            myOutStream >> d->mSizeOfMessage >> d->mMessage->version() >> newSequenceNumber >> newIdentifier >> newProtocolIdentifier >> d->mSizeOfSpecificMessage;
            if (myOutStream.status() == QDataStream::Ok) {
                d->mMessage->setIdentifier(newIdentifier);
                d->mMessage->setProtocolIdentifier(newProtocolIdentifier);
                d->mMessage->setSequenceNumber(newSequenceNumber);
                if (d->mPeerSocket->bytesAvailable() >= qint64(sizeof(quint32) + d->mSizeOfSpecificMessage)) {
                    myOutStream >> d->mMessage->specificBuffer();
                    if (myOutStream.status() == QDataStream::Ok) {
                        d->mFullMessageRead = true;
                        Q_EMIT receivedNewMessage(QSharedPointer<MessageContainer>(d->mMessage));
                        d->mMessage = nullptr;
                    } else {
                        delete d->mMessage;
                        d->mMessage = nullptr;
                        Q_EMIT newReadError(d->mPeerSocket->error());
                    }

                    d->mMessageBeingRead = false;
                }
            } else {
                delete d->mMessage;
                d->mMessage = nullptr;
                Q_EMIT newReadError(d->mPeerSocket->error());
            }
            if (d->mPeerSocket->bytesAvailable() > 0) {
                newDataToRead();
            }
        }
    }
}

void NetworkPeer::socketDisconnected()
{
    if (d->mMessageBeingRead) {
        delete d->mMessage;
        d->mMessage = nullptr;
        d->mMessageBeingRead = false;
    }
    Q_EMIT peerIsDisconnected(d->mPeerSocket->error());
}

void NetworkPeer::socketError(QAbstractSocket::SocketError socketError)
{
    if (d->mMessageBeingRead) {
        delete d->mMessage;
        d->mMessage = nullptr;
        d->mMessageBeingRead = false;
    }
    Q_EMIT newReadError(socketError);
}

#include "moc_networkpeer.cpp"
