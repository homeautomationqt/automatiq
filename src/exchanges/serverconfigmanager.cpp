/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "serverconfigmanager.h"
#include "abstractconfigprotocol.h"
#include "abstractpeerprotocol.h"
#include "featuremanager.h"

#include <QtCore/QSet>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QSharedPointer>
#include <QtCore/QVariant>
#include <QtCore/QDebug>

class ServerConfigManagerPrivate : public FeatureManager<AbstractConfigProtocol>
{
public:

    ServerConfigManagerPrivate() : FeatureManager<AbstractConfigProtocol>(), mConfiguration(), mNotifyPeerOnConfigurationChange(), mPeerOwnName()
    {
    }

    Automatiq::configuration_type mConfiguration;

    QSet<Automatiq::peerId> mNotifyPeerOnConfigurationChange;

    QString mPeerOwnName;
};

ServerConfigManager::ServerConfigManager(QObject *parent)
    : QObject(parent), d(new ServerConfigManagerPrivate)
{
}

ServerConfigManager::ServerConfigManager(const QString &peerOwnName, QObject *parent)
    : QObject(parent), d(new ServerConfigManagerPrivate)
{
    d->mPeerOwnName = peerOwnName;
}

ServerConfigManager::~ServerConfigManager()
{
    delete d;
}

const QString &ServerConfigManager::peerOwnName() const
{
    return d->mPeerOwnName;
}

void ServerConfigManager::setPeerOwnName(const QString &peerName)
{
    d->mPeerOwnName = peerName;
}

void ServerConfigManager::initialize()
{
    emit newConfigurationUpdate(d->mConfiguration);
}

void ServerConfigManager::newConnectedProtocol(Automatiq::peerId remotePeerId, QSharedPointer<AbstractPeerProtocol> aProtocol)
{
    d->mProtocols[remotePeerId] = aProtocol->createSubConfigProtocol(peerOwnName());
    QSharedPointer<AbstractConfigProtocol> &newProtocol(d->mProtocols[remotePeerId]);

    connect(newProtocol.data(), &AbstractConfigProtocol::requestConfiguration, this, &ServerConfigManager::remotePeerAskConfiguration);
    connect(newProtocol.data(), &AbstractConfigProtocol::receivedConfiguration, this, &ServerConfigManager::updateConfiguration);
    connect(newProtocol.data(), &AbstractConfigProtocol::peerConnectionClosed, this, &ServerConfigManager::peerIsDisconnected);

    aProtocol->registerSubProtocol(newProtocol->protocolId(), newProtocol);
}

void ServerConfigManager::remotePeerAskConfiguration(Automatiq::peerId remotePeerId, bool sendUpdateOnChange)
{
    auto itProtocol = d->mProtocols.find(remotePeerId);
    if (itProtocol != d->mProtocols.end()) {
        itProtocol.value()->sendConfigurationToRemotePeer(d->mConfiguration);
        if (sendUpdateOnChange) {
            d->mNotifyPeerOnConfigurationChange.insert(remotePeerId);
        }
    }
}

void ServerConfigManager::peerIsDisconnected()
{
    auto disconnectedPeerId = d->peerIsDisconnected(sender());

    if (disconnectedPeerId >= 0) {
        d->mNotifyPeerOnConfigurationChange.remove(disconnectedPeerId);
    }
}

void ServerConfigManager::updateConfiguration(Automatiq::peerId remotePeerId, const Automatiq::configuration_type &aNewConfiguration)
{
    d->mConfiguration = aNewConfiguration;
    for (auto itPeer = d->mNotifyPeerOnConfigurationChange.begin(); itPeer != d->mNotifyPeerOnConfigurationChange.end(); ++itPeer) {
        auto itProtocol = d->mProtocols.find(*itPeer);
        if (itProtocol != d->mProtocols.end()) {
            itProtocol.value()->sendConfigurationToRemotePeer(d->mConfiguration);
        }
    }
    Q_EMIT newConfigurationUpdate(aNewConfiguration);
}

#include "moc_serverconfigmanager.cpp"
