/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "abstractpeerprotocol.h"

#include "networkpeer.h"
#include "messagecontainer.h"
#include "uniqueidentifier.h"
#include "abstractpeerprotocol.h"
#include "abstractprotocol.h"
#include "commondeclarations.h"

#include <QtCore/QDateTime>
#include <QtCore/QMap>

class AbstractPeerProtocolPrivate
{
public:

    AbstractPeerProtocolPrivate()
        : mNetworkPeer(), mInitState(AbstractPeerProtocol::ProtocolInitializationState::NotConnected),
          mMessagesWithoutAcknowledge(), mOwnName(), mLastSequenceNumber(0)
    {
    }

    ~AbstractPeerProtocolPrivate()
    {
    }

    QSharedPointer<NetworkPeer> mNetworkPeer;

    AbstractPeerProtocol::ProtocolInitializationState mInitState;

    QMap<quint64, QSharedPointer<MessageContainer> > mMessagesWithoutAcknowledge;

    QMap<quint64, QSharedPointer<AbstractProtocol> > mProtocols;

    QString mOwnName;

    quint64 mLastSequenceNumber;
};

AbstractPeerProtocol::AbstractPeerProtocol(QSharedPointer<NetworkPeer> networkPeer, QObject *parent) :
    QObject(parent), d(new AbstractPeerProtocolPrivate)
{
    d->mNetworkPeer = networkPeer;
}

AbstractPeerProtocol::~AbstractPeerProtocol()
{
    delete d;
}

void AbstractPeerProtocol::newClientMessage(QSharedPointer<const MessageContainer> receivedMessage)
{
    switch (d->mInitState) {
    case ProtocolInitializationState::NotConnected:
    {
        sendError(Automatiq::ERROR_TYPE::UnexpectedMessage, receivedMessage->sequenceNumber());

        break;
    }
    case ProtocolInitializationState::NotInitialized:
    {
        manageNotInitializedProtocol(receivedMessage);
        break;
    }
    case ProtocolInitializationState::Initialized:
    {
        manageInitializedProtocol(receivedMessage);
        break;
    }
    }
}

void AbstractPeerProtocol::registerSubProtocol(quint64 aProtocolIdentifier, QSharedPointer<AbstractProtocol> aProtocol)
{
    d->mProtocols[aProtocolIdentifier] = aProtocol;
    connect(d->mProtocols[aProtocolIdentifier].data(), &AbstractProtocol::newMessageToSend, this, &AbstractPeerProtocol::sendMessageFromSubProtocol);
    connect(this, &AbstractPeerProtocol::peerConnectionClosed, d->mProtocols[aProtocolIdentifier].data(), &AbstractProtocol::peerIsDisconnected);
}

void AbstractPeerProtocol::peerIsDisconnected()
{
    Q_EMIT peerConnectionClosed();
}

void AbstractPeerProtocol::setOwnName(const QString &value)
{
    d->mOwnName = value;
}

void AbstractPeerProtocol::setInitState(AbstractPeerProtocol::ProtocolInitializationState value)
{
    d->mInitState = value;
}

AbstractPeerProtocol::ProtocolInitializationState AbstractPeerProtocol::initState() const
{
    return d->mInitState;
}

quint64& AbstractPeerProtocol::lastSequenceNumber()
{
    return d->mLastSequenceNumber;
}

QSharedPointer<MessageContainer> AbstractPeerProtocol::getAcknowledgedMessage(quint64 sequenceNumber)
{
    auto itAck = d->mMessagesWithoutAcknowledge.find(sequenceNumber);
    if (itAck != d->mMessagesWithoutAcknowledge.end()) {
        return itAck.value();
    } else {
        return QSharedPointer<MessageContainer>();
    }
}

QSharedPointer<AbstractProtocol> AbstractPeerProtocol::getSubProtocol(quint64 protocolIdentifier)
{
    auto itProtocol = d->mProtocols.find(protocolIdentifier);
    if (d->mProtocols.end() != itProtocol) {
        return *itProtocol;
    } else {
        return QSharedPointer<AbstractProtocol>();
    }
}

QSharedPointer<NetworkPeer> AbstractPeerProtocol::networkPeer()
{
    return d->mNetworkPeer;
}

void AbstractPeerProtocol::sendMessageFromSubProtocol(QSharedPointer<MessageContainer> message)
{
    sendMessage(message);
}

void AbstractPeerProtocol::sendMessage(QSharedPointer<MessageContainer> messageToSend)
{
    messageToSend->setSequenceNumber(d->mLastSequenceNumber);
    ++d->mLastSequenceNumber;

    d->mMessagesWithoutAcknowledge[messageToSend->sequenceNumber()] = messageToSend;

    Q_EMIT newMessageToSend(d->mMessagesWithoutAcknowledge[messageToSend->sequenceNumber()]);
}

void AbstractPeerProtocol::setLastSequenceNumber(quint64 aSequenceNumber) const
{
    d->mLastSequenceNumber = aSequenceNumber;
}

quint64 AbstractPeerProtocol::lastSequenceNumber() const
{
    return d->mLastSequenceNumber;
}

const QString &AbstractPeerProtocol::remotePeerName() const
{
    return d->mNetworkPeer->peerName();
}

Automatiq::peerId AbstractPeerProtocol::remotePeerId() const
{
    return d->mNetworkPeer->peerId();
}

void AbstractPeerProtocol::disconnectFromPeer()
{
    d->mNetworkPeer->disconnectFromPeer();
}

AbstractProtocolInterface::~AbstractProtocolInterface()
{
}


#include "moc_abstractpeerprotocol.cpp"
