/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_RUNTIME_MANAGER_H
#define AUTOMATIQ_RUNTIME_MANAGER_H

#include "automatiqexchanges_export.h"

#include "commondeclarations.h"

#include <QObject>

class RuntimeManagerPrivate;
class SubscribedVariableRecord;
class DataManager;
class SynchronizationManager;
class AutomatiqNode;
class AutomatiqProgramManager;

class AUTOMATIQEXCHANGES_EXPORT RuntimeManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(AutomatiqNode* hostNode
               READ hostNode
               WRITE setHostNode
               NOTIFY hostNodeChanged)

    Q_PROPERTY(DataManager* dataManager
               READ dataManager
               NOTIFY dataManagerChanged)

    Q_PROPERTY(SynchronizationManager* synchronizationManager
               READ synchronizationManager
               NOTIFY synchronizationManagerChanged)

    Q_PROPERTY(AutomatiqProgramManager* automatiqProgramManager
               READ automatiqProgramManager
               NOTIFY automatiqProgramManagerChanged)

    Q_PROPERTY(QString hostPeerName
               READ hostPeerName
               WRITE setHostPeerName
               NOTIFY hostPeerNameChanged)

public:
    explicit RuntimeManager(AutomatiqNode *hostNode = 0, QObject *parent = 0);

    virtual ~RuntimeManager();

    AutomatiqNode* hostNode() const;

    void setHostNode(AutomatiqNode *node);

    Q_INVOKABLE bool registerVariable(QObject *holder, const QString &variableName);

    Q_INVOKABLE QSharedPointer<SubscribedVariableRecord> subscribeVariable(const QString &sourceObjectName, const QString &sourceVariableName);

    Q_INVOKABLE bool unsubscribeVariable(const QString &sourceObjectName, const QString &sourceVariableName);

    DataManager* dataManager();

    SynchronizationManager* synchronizationManager();

    AutomatiqProgramManager* automatiqProgramManager();

    const QString& hostPeerName() const;

    void setHostPeerName(QString aHostPeerName);

    Q_INVOKABLE void initialize();

Q_SIGNALS:

    void synchronizationPeriodChangedMilliseconds(int newValue);

    void dataManagerChanged();

    void synchronizationManagerChanged();

    void automatiqProgramManagerChanged();

    void hostNodeChanged();

    void hostPeerNameChanged();

    void triggerProgramExecution();

public Q_SLOTS:

    void synchronizationIsDone(Automatiq::DataProcessingSteps step);

    void launchDataProcessing();

    void programExecutionIsFinished();

private:

    void triggerSynchronization(Automatiq::DataProcessingSteps synchronizationStep);

    RuntimeManagerPrivate *d;

};

#endif // RUNTIMEMANAGER_H
