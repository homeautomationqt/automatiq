/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "propertymodel.h"

class PropertyModelPrivate
{
public:

    explicit PropertyModelPrivate(QVariant &data) : mData(data), mListData(mData.toList()), mKeys(), mRoles()
    {
        if (mListData.empty()) {
            return;
        }

        mKeys = mListData.at(0).toMap().keys();

        for(int index = 0; index < mKeys.count(); ++index) {
            mRoles[index] = mKeys[index].toLocal8Bit();
        }
    }

    QVariant &mData;

    QList<QVariant> mListData;

    QList<QString> mKeys;

    QHash<int, QByteArray> mRoles;
};

PropertyModel::PropertyModel(QVariant &data, QObject *parent)
    : QAbstractListModel(parent), d(new PropertyModelPrivate(data))
{
}

PropertyModel::~PropertyModel()
{
    delete d;
}

int PropertyModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    } else {
        return d->mListData.count();
    }
}

QVariant PropertyModel::data(const QModelIndex &index, int role) const
{
    if (index.parent().isValid()) {
        return QVariant();
    }

    if (index.column() != 0) {
        return QVariant();
    }

    if (index.row() < 0 || index.row() > d->mListData.size() - 1) {
        return QVariant();
    }

    if (role < 0 || role > d->mKeys.count() - 1) {
        return QVariant();
    }

    return d->mListData[index.row()].toMap()[d->mKeys[role]];
}

bool PropertyModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.parent().isValid()) {
        return false;
    }

    if (index.column() != 0) {
        return false;
    }

    if (index.row() < 0 || index.row() > d->mListData.size() - 1) {
        return false;
    }

    if (!d->mListData[index.row()].canConvert<QMap<QString, QVariant> >()) {
        return false;
    }

    auto oldData = d->mListData[index.row()].toMap();

    if (role < 0 || role > d->mKeys.count() - 1) {
        return false;
    }

    oldData[d->mKeys[role]] = value;
    d->mListData[index.row()] = oldData;
    d->mData = d->mListData;

    Q_EMIT dataChanged(index, index, {role});

    return true;
}

bool PropertyModel::insertRows(int row, int count, const QModelIndex &parent)
{
    if (parent.isValid() && !parent.parent().isValid()) {
        return false;
    }

    if (row < 0 || row > d->mListData.count()) {
        return false;
    }

    if (count < 0) {
        return false;
    }

    beginInsertRows(parent, row, row + count - 1);
    for (int newRowCount = 0; newRowCount < count; ++newRowCount) {
        QMap<QString, QVariant> newRow;
        for (auto keyName : d->mKeys) {
            newRow[keyName];
        }

        d->mListData.insert(row, newRow);
    }
    d->mData = d->mListData;
    endInsertRows();

    return true;
}

bool PropertyModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (parent.isValid() && !parent.parent().isValid()) {
        return false;
    }

    if (row < 0 || row > d->mListData.count()) {
        return false;
    }

    if (count < 0) {
        return false;
    }

    beginRemoveRows(parent, row, row + count - 1);
    for (int newRowCount = 0; newRowCount < count; ++newRowCount) {
        d->mListData.removeAt(row);
    }
    d->mData = d->mListData;
    endRemoveRows();

    return true;
}

QHash<int, QByteArray> PropertyModel::roleNames() const
{
    return d->mRoles;
}

void PropertyModel::setRoleNames(const QList<QString> &roleNames)
{
    if (d->mKeys.empty()) {
        d->mKeys = roleNames;
    }

    for(int index = 0; index < d->mKeys.count(); ++index) {
        d->mRoles[index] = d->mKeys[index].toLocal8Bit();
    }
}
