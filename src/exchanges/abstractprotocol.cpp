/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "abstractprotocol.h"

#include "messagecontainer.h"

class AbstractProtocolPrivate
{
public:

    AbstractProtocolPrivate()
    {
    }

    Automatiq::peerId mRemotePeerId;
};

AbstractProtocol::AbstractProtocol(Automatiq::peerId aRemotePeerId, QObject *parent) :
    QObject(parent), d(new AbstractProtocolPrivate)
{
    d->mRemotePeerId = aRemotePeerId;
}

AbstractProtocol::~AbstractProtocol()
{
}

Automatiq::peerId AbstractProtocol::peerId() const
{
    return d->mRemotePeerId;
}

void AbstractProtocol::peerIsDisconnected()
{
    Q_EMIT peerConnectionClosed();
}

#include "moc_abstractprotocol.cpp"
