/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "protocolmanager.h"

#include "abstractpeerprotocol.h"
#include "networkpeer.h"
#include "messagecontainer.h"

#include <QtCore/QSharedPointer>
#include <QtCore/QString>
#include <QtCore/QMap>
#include <QtCore/QPluginLoader>

#include <QtCore/QDebug>

class ProtocolManagerPrivate
{
public:

    ProtocolManagerPrivate() : mPeerOwnName(), mPlugin(QStringLiteral("libAutomatiqProtocol.so")), mProtocolPlugin(nullptr)
    {
        mPlugin.load();
        if (mPlugin.isLoaded()) {
            mProtocolPlugin = qobject_cast<AbstractProtocolInterface*>(mPlugin.instance());
        } else {
            qDebug() << "ProtocolManagerPrivate::ProtocolManagerPrivate" << mPlugin.errorString();
        }
    }

    ~ProtocolManagerPrivate()
    {
        mPlugin.unload();
    }

    QString mPeerOwnName;

    QMap<quint64, QSharedPointer<AbstractPeerProtocol> > mProtocolsFromId;

    QMap<QSharedPointer<NetworkPeer>, QSharedPointer<AbstractPeerProtocol> > mProtocols;

    QSharedPointer<AbstractPeerProtocol> protocolFromRawPeerPointer(NetworkPeer *rawPointer) const
    {
        for(auto it = mProtocols.begin(); it != mProtocols.end(); ++it) {
            if (it.key().data() == rawPointer) {
                return it.value();
            }
        }

        return QSharedPointer<AbstractPeerProtocol>();
    }

    QSharedPointer<AbstractPeerProtocol> protocolFromPeerId(Automatiq::peerId peerId) const
    {
        auto id = mProtocolsFromId.find(peerId);
        if (id != mProtocolsFromId.end()) {
            return id.value();
        }

        for (auto it = mProtocols.begin(); it != mProtocols.end(); ++it) {
            if (it.value()->remotePeerId() == peerId) {
                return it.value();
            }
        }

        return QSharedPointer<AbstractPeerProtocol>();
    }

    QPluginLoader mPlugin;

    AbstractProtocolInterface* mProtocolPlugin;
};

ProtocolManager::ProtocolManager(QObject *parent)
    : QObject(parent), d(new ProtocolManagerPrivate)
{
}

ProtocolManager::ProtocolManager(const QString &aPeerOwnName, QObject *parent)
    : QObject(parent), d(new ProtocolManagerPrivate)
{
    d->mPeerOwnName = aPeerOwnName;
}

ProtocolManager::~ProtocolManager()
{
    delete d;
}

void ProtocolManager::handleNewConnection(QSharedPointer<NetworkPeer> aPeer, PeerManager::ConnectionOrigin connectionOrigin)
{
    QSharedPointer<AbstractPeerProtocol> myNewProtocol(d->mProtocolPlugin->createAbstractPeerProtocol(aPeer));
    d->mProtocols[aPeer] = myNewProtocol;

    connect(aPeer.data(), &NetworkPeer::peerIsDisconnected, this, &ProtocolManager::peerIsDisconnected);
    connect(aPeer.data(), &NetworkPeer::receivedNewMessage, myNewProtocol.data(), &AbstractPeerProtocol::newClientMessage);
    connect(myNewProtocol.data(), &AbstractPeerProtocol::newMessageToSend, aPeer.data(), &NetworkPeer::sendMessage);
    connect(myNewProtocol.data(), &AbstractPeerProtocol::changeState, this, &ProtocolManager::protocolChangedState);

    Q_EMIT newConnectedProtocol(d->mProtocols[aPeer]);

    switch (connectionOrigin)
    {
    case PeerManager::ConnectionOrigin::IncomingConnection:
        myNewProtocol->initProtocolDialog(d->mPeerOwnName, aPeer->peerId(), AbstractPeerProtocol::NewPeerDialogInitialization::SendPeerId);
        d->mProtocolsFromId[aPeer->peerId()] = myNewProtocol;
        break;
    case PeerManager::ConnectionOrigin::OutgoingConnection:
        myNewProtocol->initProtocolDialog(d->mPeerOwnName, aPeer->peerId(), AbstractPeerProtocol::NewPeerDialogInitialization::WaitPeerId);
        break;
    }
}

void ProtocolManager::peerIsDisconnected()
{
    NetworkPeer *myPeerInError = qobject_cast<NetworkPeer*>(sender());

    if (myPeerInError) {
        QSharedPointer<AbstractPeerProtocol> foundProtocol(d->protocolFromRawPeerPointer(myPeerInError));
        if (!foundProtocol.isNull()) {
            foundProtocol->peerIsDisconnected();
            d->mProtocols.remove(d->mProtocols.key(foundProtocol));
            d->mProtocolsFromId.remove(foundProtocol->remotePeerId());
            Q_EMIT disconnectedProtocol(foundProtocol->remotePeerId());
        }
    }
}

void ProtocolManager::protocolChangedState(Automatiq::peerId peerId, AbstractPeerProtocol::ProtocolInitializationState newState)
{
    auto protocol = d->protocolFromPeerId(peerId);

    if (protocol) {
        switch (newState)
        {
        case AbstractPeerProtocol::ProtocolInitializationState::NotConnected:
            break;
        case AbstractPeerProtocol::ProtocolInitializationState::NotInitialized:
            break;
        case AbstractPeerProtocol::ProtocolInitializationState::Initialized:
            d->mProtocolsFromId[protocol->remotePeerId()] = protocol;
            Q_EMIT newInitializedProtocol(protocol->remotePeerId(), protocol);
            break;
        }
    }
}

const QString &ProtocolManager::peerOwnName() const
{
    return d->mPeerOwnName;
}

void ProtocolManager::setPeerOwnName(const QString &peerName)
{
    d->mPeerOwnName = peerName;
}

void ProtocolManager::disconnectFromPeer(Automatiq::peerId aPeerId)
{
    QSharedPointer<AbstractPeerProtocol> currentProtocol(d->protocolFromPeerId(aPeerId));
    if (currentProtocol) {
        currentProtocol->disconnectFromPeer();
    }
}

#include "moc_protocolmanager.cpp"
