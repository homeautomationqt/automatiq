/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_ABSTRACT_PROTOCOL_H
#define AUTOMATIQ_ABSTRACT_PROTOCOL_H

#include "automatiqexchanges_export.h"

#include "commondeclarations.h"

#include <QObject>
#include <QSharedPointer>

class AbstractProtocolPrivate;

class MessageContainer;

enum class ACKNOWLEDGE_TYPE;

class AUTOMATIQEXCHANGES_EXPORT AbstractProtocol : public QObject
{
    Q_OBJECT
public:

    explicit AbstractProtocol(Automatiq::peerId aRemotePeerId, QObject *parent = 0);

    virtual ~AbstractProtocol();

    Automatiq::peerId peerId() const;

    virtual quint64 protocolId() = 0;

Q_SIGNALS:

    void newMessageToSend(QSharedPointer<MessageContainer> message);

    void peerConnectionClosed();

public Q_SLOTS:

    virtual void protocolReceivedAcknowledge(QSharedPointer<const MessageContainer> acknowledge, QSharedPointer<const MessageContainer> originalMessage) = 0;

    virtual void protocolReceivedMessage(QSharedPointer<const MessageContainer> receivedMessage) = 0;

    virtual void sendAcknowledgement(ACKNOWLEDGE_TYPE ack, quint64 sequenceNumber) = 0;

    void peerIsDisconnected();

private:

    AbstractProtocolPrivate *d;
};

#endif
