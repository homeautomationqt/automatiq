/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_AUTOMATIQ_HOST_H
#define AUTOMATIQ_AUTOMATIQ_HOST_H

#include "automatiqexchanges_export.h"

#include "automatiqnode.h"

#include "commondeclarations.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QSharedPointer>
#include <QtNetwork/QHostAddress>

class AutomatiqHostPrivate;

class ProtocolManager;
class PeerProtocol;
class SubscribedVariableRecord;
class DataManager;
class ServerConfigManager;
class SynchronizationManager;

class AUTOMATIQEXCHANGES_EXPORT AutomatiqHost : public AutomatiqNode
{
    Q_OBJECT

    Q_PROPERTY(ServerConfigManager* configManager
               READ serverConfigManager)

public:

    explicit AutomatiqHost(QObject *parent = 0);

    ~AutomatiqHost();

    void setHostPeerName(QString aHostPeerName) Q_DECL_OVERRIDE;

    ServerConfigManager* serverConfigManager();

    Q_INVOKABLE void initialize(const QHostAddress &address = QHostAddress::Any, quint16 port = 0);

private:

    AutomatiqHostPrivate *d;

};

#endif
