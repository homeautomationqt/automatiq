/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_CLIENT_MANAGER_H
#define AUTOMATIQ_CLIENT_MANAGER_H

#include "automatiqexchanges_export.h"

#include "commondeclarations.h"

#include <QtCore/QAbstractListModel>
#include <QtCore/QSharedPointer>

class ClientManagerPrivate;
class AbstractPeerProtocol;

class AUTOMATIQEXCHANGES_EXPORT ClientManager : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(Automatiq::peerId peerIdFilter
               READ peerIdFilter
               WRITE setPeerIdFilter)

    Q_PROPERTY(QString peerOwnName
               READ peerOwnName
               WRITE setPeerOwnName)

public:

    explicit ClientManager(const QString &peerOwnName, QObject *parent = 0);

    explicit ClientManager(QObject *parent = 0);

    ~ClientManager();

    Automatiq::peerId peerIdFilter() const;

    void setPeerIdFilter(Automatiq::peerId aPeerId);

    Q_INVOKABLE virtual void resetModel() = 0;

    const QString& peerOwnName() const;

    void setPeerOwnName(const QString &peerName);

public Q_SLOTS:

    virtual void newConnectedProtocol(Automatiq::peerId aPeerId, QSharedPointer<AbstractPeerProtocol> aProtocol) = 0;

private:

    ClientManagerPrivate *d;
};

#endif
