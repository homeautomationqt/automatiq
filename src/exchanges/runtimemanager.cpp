/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "runtimemanager.h"

#include "datamanager.h"
#include "synchronizationmanager.h"
#include "automatiqnode.h"
#include "protocolmanager.h"
//#include "automatiqprogrammanager.h"

#include <QtCore/QTimer>
#include <QtCore/QDateTime>
#include <QtCore/QDebug>

class RuntimeManagerPrivate
{

public:

    RuntimeManagerPrivate(AutomatiqNode *hostNode)
        : mCurrentState(Automatiq::DataProcessingSteps::Waiting), mDataManager(),
          mSynchronizationManager(), /*mProgramManager(&mSynchronizationManager),*/
          mHostNode(hostNode)
    {
    }

    Automatiq::DataProcessingSteps mCurrentState;

    DataManager mDataManager;

    SynchronizationManager mSynchronizationManager;

    //AutomatiqProgramManager mProgramManager;

    AutomatiqNode* mHostNode;

};

RuntimeManager::RuntimeManager(AutomatiqNode *hostNode, QObject *parent) :
    QObject(parent), d(new RuntimeManagerPrivate(hostNode))
{
    connect(&d->mSynchronizationManager, &SynchronizationManager::synchronizationDone, this, &RuntimeManager::synchronizationIsDone);

    if (hostNode) {
        connect(hostNode->protocolManager(), &ProtocolManager::newInitializedProtocol, &d->mDataManager, &DataManager::newConnectedProtocol);
        connect(hostNode->protocolManager(), &ProtocolManager::newInitializedProtocol, &d->mSynchronizationManager, &SynchronizationManager::newConnectedProtocol);
    }

    //connect(this, &RuntimeManager::triggerProgramExecution, &d->mProgramManager, &AutomatiqProgramManager::launchProgramExecution, Qt::DirectConnection);
    //connect(&d->mProgramManager, &AutomatiqProgramManager::programExecutionIsFinished, this, &RuntimeManager::programExecutionIsFinished, Qt::DirectConnection);
}

RuntimeManager::~RuntimeManager()
{
    delete d;
}

AutomatiqNode *RuntimeManager::hostNode() const
{
    return d->mHostNode;
}

void RuntimeManager::setHostNode(AutomatiqNode *node)
{
    d->mHostNode = node;

    if (d->mHostNode) {
        connect(d->mHostNode->protocolManager(), &ProtocolManager::newInitializedProtocol, &d->mDataManager, &DataManager::newConnectedProtocol);
        connect(d->mHostNode->protocolManager(), &ProtocolManager::newInitializedProtocol, &d->mSynchronizationManager, &SynchronizationManager::newConnectedProtocol);
    }

    setHostPeerName(d->mHostNode->hostPeerName());

    Q_EMIT hostNodeChanged();
}

void RuntimeManager::synchronizationIsDone(Automatiq::DataProcessingSteps step)
{
    Q_UNUSED(step);

    switch (d->mCurrentState)
    {
    case Automatiq::DataProcessingSteps::BeforeSynchronization:
        d->mCurrentState = Automatiq::DataProcessingSteps::SendingData;
        d->mDataManager.triggerPeriodicVariablesUpdate();
        triggerSynchronization(Automatiq::DataProcessingSteps::AfterSendingDataSynchronization);
        break;
    case Automatiq::DataProcessingSteps::AfterSendingDataSynchronization:
        d->mCurrentState = Automatiq::DataProcessingSteps::ProcessingData;
        Q_EMIT triggerProgramExecution();
        break;
    case Automatiq::DataProcessingSteps::AfterProcessingDataSynchronization:
        d->mCurrentState = Automatiq::DataProcessingSteps::Waiting;
        launchDataProcessing();
        break;
    default:
        break;
    }
}

void RuntimeManager::launchDataProcessing()
{
    triggerSynchronization(Automatiq::DataProcessingSteps::BeforeSynchronization);
}

void RuntimeManager::programExecutionIsFinished()
{
    triggerSynchronization(Automatiq::DataProcessingSteps::AfterProcessingDataSynchronization);
}

void RuntimeManager::triggerSynchronization(Automatiq::DataProcessingSteps synchronizationStep)
{
    qDebug() << QDateTime::currentDateTime().toString(QStringLiteral("hh:mm:ss.zzz")) << "AutomatiqNode::triggerSynchronization";

    /*if (d->mSynchronizationManager.isSynchronizationOnGoing()) {
        qDebug() << QDateTime::currentDateTime().toString(QStringLiteral("hh:mm:ss.zzz")) << "synchronization still ongoing";
        Q_ASSERT(false);
    }*/

    d->mCurrentState = synchronizationStep;
    d->mSynchronizationManager.requestSynchronization(synchronizationStep);
}

bool RuntimeManager::registerVariable(QObject *holder, const QString &variableName)
{
    return d->mDataManager.registerVariable(holder, variableName);
}

QSharedPointer<SubscribedVariableRecord> RuntimeManager::subscribeVariable(const QString &sourceObjectName, const QString &sourceVariableName)
{
    return d->mDataManager.subscribeVariable(sourceObjectName, sourceVariableName);
}

bool RuntimeManager::unsubscribeVariable(const QString &sourceObjectName, const QString &sourceVariableName)
{
    return d->mDataManager.unsubscribeVariable(sourceObjectName, sourceVariableName);
}

DataManager* RuntimeManager::dataManager()
{
    return &d->mDataManager;
}

SynchronizationManager* RuntimeManager::synchronizationManager()
{
    return &d->mSynchronizationManager;
}

AutomatiqProgramManager *RuntimeManager::automatiqProgramManager()
{
    return nullptr/*&d->mProgramManager*/;
}

const QString &RuntimeManager::hostPeerName() const
{
    return d->mDataManager.peerOwnName();
}

void RuntimeManager::setHostPeerName(QString aHostPeerName)
{
    d->mDataManager.setPeerOwnName(aHostPeerName);
    d->mSynchronizationManager.setPeerOwnName(aHostPeerName);
}

void RuntimeManager::initialize()
{
    //d->mProgramManager.initialize();
}

#include "moc_runtimemanager.cpp"
