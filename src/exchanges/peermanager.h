/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_PEER_MANAGER_H
#define AUTOMATIQ_PEER_MANAGER_H

#include "automatiqexchanges_export.h"

#include "networkpeer.h"

#include "commondeclarations.h"

#include <QtCore/QAbstractListModel>

class PeerManagerPrivate;

class QHostAddress;

class QTcpServer;

class PeerConnectionInformation;

class AUTOMATIQEXCHANGES_EXPORT PeerManager : public QAbstractListModel
{
    Q_OBJECT
public:
    enum class ConnectionOrigin
    {
        IncomingConnection,
        OutgoingConnection,
    };

    enum ColumnsRoles {
        NameRole = Qt::UserRole + 1,
        PeerNameRole,
        PeerIdRole,
    };

    explicit PeerManager(QObject *parent = 0);

    ~PeerManager();

    void setServer(QTcpServer *aServer);

    QSharedPointer<NetworkPeer> peerFromId(Automatiq::peerId peerId) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    Q_INVOKABLE Automatiq::peerId peerIdFromRow(int row);

Q_SIGNALS:

    void peerDisconnected(QSharedPointer<NetworkPeer> aPeer);

    void newPeer(QSharedPointer<NetworkPeer> aPeer, PeerManager::ConnectionOrigin connectionOrigin);

    void askUpdateConfiguration(Automatiq::peerId remotePeerId, const Automatiq::configuration_type &aNewConfiguration);

public Q_SLOTS:

    void connectToPeer(const QString &hostName, const QHostAddress &aAddress, quint16 port);

    void newPeerHasConnected();

    void configurationUpdate(const Automatiq::configuration_type &aNewConfiguration);

private Q_SLOTS:

    void peerFinishedConnecting();

    void peerIsInError(QAbstractSocket::SocketError socketError);

    void peerIsDisconnected(QAbstractSocket::SocketError socketError);

    void peerIdModified(Automatiq::peerId newId);

    void peerNameModified(const QString &newName);

    void reconnectTimer();

private:

    void doNewConnection(PeerConnectionInformation &newPeer);

    void updatePeersOutgoingConnectionsList(const Automatiq::configuration_type &aNewConfiguration,
                                            const QList<QVariant> &newList);

    PeerManagerPrivate *d;
};

#endif
