/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "automatiqnode.h"

#include "protocolmanager.h"
#include "abstractpeerprotocol.h"
#include "peermanager.h"

#include <QtNetwork/QHostAddress>

#include <QtCore/QDateTime>
#include <QtCore/QTimer>

class AutomatiqNodePrivate
{
public:

    AutomatiqNodePrivate()
        : mPeerManager(), mProtocolManager()
    {
    }

    ~AutomatiqNodePrivate()
    {
    }

    PeerManager mPeerManager;

    ProtocolManager mProtocolManager;

};

AutomatiqNode::AutomatiqNode(QObject *parent) : QObject(parent), d(new AutomatiqNodePrivate)
{
    connect(&d->mPeerManager, &PeerManager::newPeer, &d->mProtocolManager, &ProtocolManager::handleNewConnection);
    connect(&d->mProtocolManager, &ProtocolManager::newInitializedProtocol, this, &AutomatiqNode::handleNewConnectedPeer);
    connect(&d->mProtocolManager, &ProtocolManager::disconnectedProtocol, this, &AutomatiqNode::peerIsDisconnected);
}

AutomatiqNode::~AutomatiqNode()
{
    delete d;
}

const QString &AutomatiqNode::hostPeerName() const
{
    return d->mProtocolManager.peerOwnName();
}

void AutomatiqNode::setHostPeerName(QString aHostPeerName)
{
    if (d->mProtocolManager.peerOwnName() != aHostPeerName) {
        d->mProtocolManager.setPeerOwnName(aHostPeerName);
        Q_EMIT hostPeerNameChanged();
    }
}

void AutomatiqNode::initialize()
{
}

void AutomatiqNode::connectToPeer(const QString &hostName, const QHostAddress &peerAddress, quint16 port)
{
    d->mPeerManager.connectToPeer(hostName, peerAddress, port);
}

void AutomatiqNode::disconnectFromPeer(Automatiq::peerId peerId)
{
    d->mProtocolManager.disconnectFromPeer(peerId);
}

ProtocolManager *AutomatiqNode::protocolManager()
{
    return &d->mProtocolManager;
}

PeerManager *AutomatiqNode::peerManager()
{
    return &d->mPeerManager;
}

void AutomatiqNode::handleNewConnectedPeer(Automatiq::peerId aPeerId, QSharedPointer<AbstractPeerProtocol> aProtocol)
{
    Q_EMIT newConnectedPeer(aProtocol->remotePeerName(), aPeerId);
}

#include "moc_automatiqnode.cpp"
