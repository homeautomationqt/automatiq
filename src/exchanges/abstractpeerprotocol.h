/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_ABSTRACT_PEER_PROTOCOL_H
#define AUTOMATIQ_ABSTRACT_PEER_PROTOCOL_H

#include "automatiqexchanges_export.h"

#include "uniqueidentifier.h"

#include "commondeclarations.h"

#include <QObject>
#include <QtCore/QSharedPointer>

class AbstractPeerProtocolPrivate;
class NetworkPeer;
class MessageContainer;
class AbstractProtocol;
class AbstractConfigProtocol;
class AbstractDataProtocol;
class AbstractSynchronizationProtocol;

enum class ACKNOWLEDGE_TYPE;

namespace Automatiq
{

enum class ERROR_TYPE;

}

class AUTOMATIQEXCHANGES_EXPORT AbstractPeerProtocol : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString remotePeerName
               READ remotePeerName
               NOTIFY remotePeerNameChanged)

    Q_PROPERTY(Automatiq::peerId remotePeerId
               READ remotePeerId
               NOTIFY remotePeerIdChanged)

public:

    enum class ProtocolInitializationState
    {
        NotConnected = 0,
        NotInitialized = 1,
        Initialized = 2,
    };

    enum class NewPeerDialogInitialization
    {
        WaitPeerId,
        SendPeerId,
    };

    explicit AbstractPeerProtocol(QSharedPointer<NetworkPeer> networkPeer, QObject *parent = 0);

    virtual ~AbstractPeerProtocol();

    void setLastSequenceNumber(quint64 aSequenceNumber) const;

    quint64 lastSequenceNumber() const;

    quint64& lastSequenceNumber();

    const QString& remotePeerName() const;

    Automatiq::peerId remotePeerId() const;

    void disconnectFromPeer();

    virtual QSharedPointer<AbstractConfigProtocol> createSubConfigProtocol(const QString &peerOwnName) = 0;

    virtual QSharedPointer<AbstractDataProtocol> createSubDataProtocol(const QString &peerOwnName) = 0;

    virtual QSharedPointer<AbstractSynchronizationProtocol> createSubSynchronizationProtocol(const QString &peerOwnName) = 0;

Q_SIGNALS:

    void receivedPeerName(const QString &newPeerName, const QString &oldPeerName);

    void newMessageToSend(QSharedPointer<const MessageContainer> message);

    void changeState(Automatiq::peerId peerId, AbstractPeerProtocol::ProtocolInitializationState newState);

    void peerConnectionClosed();

    void remotePeerNameChanged();

    void remotePeerIdChanged();

public Q_SLOTS:

    virtual void initProtocolDialog(const QString &peerOwnName, Automatiq::peerId peerId, NewPeerDialogInitialization initializationMode) = 0;

    virtual void sendPeerName(const QString &peerName) = 0;

    void newClientMessage(QSharedPointer<const MessageContainer> receivedMessage);

    void registerSubProtocol(quint64 aProtocolIdentifier, QSharedPointer<AbstractProtocol> aProtocol);

    void sendMessageFromSubProtocol(QSharedPointer<MessageContainer> message);

    void peerIsDisconnected();

protected:

    virtual void sendError(Automatiq::ERROR_TYPE error, quint64 sequenceNumber) = 0;

    virtual void manageNotInitializedProtocol(QSharedPointer<const MessageContainer> aLastReceivedMessage) = 0;

    virtual void manageInitializedProtocol(QSharedPointer<const MessageContainer> aLastReceivedMessage) = 0;

    void setOwnName(const QString &value);

    void setInitState(AbstractPeerProtocol::ProtocolInitializationState value);

    AbstractPeerProtocol::ProtocolInitializationState initState() const;

    void sendMessage(QSharedPointer<MessageContainer> messageToSend);

    QSharedPointer<MessageContainer> getAcknowledgedMessage(quint64 sequenceNumber);

    QSharedPointer<AbstractProtocol> getSubProtocol(quint64 protocolIdentifier);

    QSharedPointer<NetworkPeer> networkPeer();

private:

    AbstractPeerProtocolPrivate *d;
};

class AUTOMATIQEXCHANGES_EXPORT AbstractProtocolInterface
{
public:
    virtual ~AbstractProtocolInterface();

    virtual QSharedPointer<AbstractPeerProtocol> createAbstractPeerProtocol(QSharedPointer<NetworkPeer> networkPeer, QObject *parent = 0) = 0;

};

Q_DECLARE_INTERFACE(AbstractProtocolInterface, "org.mgallien.AbstractProtocolInterface")

#endif // SERVERPROTOCOL_H
