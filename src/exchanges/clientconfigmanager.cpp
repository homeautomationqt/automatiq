/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "clientconfigmanager.h"
#include "abstractconfigprotocol.h"
#include "abstractpeerprotocol.h"
#include "featuremanager.h"
#include "automatiqclient.h"
#include "protocolmanager.h"
#include "propertymodel.h"

#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QDebug>
#include <QtCore/QHash>
#include <QtCore/QByteArray>

class ClientConfigManagerPrivate : public FeatureManager<AbstractConfigProtocol>
{
public:

    ClientConfigManagerPrivate()
        : FeatureManager<AbstractConfigProtocol>(), mReceivedConfig(), mFilteredKeys(), mFilteredValues()
    {
    }

    typedef QMap<QPair<Automatiq::peerId, QString>, QVariant> configuration_type;

    configuration_type mReceivedConfig;

    QList<Automatiq::configuration_type::key_type> mFilteredKeys;

    QList<Automatiq::configuration_type::mapped_type> mFilteredValues;

    Automatiq::configuration_type getPeerConfiguration(Automatiq::peerId peer)
    {
        Automatiq::configuration_type result;

        for (auto itNewConfig = mReceivedConfig.begin(); itNewConfig != mReceivedConfig.end(); ++itNewConfig) {
            if (itNewConfig.key().first == peer) {
                result[itNewConfig.key().second] = itNewConfig.value();
            }
        }

        return result;
    }
};

ClientConfigManager::ClientConfigManager(const QString &peerOwnName, QObject *parent)
    : ClientManager(parent), d(new ClientConfigManagerPrivate)
{
}

ClientConfigManager::ClientConfigManager(QObject *parent)
    : ClientManager(parent), d(new ClientConfigManagerPrivate)
{
}

ClientConfigManager::~ClientConfigManager()
{
    delete d;
}

QSharedPointer<AbstractConfigProtocol> ClientConfigManager::getConfigProtocol(Automatiq::peerId peerId) const
{
    return d->getProtocol(peerId);
}

int ClientConfigManager::rowCount(const QModelIndex &parent) const
{
    if (parent.parent().isValid()) {
        return 0;
    } else {
        if (peerIdFilter() == Automatiq::INVALID_PEER_ID) {
            return 0;
        }

        if (!d->getProtocol(peerIdFilter())) {
            return 0;
        }

        return d->mFilteredKeys.size();
    }
}

QHash<int, QByteArray> ClientConfigManager::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[static_cast<int>(ColumnsRoles::NameRole)] = "name";
    roles[static_cast<int>(ColumnsRoles::ValueRole)] = "value";
    roles[static_cast<int>(ColumnsRoles::ReadOnlyRole)] = "readOnly";
    return roles;
}

Qt::ItemFlags ClientConfigManager::flags(const QModelIndex &index) const
{
    if (peerIdFilter() == Automatiq::INVALID_PEER_ID) {
        return Qt::NoItemFlags;
    }

    if (!d->getProtocol(peerIdFilter())) {
        return Qt::NoItemFlags;
    }

    if (index.parent().isValid()) {
        return Qt::NoItemFlags;
    }

    if (index.column() != 0) {
        return Qt::NoItemFlags;
    }

    if (index.row() < 0 || index.row() > d->mReceivedConfig.size() - 1) {
        return Qt::NoItemFlags;
    }

    if (d->mFilteredValues[index.row()].toMap()[QStringLiteral("readOnly")].toBool()) {
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    } else {
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable;
    }
}

QVariant ClientConfigManager::data(const QModelIndex &index, int role) const
{
    if (peerIdFilter() == Automatiq::INVALID_PEER_ID) {
        return QVariant();
    }

    if (!d->getProtocol(peerIdFilter())) {
        return QVariant();
    }

    if (index.parent().isValid()) {
        return QVariant();
    }

    if (index.column() != 0) {
        return QVariant();
    }

    if (index.row() < 0 || index.row() > d->mFilteredKeys.size() - 1) {
        return QVariant();
    }

    switch(role)
    {
    case ColumnsRoles::NameRole:
        return d->mFilteredKeys[index.row()];
    case ColumnsRoles::ValueRole:
        return d->mFilteredValues[index.row()].toMap()[QStringLiteral("value")];
    case ColumnsRoles::ReadOnlyRole:
        return d->mFilteredValues[index.row()].toMap()[QStringLiteral("readOnly")];
    }

    return QVariant();
}

bool ClientConfigManager::setData(const QModelIndex &index, const QVariant &value, int role)
{
    static const QVector<int> changedRoles = {ColumnsRoles::ValueRole};

    if (peerIdFilter() == Automatiq::INVALID_PEER_ID) {
        return false;
    }

    if (!d->getProtocol(peerIdFilter())) {
        return false;
    }

    auto tempConfig = d->mReceivedConfig[{peerIdFilter(), d->mFilteredKeys[index.row()]}].toMap();
    tempConfig[QStringLiteral("value")] = value;

    d->mReceivedConfig[{peerIdFilter(), d->mFilteredKeys[index.row()]}] = tempConfig;
    d->mFilteredValues[index.row()] = tempConfig;

    Q_EMIT dataChanged(index, index, changedRoles);
    d->getProtocol(peerIdFilter())->sendConfigurationToRemotePeer(d->getPeerConfiguration(peerIdFilter()));

    return true;
}

void ClientConfigManager::resetModel()
{
    if (peerIdFilter() == Automatiq::INVALID_PEER_ID) {
        return;
    }

    if (!d->getProtocol(peerIdFilter())) {
        return;
    }

    if (d->getProtocol(peerIdFilter())) {
        d->getProtocol(peerIdFilter())->askRemotePeerConfiguration(true);
    }
}

void ClientConfigManager::newConnectedProtocol(Automatiq::peerId remotePeerId, QSharedPointer<AbstractPeerProtocol> aProtocol)
{
    d->mProtocols[remotePeerId] = aProtocol->createSubConfigProtocol(peerOwnName());
    QSharedPointer<AbstractConfigProtocol> &newProtocol(d->mProtocols[remotePeerId]);

    connect(newProtocol.data(), &AbstractConfigProtocol::requestConfiguration, this, &ClientConfigManager::remotePeerAskConfiguration);
    connect(newProtocol.data(), &AbstractConfigProtocol::receivedConfiguration, this, &ClientConfigManager::receivedRemoteConfiguration);
    connect(newProtocol.data(), &AbstractConfigProtocol::peerConnectionClosed, this, &ClientConfigManager::peerIsDisconnected);

    aProtocol->registerSubProtocol(newProtocol->protocolId(), newProtocol);
}

QVariant ClientConfigManager::dataByName(Automatiq::peerId selectedPeerId, const QString &name) const
{
    auto itKey = d->mReceivedConfig.find({selectedPeerId, name});
    if (itKey != d->mReceivedConfig.end()) {
        return itKey.value();
    }

    return QVariant();
}

PropertyModel* ClientConfigManager::modelDataByName(Automatiq::peerId selectedPeerId, const QString &name) const
{
    auto itKey = d->mReceivedConfig.find({selectedPeerId, name});
    if (itKey == d->mReceivedConfig.end()) {
        d->mReceivedConfig[{selectedPeerId, name}];
    }

    itKey = d->mReceivedConfig.find({selectedPeerId, name});

    PropertyModel *newModel = new PropertyModel(itKey.value());

    return newModel;
}

void ClientConfigManager::peerIsDisconnected()
{
    auto remotePeerId = d->peerIsDisconnected(sender());
}

void ClientConfigManager::updateRemotePeerConfiguration(Automatiq::peerId aPeerId, const configuration_type &aConfiguration)
{
    auto itProtocol = d->mProtocols.find(aPeerId);
    if (itProtocol != d->mProtocols.end()) {
        itProtocol.value()->sendConfigurationToRemotePeer(aConfiguration);
    }
}

void ClientConfigManager::updateRemotePeerConfiguration(Automatiq::peerId aPeerId)
{
    updateRemotePeerConfiguration(aPeerId, d->getPeerConfiguration(aPeerId));
}

void ClientConfigManager::receivedRemoteConfiguration(Automatiq::peerId remotePeerId, const ClientConfigManager::configuration_type &aConfiguration)
{
    Q_EMIT receivedRemotePeerConfiguration(remotePeerId, aConfiguration);

    qDebug() << "ClientConfigManager::receivedRemoteConfiguration" << aConfiguration;

    QPair<int, QString> newKey({remotePeerId, QString()});
    for (auto itNewConfig = aConfiguration.begin(); itNewConfig != aConfiguration.end(); ++itNewConfig) {
        newKey.second = itNewConfig.key();
        d->mReceivedConfig[newKey] = itNewConfig.value();
    }

    if (peerIdFilter() == remotePeerId) {
        beginResetModel();

        d->mFilteredValues = aConfiguration.values();
        d->mFilteredKeys = aConfiguration.keys();

        endResetModel();
    }
}

void ClientConfigManager::remotePeerAskConfiguration(Automatiq::peerId remotePeerId, bool sendUpdateOnChange)
{
    Q_UNUSED(remotePeerId);
    Q_UNUSED(sendUpdateOnChange);
}

#include "moc_clientconfigmanager.cpp"
