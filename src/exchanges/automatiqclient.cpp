/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "automatiqclient.h"

#include "clientconfigmanager.h"
#include "protocolmanager.h"

class AutomatiqClientPrivate
{
public:

    AutomatiqClientPrivate()
        : mConfigManager()
    {
    }

    ~AutomatiqClientPrivate()
    {
    }

    ClientConfigManager mConfigManager;
};

AutomatiqClient::AutomatiqClient(QObject *parent) : AutomatiqNode(parent), d(new AutomatiqClientPrivate)
{
    connect(protocolManager(), &ProtocolManager::newInitializedProtocol, &d->mConfigManager, &ClientConfigManager::newConnectedProtocol);
}

AutomatiqClient::~AutomatiqClient()
{
    delete d;
}

void AutomatiqClient::setHostPeerName(QString aHostPeerName)
{
    if (d->mConfigManager.peerOwnName() != aHostPeerName) {
        d->mConfigManager.setPeerOwnName(aHostPeerName);
        AutomatiqNode::setHostPeerName(aHostPeerName);
    }
}

void AutomatiqClient::connectToPeer(const QString &hostName, const QString &peerAddress, quint16 port)
{
    peerManager()->connectToPeer(hostName, QHostAddress(peerAddress), port);
}

ClientConfigManager* AutomatiqClient::configManager()
{
    return &d->mConfigManager;
}

#include "moc_automatiqclient.cpp"
