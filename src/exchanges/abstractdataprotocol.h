/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_ABSTRACT_DATA_PROTOCOL_H
#define AUTOMATIQ_ABSTRACT_DATA_PROTOCOL_H

#include "automatiqexchanges_export.h"

#include "abstractprotocol.h"
#include "commondeclarations.h"

#include <QtCore/QList>

class DataVariableWithValue;
class DataVariableIdentifier;
class DataVariableDescription;
class DataVariableSubscription;
class DataVariableSubscriptionRequest;

class AUTOMATIQEXCHANGES_EXPORT AbstractDataProtocol : public AbstractProtocol
{
    Q_OBJECT
public:

    explicit AbstractDataProtocol(Automatiq::peerId aRemotePeerId, QObject *parent = 0);

    virtual ~AbstractDataProtocol();

Q_SIGNALS:

    void receivedVariableValues(Automatiq::peerId aRemotePeerId, const QList<DataVariableWithValue> &values);

    void receivedVariablesList(Automatiq::peerId aRemotePeerId, const QList<DataVariableDescription> &variables);

    void receivedRequestForListVariables(Automatiq::peerId aRemotePeerId);

    void receivedVariableSubscriptionsList(Automatiq::peerId aRemotePeerId, const QList<DataVariableSubscription> &variables);

    void receivedVariableSubscriptionRequestsList(Automatiq::peerId aRemotePeerId, const QList<DataVariableSubscriptionRequest> &requests);

public Q_SLOTS:

    virtual void askVariablesList() = 0;

    virtual void sendVariablesList(const QList<DataVariableDescription> &variables) = 0;

    virtual void sendVariableValues(const QList<DataVariableWithValue> &values) = 0;

    virtual void sendVariableSubscriptions(const QList<DataVariableSubscription> &variables) = 0;

    virtual void sendVariableSubscriptionRequests(const QList<DataVariableSubscriptionRequest> &requests) = 0;
};

Q_DECLARE_METATYPE(AbstractDataProtocol*)

#endif
