/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_MESSAGE_CONTAINER_H
#define AUTOMATIQ_MESSAGE_CONTAINER_H

#include "automatiqexchanges_export.h"

#include <QtCore/QPair>
#include <QtCore/QByteArray>
#include <QtCore/QBuffer>
#include <QtCore/QScopedPointer>
#include <QtCore/QDataStream>
#include <QtCore/QSharedPointer>

class MessageContainerPrivate;

class AUTOMATIQEXCHANGES_EXPORT MessageContainer
{
public:

    MessageContainer();

    MessageContainer(const MessageContainer &aOther);

    MessageContainer(MessageContainer &&aOther);

    ~MessageContainer();

    MessageContainer& operator=(const MessageContainer &aOther);

    MessageContainer& operator=(MessageContainer &&aOther);

    bool isValid() const;

    quint32 length() const;

    static const quint32 headerLength = 3 * sizeof(quint32) + 2 * sizeof(quint64);

    quint32 specificLength() const;

    const QByteArray& specificBuffer() const;

    QByteArray& specificBuffer();

    void setVersion(quint32 aVersion) const;

    quint32 version() const;

    quint32& version();

    void setSequenceNumber(quint64 aSequenceNumber) const;

    quint64 sequenceNumber() const;

    quint64& sequenceNumber();

    void setIdentifier(quint64 aIdentifier) const;

    quint64 identifier() const;

    quint64& identifier();

    void setProtocolIdentifier(quint64 aProtocolIdentifier) const;

    quint64 protocolIdentifier() const;

    quint64& protocolIdentifier();

    template <typename T>
    bool setSpecificMessage(const T &aSpecificMessage)
    {
        setIdentifier(aSpecificMessage.messageId);
        setProtocolIdentifier(aSpecificMessage.protocolId);

        QBuffer myBuffer(&specificBuffer());
        myBuffer.open(QIODevice::WriteOnly);
        QDataStream myWriteStream(&myBuffer);

        myWriteStream << aSpecificMessage;

        return true;
    }

    template <typename T>
    T* specificMessage() const
    {
        if (T::messageId != identifier()) {
            return nullptr;
        }

        QScopedPointer<T> result(new T);

        QDataStream myReadStream(specificBuffer());

        myReadStream >> (*result.data());

        return result.take();
    }

private:

    MessageContainerPrivate *d;
};

Q_DECLARE_METATYPE(QSharedPointer<MessageContainer>)

#endif
