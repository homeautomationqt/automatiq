/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_SYNCHRONIZATION_MANAGER_H
#define AUTOMATIQ_SYNCHRONIZATION_MANAGER_H

#include "automatiqexchanges_export.h"

#include "commondeclarations.h"

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

class SynchronizationManagerPrivate;
class AbstractPeerProtocol;

class AUTOMATIQEXCHANGES_EXPORT SynchronizationManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString peerOwnName
               READ peerOwnName
               WRITE setPeerOwnName)

public:

    explicit SynchronizationManager(const QString &peerOwnName, QObject *parent = 0);

    explicit SynchronizationManager(QObject *parent = 0);

    ~SynchronizationManager();

    bool isSynchronizationOnGoing() const;

    const QString& peerOwnName() const;

    void setPeerOwnName(const QString &peerName);

Q_SIGNALS:

    void synchronizationDone(Automatiq::DataProcessingSteps step);

public Q_SLOTS:

    void newConnectedProtocol(Automatiq::peerId peerId, QSharedPointer<AbstractPeerProtocol> aProtocol);

    void peerIsDisconnected();

    void receivedSynchronization(Automatiq::DataProcessingSteps step, Automatiq::peerId peerId);

    void requestSynchronization(Automatiq::DataProcessingSteps step);

private:

    void checkSynchronizationIsDone(Automatiq::DataProcessingSteps step);

    void emitSynchronizationDone();

    SynchronizationManagerPrivate *d;
};

#endif
