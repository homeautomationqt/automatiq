project(AutomatiqExchanges)

set(CMAKE_CXX_STANDARD 11)

set(automatiqExchanges_LIB_SRCS
    datamanager.cpp
    networkpeer.cpp
    peermanager.cpp
    protocolmanager.cpp
    serverconfigmanager.cpp
    clientconfigmanager.cpp
    abstractprotocol.cpp
    featuremanager.cpp
    automatiqclient.cpp
    automatiqhost.cpp
    automatiqnode.cpp
    runtimemanager.cpp
    synchronizationmanager.cpp
    propertymodel.cpp
    clientmanager.cpp
    abstractpeerprotocol.cpp
    abstractdataprotocol.cpp
    abstractconfigprotocol.cpp
    abstractsynchronizationprotocol.cpp
    messagecontainer.cpp
)

ecm_generate_headers(automatiqExchanges_HEADERS
  HEADER_NAMES
  AbstractPeerProtocol
  AutomatiqNode
  RuntimeManager
  NetworkPeer
  PeerManager
  ProtocolManager
  ServerConfigManager
  ClientConfigManager
  ClientManager
  DataManager
  FeatureManager
  AutomatiqClient
  AutomatiqHost

  REQUIRED_HEADERS automatiqExchanges_HEADERS
)

ecm_setup_version(0.1.0 VARIABLE_PREFIX AUTOMATIQ_EXCHANGES
                        VERSION_HEADER "${CMAKE_CURRENT_BINARY_DIR}/automatiqexchanges_version.h"
                        PACKAGE_VERSION_FILE "${CMAKE_CURRENT_BINARY_DIR}/AutomatiqExchangesConfigVersion.cmake")

add_library(AutomatiqExchanges SHARED ${automatiqExchanges_LIB_SRCS})
generate_export_header(AutomatiqExchanges BASE_NAME AutomatiqExchanges
  EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/automatiqexchanges_export.h
)
add_library(Automatiq::Exchanges ALIAS AutomatiqExchanges)

target_link_libraries(AutomatiqExchanges PUBLIC Qt5::Core Automatiq::Core)
target_link_libraries(AutomatiqExchanges PRIVATE Qt5::Network)

target_include_directories(AutomatiqExchanges INTERFACE "$<INSTALL_INTERFACE:${INCLUDE_INSTALL_DIR}/Automatiq>")

set_target_properties(AutomatiqExchanges PROPERTIES VERSION   ${AUTOMATIQ_VERSION_STRING}
                                               SOVERSION ${AUTOMATIQ_SOVERSION}
                                               LINK_FLAGS "-Wl,--no-undefined")
 
install(TARGETS AutomatiqExchanges EXPORT AutomatiqTargets ${INSTALL_TARGETS_DEFAULT_ARGS})

install(FILES
  ${AutomatiqExchanges_BINARY_DIR}/automatiqexchanges_export.h
  ${automatiqExchanges_HEADERS}
  DESTINATION  ${INCLUDE_INSTALL_DIR}/Automatiq/automatiq/ COMPONENT Devel
)
