/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "datamanager.h"

#include "subscribedvariablerecord.h"
#include "featuremanager.h"
#include "abstractpeerprotocol.h"
#include "abstractdataprotocol.h"
#include "datavariabledescription.h"
#include "datavariableidentifier.h"
#include "datavariablewithvalue.h"
#include "datavariablesubscription.h"
#include "datavariablesubscriptionrequest.h"

#include <QtCore/QDebug>
#include <QtCore/QHash>
#include <QtCore/QMap>

#include <QtCore/QMetaProperty>

struct RegisteredVariableRecord
{
    QObject *mHolder;

    QMetaProperty mProperty;

    QList<Automatiq::peerId> mSubscribers;
};

class DataManagerPrivate : public FeatureManager<AbstractDataProtocol>
{
public:

    DataManagerPrivate()
        : FeatureManager<AbstractDataProtocol>(), mSubscriptions(), mFilteredKeys(), mFilteredValues(),
          mRegisteredVariables(), mPendingNotifications()
    {
    }

    QHash<quint64, QSharedPointer<SubscribedVariableRecord> > mSubscriptions;

    QList<quint64> mFilteredKeys;

    QList<QSharedPointer<SubscribedVariableRecord> > mFilteredValues;

    QHash<quint64, RegisteredVariableRecord> mRegisteredVariables;

    QList<DataVariableIdentifier> mPendingNotifications;
};

DataManager::DataManager(const QString &peerOwnName, QObject *parent)
    : ClientManager(peerOwnName, parent), d(new DataManagerPrivate)
{
}

DataManager::DataManager(QObject *parent)
    : ClientManager(parent), d(new DataManagerPrivate)
{
}

DataManager::~DataManager()
{
    delete d;
}

QSharedPointer<AbstractDataProtocol> DataManager::getDataProtocol(Automatiq::peerId peerId) const
{
    return d->getProtocol(peerId);
}

void DataManager::newConnectedProtocol(Automatiq::peerId remotePeerId, QSharedPointer<AbstractPeerProtocol> aProtocol)
{
    d->mProtocols[remotePeerId] = aProtocol->createSubDataProtocol(peerOwnName());
    QSharedPointer<AbstractDataProtocol> &newProtocol(d->mProtocols[remotePeerId]);

    connect(newProtocol.data(), &AbstractDataProtocol::peerConnectionClosed, this, &DataManager::peerIsDisconnected);
    connect(newProtocol.data(), &AbstractDataProtocol::receivedRequestForListVariables, this, &DataManager::receivedRequestForListVariables);
    connect(newProtocol.data(), &AbstractDataProtocol::receivedVariablesList, this, &DataManager::receivedVariablesList);
    connect(newProtocol.data(), &AbstractDataProtocol::receivedVariableValues, this, &DataManager::receivedVariableValues);
    connect(newProtocol.data(), &AbstractDataProtocol::receivedVariableSubscriptionsList, this, &DataManager::receivedVariableSubscriptionsList);
    connect(newProtocol.data(), &AbstractDataProtocol::receivedVariableSubscriptionRequestsList, this, &DataManager::receivedVariableSubscriptionRequestsList);

    aProtocol->registerSubProtocol(newProtocol->protocolId(), newProtocol);

    newProtocol->askVariablesList();
}

void DataManager::peerIsDisconnected()
{
    auto remotePeerId = d->peerIsDisconnected(sender());
    if (remotePeerId == Automatiq::INVALID_PEER_ID) {
        return;
    }

    if (peerIdFilter() == remotePeerId) {
        setPeerIdFilter(Automatiq::INVALID_PEER_ID);
        resetModel();
    }

    for (int itVariable = 0; itVariable < d->mFilteredValues.size(); ++itVariable) {
        if (d->mFilteredValues[itVariable]->sourcePeerId() == remotePeerId) {
            if (d->mFilteredValues[itVariable]->isSubscribed()) {
                d->mFilteredValues[itVariable]->unsubscribe();
            }
            d->mFilteredValues[itVariable]->setSourcePeerId(Automatiq::INVALID_PEER_ID);
            Q_EMIT dataChanged(index(itVariable, 0), index(itVariable, 0));
        }
    }
}

void DataManager::receivedVariableValues(Automatiq::peerId peerId, const QList<DataVariableWithValue> &values)
{
    for (auto itVariable = values.begin(); itVariable != values.end(); ++itVariable) {
        auto itSubscription = d->mSubscriptions.find(itVariable->identifier().identifier());
        const quint64 hashIdentifier = itVariable->identifier().identifier();
        if (itSubscription != d->mSubscriptions.end()) {
            if ((*itSubscription)->isSubscribed()) {
                (*itSubscription)->setValue(itVariable->value());
                if (peerId == peerIdFilter() || peerIdFilter() == Automatiq::INVALID_PEER_ID) {
                    int filteredIndex = d->mFilteredKeys.indexOf(hashIdentifier);
                    if (filteredIndex != -1) {
                        Q_EMIT dataChanged(index(filteredIndex, 0), index(filteredIndex, 0));
                    }
                }
            }
        }
    }
}

void DataManager::receivedVariablesList(Automatiq::peerId peerId, const QList<DataVariableDescription> &variables)
{
    QList<DataVariableSubscriptionRequest> requests;

    for (auto itVariable = variables.begin(); itVariable != variables.end(); ++itVariable) {
        const DataVariableIdentifier &variableIdentifier = itVariable->toIdentifier();
        const quint64 hashIdentifier = variableIdentifier.identifier();
        auto itSubscription = d->mSubscriptions.find(hashIdentifier);
        if (itSubscription == d->mSubscriptions.end()) {
            d->mSubscriptions[hashIdentifier].reset(new SubscribedVariableRecord(*itVariable));
            d->mSubscriptions[hashIdentifier]->setSourcePeerId(peerId);
            if (peerId == peerIdFilter() || peerIdFilter() == Automatiq::INVALID_PEER_ID) {
                beginInsertRows(QModelIndex(), d->mFilteredKeys.size(), d->mFilteredKeys.size());
                d->mFilteredKeys.push_back(hashIdentifier);
                d->mFilteredValues.push_back(d->mSubscriptions[hashIdentifier]);
                endInsertRows();
            }
        }
        itSubscription = d->mSubscriptions.find(hashIdentifier);
        if (itSubscription != d->mSubscriptions.end()) {
            QSharedPointer<SubscribedVariableRecord> currentSubscription = d->mSubscriptions[hashIdentifier];
            if (!currentSubscription->isSubscribed() &&
                (currentSubscription->sourcePeerId() == peerId || currentSubscription->sourcePeerId() == Automatiq::INVALID_PEER_ID)) {
                currentSubscription->setSourcePeerId(peerId);
                if (currentSubscription->autoSubscribe()) {
                    requests.push_back({variableIdentifier, DataVariableSubscriptionRequestType::Subscribe});
                }
                if (peerId == peerIdFilter() || peerIdFilter() == Automatiq::INVALID_PEER_ID) {
                    int filteredIndex = d->mFilteredKeys.indexOf(hashIdentifier);
                    if (filteredIndex != -1) {
                        d->mFilteredValues[filteredIndex] = currentSubscription;
                        Q_EMIT dataChanged(index(filteredIndex, 0), index(filteredIndex, 0));
                    }
                }
            }
        }
    }

    if (!requests.empty()) {
        QSharedPointer<AbstractDataProtocol> protocol = d->getProtocol(peerId);
        if (protocol) {
            protocol->sendVariableSubscriptionRequests(requests);
        }
    }
}

void DataManager::receivedRequestForListVariables(Automatiq::peerId peerId)
{
    QList<DataVariableDescription> allVariables;

    for (auto itVariable = d->mRegisteredVariables.begin(); itVariable != d->mRegisteredVariables.end(); ++itVariable) {
        allVariables.push_back(DataVariableDescription(itVariable.value().mHolder->objectName(),
                                                       QString::fromLatin1(itVariable.value().mProperty.name())));
    }

    QSharedPointer<AbstractDataProtocol> protocol = d->getProtocol(peerId);
    if (protocol) {
        protocol->sendVariablesList(allVariables);
    }
}

void DataManager::receivedVariableSubscriptionsList(Automatiq::peerId peerId, const QList<DataVariableSubscription> &variables)
{
    for (auto itVariable = variables.begin(); itVariable != variables.end(); ++itVariable) {
        auto itSubscription = d->mSubscriptions.find(itVariable->identifier().identifier());
        const quint64 hashIdentifier = itVariable->identifier().identifier();
        if (itSubscription != d->mSubscriptions.end()) {
            if (!(*itSubscription)->isSubscribed() && itVariable->isSubscribed() && (*itSubscription)->sourcePeerId() == peerId) {
                (*itSubscription)->subscribe();
                if (peerId == peerIdFilter() || peerIdFilter() == Automatiq::INVALID_PEER_ID) {
                    int filteredIndex = d->mFilteredKeys.indexOf(hashIdentifier);
                    if (filteredIndex != -1) {
                        Q_EMIT dataChanged(index(filteredIndex, 0), index(filteredIndex, 0));
                    }
                }
            } else {
                if ((*itSubscription)->isSubscribed() && !itVariable->isSubscribed()) {
                    (*itSubscription)->unsubscribe();
                    if (peerId == peerIdFilter() || peerIdFilter() == Automatiq::INVALID_PEER_ID) {
                        int filteredIndex = d->mFilteredKeys.indexOf(hashIdentifier);
                        if (filteredIndex != -1) {
                            Q_EMIT dataChanged(index(filteredIndex, 0), index(filteredIndex, 0));
                        }
                    }
                }
            }
        }
    }
}

void DataManager::receivedVariableSubscriptionRequestsList(Automatiq::peerId peerId, const QList<DataVariableSubscriptionRequest> &requests)
{
    QList<DataVariableSubscription> subscribedVariables;
    QList<DataVariableIdentifier> newSubscribedVariables;

    for (auto itRequest = requests.begin(); itRequest != requests.end(); ++itRequest) {
        const DataVariableIdentifier &requestIdentifier = itRequest->identifier();

        auto itRegisteredVariable = d->mRegisteredVariables.find(requestIdentifier.identifier());
        if (itRegisteredVariable != d->mRegisteredVariables.end()) {
            switch (itRequest->subscriptionRequestType())
            {
            case DataVariableSubscriptionRequestType::Subscribe:
                itRegisteredVariable.value().mSubscribers.push_back(peerId);
                newSubscribedVariables.push_back(requestIdentifier);
                subscribedVariables.push_back({requestIdentifier, true});
                break;
            case DataVariableSubscriptionRequestType::Unsubscribe:
                itRegisteredVariable.value().mSubscribers.removeAll(peerId);
                subscribedVariables.push_back({requestIdentifier, false});
                break;
            }
        } else {
            subscribedVariables.push_back({requestIdentifier, false});
        }
    }

    QSharedPointer<AbstractDataProtocol> protocol = d->getProtocol(peerId);
    if (protocol) {
        protocol->sendVariableSubscriptions(subscribedVariables);
        notifyVariablesValue(newSubscribedVariables);
    }
}

QSharedPointer<SubscribedVariableRecord> DataManager::subscribeVariable(const QString &sourceObjectName, const QString &sourceVariableName)
{
    DataVariableDescription myNewVariable(sourceObjectName, sourceVariableName);
    const quint64 newVariableId = myNewVariable.toIdentifier().identifier();
    auto itSubscription = d->mSubscriptions.find(newVariableId);

    if (itSubscription == d->mSubscriptions.end()) {
        d->mSubscriptions[newVariableId].reset(new SubscribedVariableRecord(myNewVariable));
        d->mSubscriptions[newVariableId]->setAutoSubscribe(true);
        if (peerIdFilter() == Automatiq::INVALID_PEER_ID) {
            beginInsertRows(QModelIndex(), d->mFilteredKeys.size(), d->mFilteredKeys.size());
            d->mFilteredKeys.push_back(newVariableId);
            d->mFilteredValues.push_back(d->mSubscriptions[newVariableId]);
            endInsertRows();
        }
    } else {
        if (d->mSubscriptions[newVariableId]->sourcePeerId() != Automatiq::INVALID_PEER_ID) {
            QList<DataVariableSubscriptionRequest> requests;

            requests.push_back({d->mSubscriptions[newVariableId]->toVariableIdentifier(), DataVariableSubscriptionRequestType::Subscribe});

            QSharedPointer<AbstractDataProtocol> protocol = d->getProtocol(d->mSubscriptions[newVariableId]->sourcePeerId());
            if (protocol) {
                protocol->sendVariableSubscriptionRequests(requests);
            }
        }
    }

    return d->mSubscriptions[newVariableId];
}

bool DataManager::unsubscribeVariable(const QString &sourceObjectName, const QString &sourceVariableName)
{
    DataVariableDescription myVariable(sourceObjectName, sourceVariableName);
    const quint64 myVariableId = myVariable.toIdentifier().identifier();

    auto itSubscription = d->mSubscriptions.find(myVariableId);

    if (itSubscription == d->mSubscriptions.end()) {
        return false;
    } else {
        (*itSubscription)->setAutoSubscribe(false);
        if ((*itSubscription)->sourcePeerId() != Automatiq::INVALID_PEER_ID) {
            QList<DataVariableSubscriptionRequest> requests;

            requests.push_back({myVariable.toIdentifier(), DataVariableSubscriptionRequestType::Unsubscribe});

            QSharedPointer<AbstractDataProtocol> protocol = d->getProtocol((*itSubscription)->sourcePeerId());
            if (protocol) {
                protocol->sendVariableSubscriptionRequests(requests);
            }
        }
    }

    return true;
}

void DataManager::triggerPeriodicVariablesUpdate()
{
    notifyVariablesValue(d->mPendingNotifications);
    d->mPendingNotifications.clear();
}

int DataManager::rowCount(const QModelIndex & parent) const
{
    if (parent.parent().isValid()) {
        return 0;
    } else {
        return d->mFilteredKeys.size();
    }
}

QHash<int, QByteArray> DataManager::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[static_cast<int>(ColumnsRoles::SourceObjectNameRole)] = "sourceObjectName";
    roles[static_cast<int>(ColumnsRoles::SourceVariableNameRole)] = "sourceVariableName";
    roles[static_cast<int>(ColumnsRoles::ValueRole)] = "value";
    roles[static_cast<int>(ColumnsRoles::IsSubcribedRole)] = "isSubscribed";
    roles[static_cast<int>(ColumnsRoles::PeerIdRole)] = "peerId";

    return roles;
}

Qt::ItemFlags DataManager::flags(const QModelIndex &index) const
{
    if (index.parent().isValid()) {
        return Qt::NoItemFlags;
    }

    if (index.column() != 0) {
        return Qt::NoItemFlags;
    }

    if (index.row() < 0 || index.row() > d->mFilteredKeys.size() - 1) {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

QVariant DataManager::data(const QModelIndex &index, int role) const
{
    if (index.parent().isValid()) {
        return QVariant();
    }

    if (index.column() != 0) {
        return QVariant();
    }

    if (index.row() < 0 || index.row() > d->mFilteredKeys.size() - 1) {
        return QVariant();
    }

    switch(role)
    {
    case ColumnsRoles::SourceObjectNameRole:
        return d->mFilteredValues[index.row()]->sourceObjectName();
    case ColumnsRoles::SourceVariableNameRole:
        return d->mFilteredValues[index.row()]->sourceVariableName();
    case ColumnsRoles::ValueRole:
        return d->mFilteredValues[index.row()]->value();
    case ColumnsRoles::IsSubcribedRole:
        return d->mFilteredValues[index.row()]->isSubscribed();
    case ColumnsRoles::PeerIdRole:
        return d->mFilteredValues[index.row()]->sourcePeerId();
    }

    return QVariant();
}

void DataManager::resetModel()
{
    beginResetModel();
    d->mFilteredKeys.clear();
    d->mFilteredValues.clear();

    for (auto itSubscription : d->mSubscriptions) {
        if (itSubscription->sourcePeerId() == peerIdFilter() || peerIdFilter() == Automatiq::INVALID_PEER_ID) {
            d->mFilteredKeys.push_back(itSubscription->toVariableIdentifier().identifier());
            d->mFilteredValues.push_back(itSubscription);
        }
    }
    endResetModel();
}

bool DataManager::registerVariable(QObject *holder, const QString &variableName)
{
    const QByteArray &variableLatinName = variableName.toLatin1();
    int index = holder->metaObject()->indexOfProperty(variableLatinName.data());

    if (index == -1) {
        return false;
    }

    const QMetaProperty &myProperty = holder->metaObject()->property(index);
    if (!myProperty. hasNotifySignal()) {
        return false;
    }

    DataVariableIdentifier registeredIdentifier(holder->objectName(), variableName);

    auto itProperty = d->mRegisteredVariables.find(registeredIdentifier.identifier());
    if (itProperty != d->mRegisteredVariables.end()) {
        return false;
    }

    RegisteredVariableRecord newRecord;
    newRecord.mHolder = holder;
    newRecord.mProperty = myProperty;

    d->mRegisteredVariables[registeredIdentifier.identifier()] = newRecord;
    connect(holder, myProperty.notifySignal(), this, metaObject()->method(metaObject()->indexOfMethod(metaObject()->normalizedSignature("propertyChanged(const QString&, const char*, const QVariant&)").data())));

    return true;
}

QSharedPointer<SubscribedVariableRecord> DataManager::getVariable(const QString &sourceObjectName, const QString &sourceVariableName) const
{
    DataVariableIdentifier myIdentifier(sourceObjectName, sourceVariableName);
    const quint64 newVariableId = myIdentifier.identifier();
    auto itSubscription = d->mSubscriptions.find(newVariableId);

    if (itSubscription != d->mSubscriptions.end()) {
        return itSubscription.value();
    }

    return QSharedPointer<SubscribedVariableRecord>();
}

void DataManager::propertyChanged(const QString &objectName, const char *propertyName, const QVariant &value)
{
    DataVariableIdentifier registeredIdentifier(objectName, QString::fromLatin1(propertyName));

    auto itProperty = d->mRegisteredVariables.find(registeredIdentifier.identifier());
    if (itProperty == d->mRegisteredVariables.end()) {
        return;
    }

    d->mPendingNotifications.push_back(registeredIdentifier);
}

void DataManager::notifyVariablesValue(const QList<DataVariableIdentifier> &variablesList)
{
    QMap<Automatiq::peerId, QList<DataVariableWithValue> > variablesToNotify;

    for (auto itVariable = variablesList.begin(); itVariable != variablesList.end(); ++itVariable) {
        auto itProperty = d->mRegisteredVariables.find(itVariable->identifier());
        if (itProperty == d->mRegisteredVariables.end()) {
            continue;
        }

        auto variableRecord = itProperty.value();

        DataVariableWithValue variableUpdate;
        variableUpdate.setIdentifier(*itVariable);
        variableUpdate.setValue(variableRecord.mProperty.read(variableRecord.mHolder));

        auto peerIdSubscribers = itProperty.value().mSubscribers;
        for (auto peerId = peerIdSubscribers.begin(); peerId != peerIdSubscribers.end(); ++peerId) {
            variablesToNotify[*peerId].push_back(variableUpdate);
        }
    }

    for (auto itMessageToSend = variablesToNotify.begin(); itMessageToSend != variablesToNotify.end(); ++itMessageToSend) {
        QSharedPointer<AbstractDataProtocol> currentProtocol(d->getProtocol(itMessageToSend.key()));

        if (currentProtocol) {
            currentProtocol->sendVariableValues(itMessageToSend.value());
        }
    }
}

#include "moc_datamanager.cpp"
