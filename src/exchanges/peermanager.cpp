/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "peermanager.h"

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

#include <QtCore/QTimer>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QSharedPointer>
#include <QtCore/QDebug>
#include <QtCore/QSettings>

struct PeerConnectionInformation
{
    typedef QSharedPointer<NetworkPeer> NetworkPeerPtr;

    QString mPeerName;

    QHostAddress mAddress;

    quint16 mPort;

    NetworkPeerPtr mPeer;

    PeerManager::ConnectionOrigin mConnectionOrigin;
};

class PeerManagerPrivate
{
public:

    PeerManagerPrivate()
        : mTimer(), mPeers(), mPeersFromId(), mServer(nullptr), mNextPeerId(0),
          mSettings(new QSettings(QStringLiteral("org.mgallien.automatiq"), QStringLiteral("client")))
    {
        QList<QVariant> allHosts(mSettings->value(QStringLiteral("PeerManager/hosts")).toList());

        for (auto itPeer : allHosts) {
            QMap<QString, QVariant> entry(itPeer.toMap());

            if (entry.count() == 2) {
                PeerConnectionInformation newPeer;

                newPeer.mAddress = QHostAddress(entry[QStringLiteral("address")].toString());
                newPeer.mPort = entry[QStringLiteral("port")].toUInt();
                newPeer.mConnectionOrigin = PeerManager::ConnectionOrigin::OutgoingConnection;

                mPeers.push_back(newPeer);
            }
        }
    }

    ~PeerManagerPrivate()
    {
        delete mSettings;
    }

    int peerIndexFromRawPointer(NetworkPeer *rawPointer) const
    {
        for(auto it = 0; it < mPeers.size(); ++it) {
            if (mPeers[it].mPeer.data() == rawPointer) {
                return it;
            }
        }

        return mPeers.size();
    }

    QTimer mTimer;

    QList<PeerConnectionInformation> mPeers;

    QMap<Automatiq::peerId, PeerConnectionInformation::NetworkPeerPtr> mPeersFromId;

    QTcpServer *mServer;

    Automatiq::peerId mNextPeerId;

    QSettings *mSettings;
};

PeerManager::PeerManager(QObject *parent)
    : QAbstractListModel(parent), d(new PeerManagerPrivate)
{
    d->mTimer.setInterval(5000);
    d->mTimer.setTimerType(Qt::VeryCoarseTimer);
    connect(&d->mTimer, &QTimer::timeout, this, &PeerManager::reconnectTimer);
}

PeerManager::~PeerManager()
{
    delete d;
}

void PeerManager::setServer(QTcpServer *aServer)
{
    d->mServer = aServer;
}

QSharedPointer<NetworkPeer> PeerManager::peerFromId(Automatiq::peerId peerId) const
{
    auto itPeer = d->mPeersFromId.end();

    if ((itPeer = d->mPeersFromId.find(peerId)) != d->mPeersFromId.end()) {
        return itPeer.value();
    } else {
        return QSharedPointer<NetworkPeer>();
    }
}

void PeerManager::connectToPeer(const QString &hostName, const QHostAddress &aAddress, quint16 port)
{
    for (auto itPeer = d->mPeers.begin(); itPeer != d->mPeers.end(); ++itPeer) {
        if (itPeer->mPeerName == hostName) {
            return;
        }
    }

    beginInsertRows(QModelIndex(), d->mPeers.size(), d->mPeers.size());

    PeerConnectionInformation newPeer;

    newPeer.mPeerName = hostName;
    newPeer.mAddress = aAddress;
    newPeer.mPort = port;
    newPeer.mConnectionOrigin = ConnectionOrigin::OutgoingConnection;

    doNewConnection(newPeer);

    d->mPeers.push_back(newPeer);

    endInsertRows();
}

void PeerManager::newPeerHasConnected()
{
    QSharedPointer<NetworkPeer> myNewPeer(new NetworkPeer);
    myNewPeer->setPeerId(d->mNextPeerId);
    ++d->mNextPeerId;

    myNewPeer->setPeerSocket(d->mServer->nextPendingConnection());

    beginInsertRows(QModelIndex(), d->mPeers.size(), d->mPeers.size());
    PeerConnectionInformation newPeerConnection;
    newPeerConnection.mPeer = myNewPeer;
    newPeerConnection.mAddress = myNewPeer->peerSocket()->peerAddress();
    newPeerConnection.mPort = myNewPeer->peerSocket()->peerPort();
    newPeerConnection.mConnectionOrigin = ConnectionOrigin::IncomingConnection;
    d->mPeers.push_back(newPeerConnection);
    d->mPeersFromId[myNewPeer->peerId()] = myNewPeer;

    connect(myNewPeer.data(), &NetworkPeer::peerIsDisconnected, this, &PeerManager::peerIsDisconnected);

    Q_EMIT newPeer(myNewPeer, ConnectionOrigin::IncomingConnection);
    endInsertRows();
}

void PeerManager::configurationUpdate(const Automatiq::configuration_type &aNewConfiguration)
{
    QList<QVariant> allHosts;
    for (auto itPeer : d->mPeers) {
        if (itPeer.mConnectionOrigin == ConnectionOrigin::OutgoingConnection) {
            QMap<QString, QVariant> newEntry;
            newEntry[QStringLiteral("address")] = itPeer.mAddress.toString();
            newEntry[QStringLiteral("port")] = itPeer.mPort;
            allHosts.push_back(newEntry);
        }
    }

    auto itConfig = aNewConfiguration.find(QStringLiteral("PeerManager/hosts"));
    if (itConfig != aNewConfiguration.end()) {
        QList<QVariant> newHosts = itConfig.value().toList();
        bool updateNecessary = false;

        for(auto oneNewHost : newHosts) {
            bool found = false;

            for(auto oneHost : allHosts) {
                auto oneHostInformation = oneHost.toMap();
                if (oneNewHost == oneHostInformation) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                updateNecessary = true;
            }
        }

        for(auto oneHost : allHosts) {
            bool found = false;
            auto oneHostInformation = oneHost.toMap();

            for(auto oneNewHost : newHosts) {
                if (oneNewHost == oneHostInformation) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                updateNecessary = true;
            }
        }

        if (updateNecessary) {
            updatePeersOutgoingConnectionsList(aNewConfiguration, newHosts);
        }
    } else {
        updatePeersOutgoingConnectionsList(aNewConfiguration, allHosts);
    }
}

void PeerManager::updatePeersOutgoingConnectionsList(const Automatiq::configuration_type &aNewConfiguration,
                                                     const QList<QVariant> &newList)
{
    Automatiq::configuration_type configurationUpdate(aNewConfiguration);
    configurationUpdate[QStringLiteral("PeerManager/hosts")] = newList;

    d->mSettings->setValue(QStringLiteral("PeerManager/hosts"), newList);

    for (auto itPeer = d->mPeers.begin(); itPeer != d->mPeers.end(); ) {
        if (itPeer->mConnectionOrigin == ConnectionOrigin::OutgoingConnection) {
            bool found = false;

            for (auto oneHost : newList) {
                auto oneHostInformation(oneHost.toMap());

                if (itPeer->mAddress.toString() == oneHostInformation[QStringLiteral("address")].toString() &&
                    itPeer->mPort == oneHostInformation[QStringLiteral("port")].toUInt()) {
                    found = true;
                }
            }

            if (!found) {
                if (itPeer->mPeer) {
                    itPeer->mPeer->disconnectFromPeer();
                }
                itPeer = d->mPeers.erase(itPeer);
            } else {
                ++itPeer;
            }
        } else {
            ++itPeer;
        }
    }

    for (auto oneHost : newList) {
        auto oneHostInformation(oneHost.toMap());
        bool found = false;

        for (auto itPeer = d->mPeers.begin(); itPeer != d->mPeers.end(); ++itPeer) {
            if (itPeer->mConnectionOrigin == ConnectionOrigin::OutgoingConnection &&
                    itPeer->mAddress.toString() == oneHostInformation[QStringLiteral("address")].toString() &&
                    itPeer->mPort == oneHostInformation[QStringLiteral("port")].toUInt()) {
                found = true;
            }

            if (!found) {
                connectToPeer(oneHostInformation[QStringLiteral("address")].toString(),
                              QHostAddress(oneHostInformation[QStringLiteral("address")].toString()),
                              oneHostInformation[QStringLiteral("port")].toUInt());
            }
        }
    }
}

void PeerManager::peerIsInError(QAbstractSocket::SocketError socketError)
{
    peerIsDisconnected(socketError);
}

void PeerManager::peerIsDisconnected(QAbstractSocket::SocketError socketError)
{
    NetworkPeer *myPeerInError = qobject_cast<NetworkPeer*>(sender());

    if (myPeerInError) {
        auto peerSharedPointerIndex = d->peerIndexFromRawPointer(myPeerInError);
        if (peerSharedPointerIndex != d->mPeers.size()) {
            Q_EMIT peerDisconnected(d->mPeers[peerSharedPointerIndex].mPeer);
            switch (d->mPeers[peerSharedPointerIndex].mConnectionOrigin)
            {
            case ConnectionOrigin::IncomingConnection:
                beginRemoveRows(QModelIndex(), peerSharedPointerIndex, peerSharedPointerIndex);
                d->mPeersFromId.remove(myPeerInError->peerId());
                d->mPeers.removeAt(peerSharedPointerIndex);
                endRemoveRows();
                break;
            case ConnectionOrigin::OutgoingConnection:
                d->mPeersFromId.remove(myPeerInError->peerId());
                d->mPeers[peerSharedPointerIndex].mPeer.reset();
                Q_EMIT dataChanged(index(peerSharedPointerIndex), index(peerSharedPointerIndex), {PeerIdRole});
                d->mTimer.start();
                break;
            }
        }
    }
}

void PeerManager::peerIdModified(Automatiq::peerId newId)
{
    NetworkPeer *myPeer = qobject_cast<NetworkPeer*>(sender());

    if (myPeer) {
        auto peerIndex = d->peerIndexFromRawPointer(myPeer);
        if (peerIndex < d->mPeers.size()) {
            d->mPeersFromId[newId] = d->mPeers[peerIndex].mPeer;
            Q_EMIT dataChanged(index(peerIndex), index(peerIndex), {PeerIdRole});
        }
    }
}

void PeerManager::peerNameModified(const QString &newName)
{
    NetworkPeer *myPeer = qobject_cast<NetworkPeer*>(sender());

    if (myPeer) {
        auto peerIndex = d->peerIndexFromRawPointer(myPeer);
        if (peerIndex < d->mPeers.size()) {
            if (d->mPeers[peerIndex].mPeerName.isNull()) {
                d->mPeers[peerIndex].mPeerName = newName;
                Q_EMIT dataChanged(index(peerIndex), index(peerIndex), {PeerNameRole});
            }
        }
    }
}

void PeerManager::reconnectTimer()
{
    for (int peerIndex = 0; peerIndex < d->mPeers.size(); ++peerIndex) {
        PeerConnectionInformation &itPeer = d->mPeers[peerIndex];
        if (itPeer.mConnectionOrigin == ConnectionOrigin::OutgoingConnection && !itPeer.mPeer) {
            doNewConnection(itPeer);
            Q_EMIT dataChanged(index(peerIndex), index(peerIndex));
        }
    }
}

void PeerManager::doNewConnection(PeerConnectionInformation &newPeer)
{
    newPeer.mPeer.reset(new NetworkPeer);

    newPeer.mPeer->setPeerId(d->mNextPeerId);
    ++d->mNextPeerId;

    NetworkPeer *myNewPeer = newPeer.mPeer.data();

    QTcpSocket *myNewSocket = new QTcpSocket(myNewPeer);

    newPeer.mPeer->setPeerSocket(myNewSocket);

    connect(myNewPeer, &NetworkPeer::peerIsConnected, this, &PeerManager::peerFinishedConnecting);
    connect(myNewPeer, &NetworkPeer::newReadError, this, &PeerManager::peerIsInError);
    connect(myNewPeer, &NetworkPeer::peerIsDisconnected, this, &PeerManager::peerIsDisconnected);
    connect(myNewPeer, &NetworkPeer::peerIdChanged, this, &PeerManager::peerIdModified);
    connect(myNewPeer, &NetworkPeer::peerNameChanged, this, &PeerManager::peerNameModified);

    myNewSocket->connectToHost(newPeer.mAddress, newPeer.mPort);
}

void PeerManager::peerFinishedConnecting()
{
    NetworkPeer *myConnectedPeer = qobject_cast<NetworkPeer*>(sender());
    bool existsPeerDisconnected = false;
    bool foundPeer = false;

    for (auto itConnection = d->mPeers.begin(); itConnection != d->mPeers.end() && (!foundPeer || !existsPeerDisconnected); ++itConnection) {
        if (itConnection->mConnectionOrigin == ConnectionOrigin::OutgoingConnection && itConnection->mPeer && itConnection->mPeer->isDisconnected()) {
            existsPeerDisconnected = true;
        }

        if (myConnectedPeer) {
            if (itConnection->mPeer && itConnection->mPeer.data() == myConnectedPeer) {
                foundPeer = true;
                itConnection->mConnectionOrigin = ConnectionOrigin::OutgoingConnection;

                Q_EMIT newPeer(itConnection->mPeer, ConnectionOrigin::OutgoingConnection);
            }
        }
    }
    if (!existsPeerDisconnected) {
        d->mTimer.stop();
    }
}

int PeerManager::rowCount(const QModelIndex & parent) const
{
    if (parent.parent().isValid()) {
        return 0;
    } else {
        return d->mPeers.size();
    }
}

QHash<int, QByteArray> PeerManager::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[static_cast<int>(ColumnsRoles::NameRole)] = "name";
    roles[static_cast<int>(ColumnsRoles::PeerNameRole)] = "peerName";
    roles[static_cast<int>(ColumnsRoles::PeerIdRole)] = "peerId";

    return roles;
}

Qt::ItemFlags PeerManager::flags(const QModelIndex &index) const
{
    if (index.parent().isValid()) {
        return Qt::NoItemFlags;
    }

    if (index.column() != 0) {
        return Qt::NoItemFlags;
    }

    if (index.row() < 0 || index.row() > d->mPeers.size() - 1) {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

QVariant PeerManager::data(const QModelIndex &index, int role) const
{
    if (index.parent().isValid()) {
        return QVariant();
    }

    if (index.column() != 0) {
        return QVariant();
    }

    if (index.row() < 0 || index.row() > d->mPeers.size() - 1) {
        return QVariant();
    }

    switch(role)
    {
    case ColumnsRoles::NameRole:
        if (d->mPeers[index.row()].mPeer) {
            return d->mPeers[index.row()].mPeer->peerName();
        } else {
            return QString();
        }
    case ColumnsRoles::PeerNameRole:
        return d->mPeers[index.row()].mPeerName;
    case ColumnsRoles::PeerIdRole:
        if (d->mPeers[index.row()].mPeer) {
            return d->mPeers[index.row()].mPeer->peerId();
        } else {
            return Automatiq::INVALID_PEER_ID;
        }
    }

    return QVariant();
}

Automatiq::peerId PeerManager::peerIdFromRow(int row)
{
    if (d->mPeers[row].mPeer) {
        return d->mPeers[row].mPeer->peerId();
    } else {
        return Automatiq::INVALID_PEER_ID;
    }
}

#include "moc_peermanager.cpp"
