/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_ABSTRACT_SYNCHRONIZATION_PROTOCOL_H
#define AUTOMATIQ_ABSTRACT_SYNCHRONIZATION_PROTOCOL_H

#include "automatiqexchanges_export.h"

#include "abstractprotocol.h"
#include "commondeclarations.h"

class AUTOMATIQEXCHANGES_EXPORT AbstractSynchronizationProtocol : public AbstractProtocol
{
    Q_OBJECT
public:

    explicit AbstractSynchronizationProtocol(Automatiq::peerId aRemotePeerId, QObject *parent = 0);

    virtual ~AbstractSynchronizationProtocol();

Q_SIGNALS:

    void receivedSynchronization(Automatiq::DataProcessingSteps step, Automatiq::peerId remotePeerId);

public Q_SLOTS:

    virtual void sendSynchronizationToRemotePeer(Automatiq::DataProcessingSteps step) = 0;
};

Q_DECLARE_METATYPE(AbstractSynchronizationProtocol*)

#endif
