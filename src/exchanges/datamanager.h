/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_DATA_MANAGER_H
#define AUTOMATIQ_DATA_MANAGER_H

#include "automatiqexchanges_export.h"

#include "clientmanager.h"
#include "commondeclarations.h"

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>
#include <QtCore/QList>
#include <QtCore/QString>

class DataManagerPrivate;
class AbstractPeerProtocol;
class AbstractDataProtocol;
class DataVariableWithValue;
class DataVariableIdentifier;
class DataVariableDescription;
class DataVariableSubscription;
class DataVariableSubscriptionRequest;
class SubscribedVariableRecord;

class AUTOMATIQEXCHANGES_EXPORT DataManager : public ClientManager
{
    Q_OBJECT

public:

    enum ColumnsRoles {
        SourceObjectNameRole = Qt::UserRole + 1,
        SourceVariableNameRole,
        ValueRole,
        IsSubcribedRole,
        PeerIdRole,
    };

    explicit DataManager(const QString &peerOwnName, QObject *parent = 0);

    explicit DataManager(QObject *parent = 0);

    virtual ~DataManager();

    QSharedPointer<AbstractDataProtocol> getDataProtocol(Automatiq::peerId peerId) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    void resetModel() Q_DECL_OVERRIDE;

    void newConnectedProtocol(Automatiq::peerId remotePeerId, QSharedPointer<AbstractPeerProtocol> aProtocol) Q_DECL_OVERRIDE;

    bool registerVariable(QObject *holder, const QString &variableName);

    QSharedPointer<SubscribedVariableRecord> getVariable(const QString &sourceObjectName, const QString &sourceVariableName) const;

Q_SIGNALS:

    void autoSubscribeVariablesChanged();

public Q_SLOTS:

    void peerIsDisconnected();

    void receivedVariableValues(Automatiq::peerId peerId, const QList<DataVariableWithValue> &values);

    void receivedVariablesList(Automatiq::peerId peerId, const QList<DataVariableDescription> &variables);

    void receivedRequestForListVariables(Automatiq::peerId peerId);

    void receivedVariableSubscriptionsList(Automatiq::peerId peerId, const QList<DataVariableSubscription> &variables);

    void receivedVariableSubscriptionRequestsList(Automatiq::peerId peerId, const QList<DataVariableSubscriptionRequest> &requests);

    QSharedPointer<SubscribedVariableRecord> subscribeVariable(const QString &sourceObjectName, const QString &sourceVariableName);

    bool unsubscribeVariable(const QString &sourceObjectName, const QString &sourceVariableName);

    void triggerPeriodicVariablesUpdate();

private Q_SLOTS:

    void propertyChanged(const QString &objectName, const char *propertyName, const QVariant &value);

private:

    void notifyVariablesValue(const QList<DataVariableIdentifier> &variablesList);

    DataManagerPrivate *d;
};

#endif
