/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "clientmanager.h"
#include "protocolmanager.h"

class ClientManagerPrivate
{
public:

    ClientManagerPrivate() : mPeerIdFilter(Automatiq::INVALID_PEER_ID), mPeerOwnName()
    {
    }

    ClientManagerPrivate(const QString &peerOwnName)
        : mPeerIdFilter(Automatiq::INVALID_PEER_ID), mPeerOwnName(peerOwnName)
    {
    }

    Automatiq::peerId mPeerIdFilter;

    QString mPeerOwnName;
};

ClientManager::ClientManager(const QString &peerOwnName, QObject *parent)
    : QAbstractListModel(parent), d(new ClientManagerPrivate(peerOwnName))
{
}

ClientManager::ClientManager(QObject *parent)
    : QAbstractListModel(parent), d(new ClientManagerPrivate)
{
}

ClientManager::~ClientManager()
{
    delete d;
}

Automatiq::peerId ClientManager::peerIdFilter() const
{
    return d->mPeerIdFilter;
}

void ClientManager::setPeerIdFilter(Automatiq::peerId aPeerId)
{
    d->mPeerIdFilter = aPeerId;
    resetModel();
}

const QString& ClientManager::peerOwnName() const
{
    return d->mPeerOwnName;
}

void ClientManager::setPeerOwnName(const QString &peerName)
{
    d->mPeerOwnName = peerName;
}

#include "moc_clientmanager.cpp"
