/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_AUTOMATIQ_NODE_H
#define AUTOMATIQ_AUTOMATIQ_NODE_H

#include "automatiqexchanges_export.h"

#include "commondeclarations.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QSharedPointer>
#include <QtNetwork/QHostAddress>

class AutomatiqNodePrivate;

class ProtocolManager;
class AbstractPeerProtocol;
class PeerManager;

class AUTOMATIQEXCHANGES_EXPORT AutomatiqNode : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString hostPeerName
               READ hostPeerName
               WRITE setHostPeerName
               NOTIFY hostPeerNameChanged)

    Q_PROPERTY(PeerManager* peerManager
               READ peerManager
               NOTIFY peerManagerChanged)

public:

    explicit AutomatiqNode(QObject *parent = 0);

    ~AutomatiqNode();

    const QString& hostPeerName() const;

    virtual void setHostPeerName(QString aHostPeerName);

    Q_INVOKABLE void initialize();

    ProtocolManager* protocolManager();

    PeerManager* peerManager();

public Q_SLOTS:

    Q_INVOKABLE void connectToPeer(const QString &hostName, const QHostAddress &peerAddress, quint16 port);

    Q_INVOKABLE void disconnectFromPeer(Automatiq::peerId peerId);

Q_SIGNALS:

    void newConnectedPeer(const QString &peerName, Automatiq::peerId remotePeerId);

    void peerIsDisconnected(Automatiq::peerId remotePeerId);

    void peerManagerChanged();

    void hostPeerNameChanged();

private Q_SLOTS:

    void handleNewConnectedPeer(Automatiq::peerId aPeerId, QSharedPointer<AbstractPeerProtocol> aProtocol);

private:

    AutomatiqNodePrivate *d;

};

#endif
