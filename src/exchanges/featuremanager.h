/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_FEATURE_MANAGER_H
#define AUTOMATIQ_FEATURE_MANAGER_H

#include "automatiqexchanges_export.h"

#include "commondeclarations.h"

#include <QMap>
#include <QSet>
#include <QSharedPointer>

template <typename PROTOCOL_TYPE>
class FeatureManager
{
public:
    FeatureManager() : mProtocols(), mPeerFeatures()
    {
    }

    ~FeatureManager()
    {
    }

    bool supportFeature(Automatiq::peerId peerId, quint64 feature) const
    {
        auto itClient = mPeerFeatures.find(peerId);
        if (itClient != mPeerFeatures.end()) {
            return itClient.value().contains(feature);
        } else {
            return false;
        }
    }

    void addFeature(Automatiq::peerId peerId, quint64 feature)
    {
        auto itClient = mPeerFeatures.find(peerId);
        if (itClient != mPeerFeatures.end()) {
            itClient.value().insert(feature);
        }
    }

    void removeFeature(Automatiq::peerId peerId, quint64 feature)
    {
        auto itClient = mPeerFeatures.find(peerId);
        if (itClient != mPeerFeatures.end()) {
            itClient.value().remove(feature);
        }
    }

    Automatiq::peerId peerIsDisconnected(QObject *disconnectedProtocol)
    {
        QSharedPointer<PROTOCOL_TYPE> myProtocol;
        for (auto itProtocol: mProtocols) {
            if (itProtocol == disconnectedProtocol) {
                myProtocol = itProtocol;
                break;
            }
        }
        if (myProtocol) {
            mPeerFeatures.remove(myProtocol->peerId());
            mProtocols.remove(myProtocol->peerId());
            return myProtocol->peerId();
        }

        return Automatiq::INVALID_PEER_ID;
    }

    QSharedPointer<PROTOCOL_TYPE> getProtocol(Automatiq::peerId peerId) const
    {
        auto itProtocol = mProtocols.find(peerId);

        if (itProtocol != mProtocols.end()) {
            return itProtocol.value();
        }

        return QSharedPointer<PROTOCOL_TYPE>();
    }

    QMap<Automatiq::peerId, QSharedPointer<PROTOCOL_TYPE> > mProtocols;

    QMap<Automatiq::peerId, QSet<quint64> > mPeerFeatures;

};

#endif
