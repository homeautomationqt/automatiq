/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "automatiqhost.h"

#include "serverconfigmanager.h"
#include "protocolmanager.h"

#include <QtNetwork/QHostAddress>
#include <QtNetwork/QTcpServer>

#include <QtCore/QTimer>

class AutomatiqHostPrivate
{
public:

    AutomatiqHostPrivate()
        : mServerConfigManager()
    {
    }

    ~AutomatiqHostPrivate()
    {
    }

    ServerConfigManager mServerConfigManager;

    QTcpServer mServer;
};

AutomatiqHost::AutomatiqHost(QObject *parent) : AutomatiqNode(parent), d(new AutomatiqHostPrivate)
{
    peerManager()->setServer(&d->mServer);
    connect(&d->mServer, &QTcpServer::newConnection, peerManager(), &PeerManager::newPeerHasConnected);

    connect(protocolManager(), &ProtocolManager::newInitializedProtocol, &d->mServerConfigManager, &ServerConfigManager::newConnectedProtocol);

    connect(&d->mServerConfigManager, &ServerConfigManager::newConfigurationUpdate, peerManager(), &PeerManager::configurationUpdate);
    connect(peerManager(), &PeerManager::askUpdateConfiguration, &d->mServerConfigManager, &ServerConfigManager::updateConfiguration);
}

AutomatiqHost::~AutomatiqHost()
{
    d->mServer.close();
    delete d;
}

void AutomatiqHost::setHostPeerName(QString aHostPeerName)
{
    if (d->mServerConfigManager.peerOwnName() != aHostPeerName) {
        d->mServerConfigManager.setPeerOwnName(aHostPeerName);
        AutomatiqNode::setHostPeerName(aHostPeerName);
    }
}

ServerConfigManager *AutomatiqHost::serverConfigManager()
{
    return &d->mServerConfigManager;
}

void AutomatiqHost::initialize(const QHostAddress &address, quint16 port)
{
    d->mServer.listen(address, port);

    AutomatiqNode::initialize();

    d->mServerConfigManager.initialize();
}

#include "moc_automatiqhost.cpp"
