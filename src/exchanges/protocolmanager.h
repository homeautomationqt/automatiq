/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_PROTOCOL_MANAGER_H
#define AUTOMATIQ_PROTOCOL_MANAGER_H

#include "automatiqexchanges_export.h"

#include "peermanager.h"
#include "abstractpeerprotocol.h"

#include "commondeclarations.h"

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

class NetworkPeer;

class ProtocolManagerPrivate;

class AUTOMATIQEXCHANGES_EXPORT ProtocolManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString peerOwnName
               READ peerOwnName WRITE setPeerOwnName)

public:
    explicit ProtocolManager(QObject *parent = 0);

    explicit ProtocolManager(const QString &aPeerOwnName, QObject *parent = 0);

    ~ProtocolManager();

    const QString &peerOwnName() const;

    void setPeerOwnName(const QString &peerName);

    void disconnectFromPeer(Automatiq::peerId aPeerId);

Q_SIGNALS:

    void newConnectedProtocol(QSharedPointer<AbstractPeerProtocol> aProtocol);

    void newInitializedProtocol(Automatiq::peerId aPeerId, QSharedPointer<AbstractPeerProtocol> aProtocol);

    void disconnectedProtocol(Automatiq::peerId aPeerId);

public Q_SLOTS:

    void handleNewConnection(QSharedPointer<NetworkPeer> aPeer, PeerManager::ConnectionOrigin connectionOrigin);

private Q_SLOTS:

    void peerIsDisconnected();

    void protocolChangedState(Automatiq::peerId peerId, AbstractPeerProtocol::ProtocolInitializationState newState);

private:

    ProtocolManagerPrivate *d;
};

#endif // PROTOCOLMANAGER_H
