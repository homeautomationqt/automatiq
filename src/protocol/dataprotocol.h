/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_DATA_PROTOCOL_H
#define AUTOMATIQ_DATA_PROTOCOL_H

#include "automatiqprotocol_export.h"

#include "uniqueidentifier.h"
#include "abstractdataprotocol.h"

#include "commondeclarations.h"

#include <QtCore/QObject>
#include <QtCore/QList>

class DataProtocolPrivate;
class NetworkPeer;
class MessageContainer;
class DataVariableWithValue;
class DataVariableIdentifier;
class DataVariableDescription;
class DataVariableSubscription;
class DataVariableSubscriptionRequest;

class AUTOMATIQPROTOCOL_NO_EXPORT DataProtocol : public AbstractDataProtocol
{
    Q_OBJECT
public:

    static const quint64 valueProtocolId = "DataProtocol"_uniqueId;

    explicit DataProtocol(const QString &aPeerOwnName, Automatiq::peerId aRemotePeerId, QObject *parent = 0);

    virtual ~DataProtocol();

    quint64 protocolId() override;

public Q_SLOTS:

    void sendAcknowledgement(ACKNOWLEDGE_TYPE ack, quint64 sequenceNumber) override;

    void protocolReceivedAcknowledge(QSharedPointer<const MessageContainer> acknowledge, QSharedPointer<const MessageContainer> originalMessage) override;

    void protocolReceivedMessage(QSharedPointer<const MessageContainer> receivedMessage) override;

    void askVariablesList() override;

    void sendVariablesList(const QList<DataVariableDescription> &variables) override;

    void sendVariableValues(const QList<DataVariableWithValue> &values) override;

    void sendVariableSubscriptions(const QList<DataVariableSubscription> &variables) override;

    void sendVariableSubscriptionRequests(const QList<DataVariableSubscriptionRequest> &requests) override;

private:

    DataProtocolPrivate *d;
};

Q_DECLARE_METATYPE(DataProtocol*)

#endif
