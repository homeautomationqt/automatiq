/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_MESSAGE_LIST_VARIABLES_H
#define AUTOMATIQ_MESSAGE_LIST_VARIABLES_H

#include "automatiqprotocol_export.h"

#include "uniqueidentifier.h"

#include <QtCore/QList>

class QByteArray;
class QDataStream;

class MessageListVariablesPrivate;
class DataVariableDescription;

class AUTOMATIQPROTOCOL_NO_EXPORT MessageListVariables
{
public:

    static const quint64 messageId = "MessageListVariables"_uniqueId;

    static const quint64 protocolId = "DataProtocol"_uniqueId;

    typedef QList<DataVariableDescription> VariableDescriptors;

    MessageListVariables();

    explicit MessageListVariables(const VariableDescriptors &aVariableDescriptors);

    MessageListVariables(const MessageListVariables &aOther);

    MessageListVariables(MessageListVariables &&aOther);

    ~MessageListVariables();

    MessageListVariables& operator=(const MessageListVariables &aOther);

    MessageListVariables& operator=(MessageListVariables &&aOther);

    void setValid(bool aIsValid);

    bool isValid() const;

    bool& isValid();

    void setVariableDescriptors(const MessageListVariables::VariableDescriptors &variablesId);

    const VariableDescriptors& variableDescriptors() const;

    VariableDescriptors& variableDescriptors();

    bool operator==(const MessageListVariables &aOther) const;

private:

    MessageListVariablesPrivate *d;
};

QDataStream & operator<< (QDataStream& stream, const MessageListVariables& message) AUTOMATIQPROTOCOL_NO_EXPORT;
QDataStream & operator>> (QDataStream& stream, MessageListVariables& message) AUTOMATIQPROTOCOL_NO_EXPORT;

#endif
