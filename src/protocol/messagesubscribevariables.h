/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_MESSAGE_SUBSCRIBE_VARIABLES_H
#define AUTOMATIQ_MESSAGE_SUBSCRIBE_VARIABLES_H

#include "automatiqprotocol_export.h"

#include "uniqueidentifier.h"

#include <QtCore/QList>

class QByteArray;
class QDataStream;

class MessageSubscribeVariablesPrivate;
class DataVariableSubscription;

class AUTOMATIQPROTOCOL_EXPORT MessageSubscribeVariables
{
public:

    static const quint64 messageId = "MessageSubscribeVariables"_uniqueId;

    static const quint64 protocolId = "DataProtocol"_uniqueId;

    typedef QList<DataVariableSubscription> VariableSubscriptions;

    MessageSubscribeVariables();

    explicit MessageSubscribeVariables(const VariableSubscriptions &subscriptions);

    MessageSubscribeVariables(const MessageSubscribeVariables &aOther);

    MessageSubscribeVariables(MessageSubscribeVariables &&aOther);

    ~MessageSubscribeVariables();

    MessageSubscribeVariables& operator=(const MessageSubscribeVariables &aOther);

    MessageSubscribeVariables& operator=(MessageSubscribeVariables &&aOther);

    void setValid(bool aIsValid);

    bool isValid() const;

    bool& isValid();

    void setVariableSubscriptions(const VariableSubscriptions &subscriptions);

    const VariableSubscriptions& variableSubscriptions() const;

    VariableSubscriptions& variableSubscriptions();

    bool operator==(const MessageSubscribeVariables &aOther) const;

private:

    MessageSubscribeVariablesPrivate *d;
};

QDataStream & operator<< (QDataStream& stream, const MessageSubscribeVariables& message) AUTOMATIQPROTOCOL_NO_EXPORT;
QDataStream & operator>> (QDataStream& stream, MessageSubscribeVariables& message) AUTOMATIQPROTOCOL_NO_EXPORT;

#endif
