/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_SYNCHRONIZATION_PROTOCOL_H
#define AUTOMATIQ_SYNCHRONIZATION_PROTOCOL_H

#include "automatiqprotocol_export.h"

#include "uniqueidentifier.h"
#include "abstractsynchronizationprotocol.h"

#include "commondeclarations.h"

#include <QObject>

class SynchronizationProtocolPrivate;
class NetworkPeer;
class MessageContainer;

class AUTOMATIQPROTOCOL_NO_EXPORT SynchronizationProtocol : public AbstractSynchronizationProtocol
{
    Q_OBJECT
public:

    static const quint64 valueProtocolId = "SynchronizationProtocol"_uniqueId;

    explicit SynchronizationProtocol(const QString &aPeerOwnName, Automatiq::peerId aRemotePeerId, QObject *parent = 0);

    virtual ~SynchronizationProtocol();

    quint64 protocolId() override;

public Q_SLOTS:

    void sendAcknowledgement(ACKNOWLEDGE_TYPE ack, quint64 sequenceNumber) override;

    void protocolReceivedAcknowledge(QSharedPointer<const MessageContainer> acknowledge, QSharedPointer<const MessageContainer> originalMessage) override;

    void protocolReceivedMessage(QSharedPointer<const MessageContainer> receivedMessage) override;

    void sendSynchronizationToRemotePeer(Automatiq::DataProcessingSteps step) override;

private:

    SynchronizationProtocolPrivate *d;
};

Q_DECLARE_METATYPE(SynchronizationProtocol*)

#endif
