/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagehelloacknowledge.h"

#include "commondeclarations.h"

#include <QtCore/QDataStream>
#include <QtCore/QString>
#include <QtCore/QDateTime>

class MessageHelloAcknowledgePrivate
{
public:

    MessageHelloAcknowledgePrivate() : mIsValid(false), mTimestamp(), mPeerName(), mPeerId(0)
    {
    }

    bool mIsValid;

    QDateTime mTimestamp;

    QString mPeerName;

    Automatiq::peerId mPeerId;
};

MessageHelloAcknowledge::MessageHelloAcknowledge()
    : d(new MessageHelloAcknowledgePrivate)
{
}

MessageHelloAcknowledge::MessageHelloAcknowledge(const MessageHelloAcknowledge &aOther)
    : d(new MessageHelloAcknowledgePrivate(*aOther.d))
{
}

MessageHelloAcknowledge::MessageHelloAcknowledge(MessageHelloAcknowledge &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageHelloAcknowledge::~MessageHelloAcknowledge()
{
    delete d;
}

MessageHelloAcknowledge& MessageHelloAcknowledge::operator=(const MessageHelloAcknowledge &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mTimestamp = aOther.d->mTimestamp;
        d->mPeerName = aOther.d->mPeerName;
        d->mPeerId = aOther.d->mPeerId;
    }

    return *this;
}

MessageHelloAcknowledge& MessageHelloAcknowledge::operator=(MessageHelloAcknowledge &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageHelloAcknowledge::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageHelloAcknowledge::isValid() const
{
    return d->mIsValid;
}

bool& MessageHelloAcknowledge::isValid()
{
    return d->mIsValid;
}

void MessageHelloAcknowledge::setTimestamp(const QDateTime &aTimestamp)
{
    d->mTimestamp = aTimestamp;
}

const QDateTime& MessageHelloAcknowledge::timestamp() const
{
    return d->mTimestamp;
}

QDateTime &MessageHelloAcknowledge::timestamp()
{
    return d->mTimestamp;
}

void MessageHelloAcknowledge::setPeerName(const QString &aName)
{
    d->mPeerName = aName;
}

const QString &MessageHelloAcknowledge::peerName() const
{
    return d->mPeerName;
}

QString &MessageHelloAcknowledge::peerName()
{
    return d->mPeerName;
}

QDataStream & operator<< (QDataStream& stream, const MessageHelloAcknowledge& message)
{
    stream << message.isValid() << message.timestamp() << message.peerName();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageHelloAcknowledge& message)
{
    stream >> message.isValid() >> message.timestamp() >> message.peerName();

    return stream;
}

bool MessageHelloAcknowledge::operator==(const MessageHelloAcknowledge &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mTimestamp == aOther.d->mTimestamp && d->mPeerName == aOther.d->mPeerName;
}
