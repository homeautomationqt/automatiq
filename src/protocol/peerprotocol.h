/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_PEER_PROTOCOL_H
#define AUTOMATIQ_PEER_PROTOCOL_H

#include "automatiqprotocol_export.h"

#include "abstractpeerprotocol.h"

#include "uniqueidentifier.h"

class PeerProtocolPrivate;

class AUTOMATIQPROTOCOL_EXPORT PeerProtocol : public AbstractPeerProtocol
{
    Q_OBJECT

public:

    static const quint64 protocolId = "PeerProtocol"_uniqueId;

    explicit PeerProtocol(QSharedPointer<NetworkPeer> networkPeer, QObject *parent = 0);

    virtual ~PeerProtocol();

public:

    void initProtocolDialog(const QString &peerOwnName, Automatiq::peerId peerId, NewPeerDialogInitialization initializationMode) override;

    void sendPeerName(const QString &peerName) override;

    QSharedPointer<AbstractConfigProtocol> createSubConfigProtocol(const QString &peerOwnName) override;

    QSharedPointer<AbstractDataProtocol> createSubDataProtocol(const QString &peerOwnName) override;

    QSharedPointer<AbstractSynchronizationProtocol> createSubSynchronizationProtocol(const QString &peerOwnName) override;

private:

    void manageNotInitializedProtocol(QSharedPointer<const MessageContainer> aLastReceivedMessage) override;

    void manageInitializedProtocol(QSharedPointer<const MessageContainer> aLastReceivedMessage) override;

    void sendAcknowledgement(ACKNOWLEDGE_TYPE ack, quint64 sequenceNumber);

    void sendError(Automatiq::ERROR_TYPE error, quint64 sequenceNumber) override;

    PeerProtocolPrivate *d;
};

#endif // SERVERPROTOCOL_H
