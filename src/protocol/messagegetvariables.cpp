/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagegetvariables.h"

#include <QtCore/QDataStream>

class MessageGetVariablesPrivate
{
public:

    MessageGetVariablesPrivate() : mIsValid(false), mSendUpdateForChange(false)
    {
    }

    bool mIsValid;

    bool mSendUpdateForChange;
};

MessageGetVariables::MessageGetVariables()
    : d(new MessageGetVariablesPrivate)
{
}

MessageGetVariables::MessageGetVariables(const MessageGetVariables &aOther)
    : d(new MessageGetVariablesPrivate(*aOther.d))
{
}

MessageGetVariables::MessageGetVariables(MessageGetVariables &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageGetVariables::~MessageGetVariables()
{
    delete d;
}

MessageGetVariables& MessageGetVariables::operator=(const MessageGetVariables &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mSendUpdateForChange = aOther.d->mSendUpdateForChange;
    }

    return *this;
}

MessageGetVariables& MessageGetVariables::operator=(MessageGetVariables &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageGetVariables::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageGetVariables::isValid() const
{
    return d->mIsValid;
}

bool& MessageGetVariables::isValid()
{
    return d->mIsValid;
}

void MessageGetVariables::setSendUpdateForChange(bool sendUpdate)
{
    d->mSendUpdateForChange = sendUpdate;
}

bool MessageGetVariables::sendUpdateForChange() const
{
    return d->mSendUpdateForChange;
}

bool& MessageGetVariables::sendUpdateForChange()
{
    return d->mSendUpdateForChange;
}

QDataStream & operator<< (QDataStream& stream, const MessageGetVariables &message)
{
    stream << message.isValid() << message.sendUpdateForChange();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageGetVariables &message)
{
    stream >> message.isValid() >> message.sendUpdateForChange();

    return stream;
}

bool MessageGetVariables::operator==(const MessageGetVariables &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mSendUpdateForChange == aOther.d->mSendUpdateForChange;
}
