/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagehello.h"

#include <QtCore/QDataStream>
#include <QtCore/QString>
#include <QtCore/QDateTime>

class MessageHelloPrivate
{
public:

    MessageHelloPrivate() : mIsValid(false), mTimestamp(), mPeerName()
    {
    }

    bool mIsValid;

    QDateTime mTimestamp;

    QString mPeerName;
};

MessageHello::MessageHello()
    : d(new MessageHelloPrivate)
{
}

MessageHello::MessageHello(const MessageHello &aOther)
    : d(new MessageHelloPrivate(*aOther.d))
{
}

MessageHello::MessageHello(MessageHello &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageHello::~MessageHello()
{
    delete d;
}

MessageHello& MessageHello::operator=(const MessageHello &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mTimestamp = aOther.d->mTimestamp;
        d->mPeerName = aOther.d->mPeerName;
    }

    return *this;
}

MessageHello& MessageHello::operator=(MessageHello &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageHello::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageHello::isValid() const
{
    return d->mIsValid;
}

bool& MessageHello::isValid()
{
    return d->mIsValid;
}

void MessageHello::setTimestamp(const QDateTime &aTimestamp)
{
    d->mTimestamp = aTimestamp;
}

const QDateTime& MessageHello::timestamp() const
{
    return d->mTimestamp;
}

QDateTime &MessageHello::timestamp()
{
    return d->mTimestamp;
}

void MessageHello::setPeerName(const QString &aName)
{
    d->mPeerName = aName;
}

const QString &MessageHello::peerName() const
{
    return d->mPeerName;
}

QString &MessageHello::peerName()
{
    return d->mPeerName;
}

QDataStream & operator<< (QDataStream& stream, const MessageHello& message)
{
    stream << message.isValid() << message.timestamp() << message.peerName();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageHello& message)
{
    stream >> message.isValid() >> message.timestamp() >> message.peerName();

    return stream;
}

bool MessageHello::operator==(const MessageHello &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mTimestamp == aOther.d->mTimestamp && d->mPeerName == aOther.d->mPeerName;
}
