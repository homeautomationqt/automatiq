/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_MESSAGE_LIST_VARIABLE_VALUES_H
#define AUTOMATIQ_MESSAGE_LIST_VARIABLE_VALUES_H

#include "automatiqprotocol_export.h"

#include "uniqueidentifier.h"

#include <QtCore/QList>

class QByteArray;
class QDataStream;

class MessageListVariableValuesPrivate;
class DataVariableWithValue;

class AUTOMATIQPROTOCOL_NO_EXPORT MessageListVariableValues
{
public:

    static const quint64 messageId = "MessageListVariableValues"_uniqueId;

    static const quint64 protocolId = "DataProtocol"_uniqueId;

    typedef QList<DataVariableWithValue> VariableValues;

    MessageListVariableValues();

    explicit MessageListVariableValues(const VariableValues &aVariableValues);

    MessageListVariableValues(const MessageListVariableValues &aOther);

    MessageListVariableValues(MessageListVariableValues &&aOther);

    ~MessageListVariableValues();

    MessageListVariableValues& operator=(const MessageListVariableValues &aOther);

    MessageListVariableValues& operator=(MessageListVariableValues &&aOther);

    void setValid(bool aIsValid);

    bool isValid() const;

    bool& isValid();

    void setVariableValues(const VariableValues &aVariableValues);

    const VariableValues& variableValues() const;

    VariableValues& variableValues();

    bool operator==(const MessageListVariableValues &aOther) const;

private:

    MessageListVariableValuesPrivate *d;
};

QDataStream & operator<< (QDataStream& stream, const MessageListVariableValues& message) AUTOMATIQPROTOCOL_NO_EXPORT;
QDataStream & operator>> (QDataStream& stream, MessageListVariableValues& message) AUTOMATIQPROTOCOL_NO_EXPORT;

#endif
