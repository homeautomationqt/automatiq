/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagelistvariables.h"

#include "datavariabledescription.h"

#include <QtCore/QDataStream>

class MessageListVariablesPrivate
{
public:

    bool mIsValid;

    MessageListVariables::VariableDescriptors mVariableDescriptors;

    MessageListVariablesPrivate() : mIsValid(false), mVariableDescriptors()
    {
    }

    explicit MessageListVariablesPrivate(const MessageListVariables::VariableDescriptors &aVariableDescriptors)
        : mIsValid(true), mVariableDescriptors(aVariableDescriptors)
    {
    }
};

MessageListVariables::MessageListVariables()
    : d(new MessageListVariablesPrivate)
{
}

MessageListVariables::MessageListVariables(const VariableDescriptors &aVariableDescriptors)
    : d(new MessageListVariablesPrivate(aVariableDescriptors))
{
}

MessageListVariables::MessageListVariables(const MessageListVariables &aOther)
    : d(new MessageListVariablesPrivate(*aOther.d))
{
}

MessageListVariables::MessageListVariables(MessageListVariables &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageListVariables::~MessageListVariables()
{
    delete d;
}

MessageListVariables& MessageListVariables::operator=(const MessageListVariables &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mVariableDescriptors = aOther.d->mVariableDescriptors;
    }

    return *this;
}

MessageListVariables& MessageListVariables::operator=(MessageListVariables &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageListVariables::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageListVariables::isValid() const
{
    return d->mIsValid;
}

bool& MessageListVariables::isValid()
{
    return d->mIsValid;
}

void MessageListVariables::setVariableDescriptors(const MessageListVariables::VariableDescriptors &variablesId)
{
    d->mVariableDescriptors = variablesId;
}

const MessageListVariables::VariableDescriptors &MessageListVariables::variableDescriptors() const
{
    return d->mVariableDescriptors;
}

MessageListVariables::VariableDescriptors &MessageListVariables::variableDescriptors()
{
    return d->mVariableDescriptors;
}

QDataStream & operator<< (QDataStream& stream, const MessageListVariables& message)
{
    stream << message.isValid() << message.variableDescriptors();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageListVariables& message)
{
    stream >> message.isValid() >> message.variableDescriptors();

    return stream;
}

bool MessageListVariables::operator==(const MessageListVariables &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mVariableDescriptors == aOther.d->mVariableDescriptors;
}

