/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messageerror.h"

#include "commondeclarations.h"

#include <QtCore/QDataStream>
#include <QtCore/QString>
#include <QtCore/QTime>

class MessageErrorPrivate
{
public:

    MessageErrorPrivate() : mIsValid(false), mError(Automatiq::ERROR_TYPE::Invalid), mSequenceNumber()
    {
    }

    bool mIsValid;

    Automatiq::ERROR_TYPE mError;

    quint64 mSequenceNumber;
};

MessageError::MessageError()
    : d(new MessageErrorPrivate)
{
}

MessageError::MessageError(const MessageError &aOther)
    : d(new MessageErrorPrivate(*aOther.d))
{
}

MessageError::MessageError(MessageError &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageError::~MessageError()
{
    delete d;
}

MessageError& MessageError::operator=(const MessageError &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mError = aOther.d->mError;
        d->mSequenceNumber = aOther.d->mSequenceNumber;
    }

    return *this;
}

MessageError& MessageError::operator=(MessageError &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageError::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageError::isValid() const
{
    return d->mIsValid;
}

bool& MessageError::isValid()
{
    return d->mIsValid;
}
void MessageError::setErrorType(Automatiq::ERROR_TYPE ack) const
{
    d->mError = ack;
}

Automatiq::ERROR_TYPE MessageError::errorType() const
{
    return d->mError;
}

Automatiq::ERROR_TYPE& MessageError::errorType()
{
    return d->mError;
}

void MessageError::setSequenceNumber(quint64 aSequenceNumber) const
{
    d->mSequenceNumber = aSequenceNumber;
}

quint64 MessageError::sequenceNumber() const
{
    return d->mSequenceNumber;
}

quint64& MessageError::sequenceNumber()
{
    return d->mSequenceNumber;
}

QDataStream & operator<< (QDataStream& stream, const MessageError& message)
{
    stream << message.isValid() << static_cast<quint32>(message.errorType()) << message.sequenceNumber();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageError& message)
{
    quint32 errorReadValue;
    stream >> message.isValid() >> errorReadValue >> message.sequenceNumber();
    message.setErrorType(static_cast<Automatiq::ERROR_TYPE>(errorReadValue));

    return stream;
}

bool MessageError::operator==(const MessageError &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mError == aOther.d->mError && d->mSequenceNumber == aOther.d->mSequenceNumber;
}
