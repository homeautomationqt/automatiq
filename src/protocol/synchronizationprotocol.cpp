/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "synchronizationprotocol.h"

#include "messagesynchronization.h"
#include "messageacknowledge.h"

#include "messagecontainer.h"

#include <QtCore/QDebug>

class SynchronizationProtocolPrivate
{
public:

    SynchronizationProtocolPrivate() : mPeerOwnName()
    {
    }

    QString mPeerOwnName;
};

SynchronizationProtocol::SynchronizationProtocol(const QString &aPeerOwnName, Automatiq::peerId aRemotePeerId, QObject *parent) :
    AbstractSynchronizationProtocol(aRemotePeerId, parent), d(new SynchronizationProtocolPrivate)
{
    d->mPeerOwnName = aPeerOwnName;
}

SynchronizationProtocol::~SynchronizationProtocol()
{
    delete d;
}

quint64 SynchronizationProtocol::protocolId()
{
    return SynchronizationProtocol::valueProtocolId;
}

void SynchronizationProtocol::sendAcknowledgement(ACKNOWLEDGE_TYPE ack, quint64 sequenceNumber)
{
    MessageAcknowledge myAck;
    myAck.setValid(true);
    myAck.setAcknowledge(ack);
    myAck.setSequenceNumber(sequenceNumber);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);

    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myAck);

    Q_EMIT newMessageToSend(myContainer);
}

void SynchronizationProtocol::protocolReceivedAcknowledge(QSharedPointer<const MessageContainer> acknowledge, QSharedPointer<const MessageContainer> originalMessage)
{
    if (originalMessage->protocolIdentifier() == SynchronizationProtocol::valueProtocolId) {
        switch (originalMessage->identifier())
        {
        case MessageSynchronization::messageId:
            break;
        }
    }
}

void SynchronizationProtocol::protocolReceivedMessage(QSharedPointer<const MessageContainer> receivedMessage)
{
    qDebug() << "SynchronizationProtocol::protocolReceivedMessage";
    if (receivedMessage->protocolIdentifier() == SynchronizationProtocol::valueProtocolId) {
        switch (receivedMessage->identifier())
        {
        case MessageSynchronization::messageId:
            Q_EMIT receivedSynchronization(receivedMessage->specificMessage<MessageSynchronization>()->synchronizationStep(), peerId());

            break;
        }
    }
}

void SynchronizationProtocol::sendSynchronizationToRemotePeer(Automatiq::DataProcessingSteps step)
{
    MessageSynchronization myMessage;
    myMessage.setValid(true);
    myMessage.setSynchronizationStep(step);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);
    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myMessage);

    Q_EMIT newMessageToSend(myContainer);
}

#include "moc_synchronizationprotocol.cpp"
