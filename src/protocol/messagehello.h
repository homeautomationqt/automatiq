/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_MESSAGE_HELLO_H
#define AUTOMATIQ_MESSAGE_HELLO_H

#include "automatiqprotocol_export.h"

#include "uniqueidentifier.h"

class QByteArray;
class QDataStream;
class QDateTime;

class MessageHelloPrivate;

class AUTOMATIQPROTOCOL_NO_EXPORT MessageHello
{
public:

    static const quint64 messageId = "MessageHello"_uniqueId;

    static const quint64 protocolId = "PeerProtocol"_uniqueId;

    MessageHello();

    MessageHello(const MessageHello &aOther);

    MessageHello(MessageHello &&aOther);

    ~MessageHello();

    MessageHello& operator=(const MessageHello &aOther);

    MessageHello& operator=(MessageHello &&aOther);

    void setValid(bool aIsValid);

    bool isValid() const;

    bool& isValid();

    void setTimestamp(const QDateTime &aTimestamp);

    const QDateTime& timestamp() const;

    QDateTime& timestamp();

    void setPeerName(const QString &aName);

    const QString& peerName() const;

    QString& peerName();

    bool operator==(const MessageHello &aOther) const;

private:

    MessageHelloPrivate *d;

};

QDataStream & operator<< (QDataStream& stream, const MessageHello& message) AUTOMATIQPROTOCOL_NO_EXPORT;
QDataStream & operator>> (QDataStream& stream, MessageHello& message) AUTOMATIQPROTOCOL_NO_EXPORT;

#endif
