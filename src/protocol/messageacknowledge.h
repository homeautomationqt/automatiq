/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_MESSAGE_ACKNOWLEDGE_H
#define AUTOMATIQ_MESSAGE_ACKNOWLEDGE_H

#include "automatiqprotocol_export.h"

#include "uniqueidentifier.h"

class QByteArray;
class QDataStream;

enum class ACKNOWLEDGE_TYPE
{
    KO = 0,
    OK = 1,
};

class MessageAcknowledgePrivate;

class AUTOMATIQPROTOCOL_NO_EXPORT MessageAcknowledge
{
public:

    static const quint64 messageId = "MessageAcknowledge"_uniqueId;

    static const quint64 protocolId = "PeerProtocol"_uniqueId;

    MessageAcknowledge();

    MessageAcknowledge(const MessageAcknowledge &aOther);

    MessageAcknowledge(MessageAcknowledge &&aOther);

    ~MessageAcknowledge();

    MessageAcknowledge& operator=(const MessageAcknowledge &aOther);

    MessageAcknowledge& operator=(MessageAcknowledge &&aOther);

    void setValid(bool aIsValid);

    bool isValid() const;

    bool& isValid();

    void setAcknowledge(ACKNOWLEDGE_TYPE ack) const;

    ACKNOWLEDGE_TYPE acknowledge() const;

    ACKNOWLEDGE_TYPE& acknowledge();

    void setSequenceNumber(quint64 aSequenceNumber) const;

    quint64 sequenceNumber() const;

    quint64& sequenceNumber();

    bool operator==(const MessageAcknowledge &aOther) const;

private:

    MessageAcknowledgePrivate *d;

};

QDataStream & operator<< (QDataStream& stream, const MessageAcknowledge& message) AUTOMATIQPROTOCOL_NO_EXPORT;
QDataStream & operator>> (QDataStream& stream, MessageAcknowledge& message) AUTOMATIQPROTOCOL_NO_EXPORT;

#endif
