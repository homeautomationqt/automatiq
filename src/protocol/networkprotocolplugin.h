/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_NETWORK_PROTOCOL_PLUGIN_H
#define AUTOMATIQ_NETWORK_PROTOCOL_PLUGIN_H

#include "automatiqprotocol_export.h"

#include "abstractpeerprotocol.h"

class NetworkProtocolPlugin : public QObject, public AbstractProtocolInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.mgallien.AbstractProtocolInterface" FILE "networkprotocol.json")
    Q_INTERFACES(AbstractProtocolInterface)

public:

    NetworkProtocolPlugin(QObject *parent = 0);

    virtual ~NetworkProtocolPlugin();

    QSharedPointer<AbstractPeerProtocol> createAbstractPeerProtocol(QSharedPointer<NetworkPeer> networkPeer, QObject *parent = 0) override;

};

#endif
