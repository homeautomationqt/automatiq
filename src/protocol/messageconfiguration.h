/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_MESSAGE_CONFIGURATION_H
#define AUTOMATIQ_MESSAGE_CONFIGURATION_H

#include "automatiqprotocol_export.h"

#include "uniqueidentifier.h"

#include <QtCore/QMap>
#include <QtCore/QVariant>
#include <QtCore/QString>

class QDataStream;

class MessageConfigurationPrivate;

class AUTOMATIQPROTOCOL_NO_EXPORT MessageConfiguration
{
public:

    typedef QMap<QString, QVariant> configuration_type;

    static const quint64 messageId = "MessageConfiguration"_uniqueId;

    static const quint64 protocolId = "ConfigProtocol"_uniqueId;

    MessageConfiguration();

    MessageConfiguration(const MessageConfiguration &aOther);

    MessageConfiguration(MessageConfiguration &&aOther);

    ~MessageConfiguration();

    MessageConfiguration& operator=(const MessageConfiguration &aOther);

    MessageConfiguration& operator=(MessageConfiguration &&aOther);

    void setValid(bool aIsValid);

    bool isValid() const;

    bool& isValid();

    void setConfiguration(const configuration_type &aConfiguration);

    const configuration_type& configuration() const;

    configuration_type& configuration();

    bool operator==(const MessageConfiguration &aOther) const;

private:

    MessageConfigurationPrivate *d;

};

QDataStream & operator<< (QDataStream& stream, const MessageConfiguration& message) AUTOMATIQPROTOCOL_NO_EXPORT;
QDataStream & operator>> (QDataStream& stream, MessageConfiguration& message) AUTOMATIQPROTOCOL_NO_EXPORT;

#endif
