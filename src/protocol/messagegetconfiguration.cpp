/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagegetconfiguration.h"

#include <QtCore/QDataStream>
#include <QtCore/QString>
#include <QtCore/QDateTime>

class MessageGetConfigurationPrivate
{
public:

    MessageGetConfigurationPrivate() : mIsValid(false), mSendUpdateForChange(false)
    {
    }

    bool mIsValid;

    bool mSendUpdateForChange;
};

MessageGetConfiguration::MessageGetConfiguration()
    : d(new MessageGetConfigurationPrivate)
{
}

MessageGetConfiguration::MessageGetConfiguration(const MessageGetConfiguration &aOther)
    : d(new MessageGetConfigurationPrivate(*aOther.d))
{
}

MessageGetConfiguration::MessageGetConfiguration(MessageGetConfiguration &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageGetConfiguration::~MessageGetConfiguration()
{
    delete d;
}

MessageGetConfiguration& MessageGetConfiguration::operator=(const MessageGetConfiguration &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mSendUpdateForChange = aOther.d->mSendUpdateForChange;
    }

    return *this;
}

MessageGetConfiguration& MessageGetConfiguration::operator=(MessageGetConfiguration &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageGetConfiguration::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageGetConfiguration::isValid() const
{
    return d->mIsValid;
}

bool& MessageGetConfiguration::isValid()
{
    return d->mIsValid;
}

void MessageGetConfiguration::setSendUpdateForChange(bool sendUpdate)
{
    d->mSendUpdateForChange = sendUpdate;
}

bool MessageGetConfiguration::sendUpdateForChange() const
{
    return d->mSendUpdateForChange;
}

bool& MessageGetConfiguration::sendUpdateForChange()
{
    return d->mSendUpdateForChange;
}

QDataStream & operator<< (QDataStream& stream, const MessageGetConfiguration &message)
{
    stream << message.isValid() << message.sendUpdateForChange();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageGetConfiguration &message)
{
    stream >> message.isValid() >> message.sendUpdateForChange();

    return stream;
}

bool MessageGetConfiguration::operator==(const MessageGetConfiguration &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mSendUpdateForChange == aOther.d->mSendUpdateForChange;
}
