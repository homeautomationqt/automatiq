/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_MESSAGE_ERROR_H
#define AUTOMATIQ_MESSAGE_ERROR_H

#include "automatiqprotocol_export.h"

#include "uniqueidentifier.h"

class QByteArray;
class QDataStream;

class MessageErrorPrivate;

namespace Automatiq
{

enum class ERROR_TYPE;

}

class AUTOMATIQPROTOCOL_NO_EXPORT MessageError
{
public:

    static const quint64 messageId = "MessageError"_uniqueId;

    static const quint64 protocolId = "PeerProtocol"_uniqueId;

    MessageError();

    MessageError(const MessageError &aOther);

    MessageError(MessageError &&aOther);

    ~MessageError();

    MessageError& operator=(const MessageError &aOther);

    MessageError& operator=(MessageError &&aOther);

    void setValid(bool aIsValid);

    bool isValid() const;

    bool& isValid();

    void setErrorType(Automatiq::ERROR_TYPE ack) const;

    Automatiq::ERROR_TYPE errorType() const;

    Automatiq::ERROR_TYPE& errorType();

    void setSequenceNumber(quint64 aSequenceNumber) const;

    quint64 sequenceNumber() const;

    quint64& sequenceNumber();

    bool operator==(const MessageError &aOther) const;

private:

    MessageErrorPrivate *d;

};

QDataStream & operator<< (QDataStream& stream, const MessageError& message) AUTOMATIQPROTOCOL_NO_EXPORT;
QDataStream & operator>> (QDataStream& stream, MessageError& message) AUTOMATIQPROTOCOL_NO_EXPORT;

#endif
