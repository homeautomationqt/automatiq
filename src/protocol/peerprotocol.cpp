/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "peerprotocol.h"

#include "networkpeer.h"
#include "messagecontainer.h"
#include "uniqueidentifier.h"
#include "abstractprotocol.h"
#include "commondeclarations.h"

#include "messagehello.h"
#include "messagehelloacknowledge.h"
#include "messagegetvariables.h"
#include "messageacknowledge.h"
#include "messageerror.h"

#include "configprotocol.h"
#include "dataprotocol.h"
#include "synchronizationprotocol.h"

#include <QtCore/QDateTime>
#include <QtCore/QMap>

class PeerProtocolPrivate
{
public:

    PeerProtocolPrivate()
    {
    }

    ~PeerProtocolPrivate()
    {
    }

};

PeerProtocol::PeerProtocol(QSharedPointer<NetworkPeer> networkPeer, QObject *parent) :
    AbstractPeerProtocol(networkPeer, parent), d(new PeerProtocolPrivate)
{
}

PeerProtocol::~PeerProtocol()
{
    delete d;
}

void PeerProtocol::initProtocolDialog(const QString &peerOwnName, Automatiq::peerId peerId, NewPeerDialogInitialization initializationMode)
{
    setOwnName(peerOwnName);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);
    myContainer->setVersion(1);

    switch (initializationMode)
    {
    case NewPeerDialogInitialization::SendPeerId:
    {
        MessageHelloAcknowledge myHello;
        myHello.setTimestamp(QDateTime::currentDateTime());
        myHello.setPeerName(peerOwnName);
        Q_EMIT remotePeerIdChanged();
        myHello.setValid(true);
        myContainer->setSpecificMessage(myHello);

        break;
    }
    case NewPeerDialogInitialization::WaitPeerId:
    {
        MessageHello myHello;
        myHello.setTimestamp(QDateTime::currentDateTime());
        myHello.setPeerName(peerOwnName);
        myHello.setValid(true);
        myContainer->setSpecificMessage(myHello);

        break;
    }
    }

    myContainer->setSequenceNumber(lastSequenceNumber());
    ++lastSequenceNumber();

    sendMessage(myContainer);

    setInitState(ProtocolInitializationState::NotInitialized);
    Q_EMIT changeState(remotePeerId(), initState());
}

void PeerProtocol::manageNotInitializedProtocol(QSharedPointer<const MessageContainer> aLastReceivedMessage)
{
    switch(aLastReceivedMessage->identifier())
    {
    case MessageAcknowledge::messageId:
    {
        QScopedPointer<MessageAcknowledge> ackMsg(aLastReceivedMessage->specificMessage<MessageAcknowledge>());
        QSharedPointer<MessageContainer> ackedMessage = getAcknowledgedMessage(ackMsg->sequenceNumber());
        if (!ackedMessage || ackedMessage->identifier() != MessageHello::messageId) {
            sendError(Automatiq::ERROR_TYPE::UnexpectedMessage, aLastReceivedMessage->sequenceNumber());
        }

        break;
    }
    case MessageHello::messageId:
    {
        QScopedPointer<MessageHello> helloMessage(aLastReceivedMessage->specificMessage<MessageHello>());

        if (!helloMessage.isNull() && helloMessage->isValid() && !helloMessage->peerName().isEmpty()) {
            setInitState(ProtocolInitializationState::Initialized);
            networkPeer()->setPeerName(helloMessage->peerName());
            Q_EMIT remotePeerNameChanged();

            sendAcknowledgement(ACKNOWLEDGE_TYPE::OK, aLastReceivedMessage->sequenceNumber());

            Q_EMIT changeState(remotePeerId(), initState());
        } else {
            sendAcknowledgement(ACKNOWLEDGE_TYPE::KO, aLastReceivedMessage->sequenceNumber());
        }

        break;
    }
    case MessageHelloAcknowledge::messageId:
    {
        QScopedPointer<MessageHelloAcknowledge> helloMessage(aLastReceivedMessage->specificMessage<MessageHelloAcknowledge>());

        if (!helloMessage.isNull() && helloMessage->isValid() && !helloMessage->peerName().isEmpty()) {
            setInitState(ProtocolInitializationState::Initialized);
            networkPeer()->setPeerName(helloMessage->peerName());
            Q_EMIT remotePeerNameChanged();

            sendAcknowledgement(ACKNOWLEDGE_TYPE::OK, aLastReceivedMessage->sequenceNumber());

            Q_EMIT changeState(networkPeer()->peerId(), initState());
        } else {
            sendAcknowledgement(ACKNOWLEDGE_TYPE::KO, aLastReceivedMessage->sequenceNumber());
        }

        break;
    }
    default:
        sendError(Automatiq::ERROR_TYPE::UnexpectedMessage, aLastReceivedMessage->sequenceNumber());
        break;
    }
}

void PeerProtocol::manageInitializedProtocol(QSharedPointer<const MessageContainer> aLastReceivedMessage)
{
    switch(aLastReceivedMessage->identifier())
    {
    case MessageAcknowledge::messageId:
    {
        QSharedPointer<MessageAcknowledge> ackMsg(aLastReceivedMessage->specificMessage<MessageAcknowledge>());
        QSharedPointer<MessageContainer> ackedMessage = getAcknowledgedMessage(ackMsg->sequenceNumber());
        if (ackedMessage) {
            QSharedPointer<AbstractProtocol> subProtocol = getSubProtocol(ackedMessage->protocolIdentifier());
            if (subProtocol) {
                subProtocol->protocolReceivedAcknowledge(aLastReceivedMessage, ackedMessage);
            } else {
                if (ackedMessage->protocolIdentifier() != PeerProtocol::protocolId) {
                    sendError(Automatiq::ERROR_TYPE::UnexpectedMessage, aLastReceivedMessage->sequenceNumber());
                }
            }
        } else {
            sendError(Automatiq::ERROR_TYPE::UnexpectedMessage, aLastReceivedMessage->sequenceNumber());
        }
        break;
    }
    case MessageHello::messageId:
    {
        sendAcknowledgement(ACKNOWLEDGE_TYPE::KO, aLastReceivedMessage->sequenceNumber());
        break;
    }
    default:
    {
        QSharedPointer<AbstractProtocol> subProtocol = getSubProtocol(aLastReceivedMessage->protocolIdentifier());
        if (subProtocol) {
            subProtocol->protocolReceivedMessage(aLastReceivedMessage);
        } else {
            sendError(Automatiq::ERROR_TYPE::UnexpectedMessage, aLastReceivedMessage->sequenceNumber());
        }
        break;
    }
    }
}

void PeerProtocol::sendAcknowledgement(ACKNOWLEDGE_TYPE ack, quint64 sequenceNumber)
{
    MessageAcknowledge myAck;
    myAck.setValid(true);
    myAck.setAcknowledge(ack);
    myAck.setSequenceNumber(sequenceNumber);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);

    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myAck);
    myContainer->setSequenceNumber(sequenceNumber);

    Q_EMIT newMessageToSend(myContainer);
}

void PeerProtocol::sendError(Automatiq::ERROR_TYPE error, quint64 sequenceNumber)
{
    MessageError myErrorMessage;
    myErrorMessage.setValid(true);
    myErrorMessage.setErrorType(error);
    myErrorMessage.setSequenceNumber(sequenceNumber);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);

    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myErrorMessage);

    Q_EMIT newMessageToSend(myContainer);
}

void PeerProtocol::sendPeerName(const QString &peerName)
{
    MessageHello myNewNameMessage;
    myNewNameMessage.setValid(true);
    myNewNameMessage.setPeerName(peerName);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);

    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myNewNameMessage);

    Q_EMIT newMessageToSend(myContainer);
}

QSharedPointer<AbstractConfigProtocol> PeerProtocol::createSubConfigProtocol(const QString &peerOwnName)
{
    return QSharedPointer<AbstractConfigProtocol>(new ConfigProtocol(peerOwnName, remotePeerId()));
}

QSharedPointer<AbstractDataProtocol> PeerProtocol::createSubDataProtocol(const QString &peerOwnName)
{
    return QSharedPointer<AbstractDataProtocol>(new DataProtocol(peerOwnName, remotePeerId()));
}

QSharedPointer<AbstractSynchronizationProtocol> PeerProtocol::createSubSynchronizationProtocol(const QString &peerOwnName)
{
    return QSharedPointer<AbstractSynchronizationProtocol>(new SynchronizationProtocol(peerOwnName, remotePeerId()));
}

#include "moc_peerprotocol.cpp"
