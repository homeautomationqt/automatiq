/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_MESSAGE_HELLO_ACKNOWLEDGE_H
#define AUTOMATIQ_MESSAGE_HELLO_ACKNOWLEDGE_H

#include "automatiqprotocol_export.h"

#include "uniqueidentifier.h"

class QByteArray;
class QDataStream;
class QDateTime;

class MessageHelloAcknowledgePrivate;

class AUTOMATIQPROTOCOL_NO_EXPORT MessageHelloAcknowledge
{
public:

    static const quint64 messageId = "MessageHelloAcknowledgeAcknowledge"_uniqueId;

    static const quint64 protocolId = "PeerProtocol"_uniqueId;

    MessageHelloAcknowledge();

    MessageHelloAcknowledge(const MessageHelloAcknowledge &aOther);

    MessageHelloAcknowledge(MessageHelloAcknowledge &&aOther);

    ~MessageHelloAcknowledge();

    MessageHelloAcknowledge& operator=(const MessageHelloAcknowledge &aOther);

    MessageHelloAcknowledge& operator=(MessageHelloAcknowledge &&aOther);

    void setValid(bool aIsValid);

    bool isValid() const;

    bool& isValid();

    void setTimestamp(const QDateTime &aTimestamp);

    const QDateTime& timestamp() const;

    QDateTime& timestamp();

    void setPeerName(const QString &aName);

    const QString& peerName() const;

    QString& peerName();

    bool operator==(const MessageHelloAcknowledge &aOther) const;

private:

    MessageHelloAcknowledgePrivate *d;

};

QDataStream & operator<< (QDataStream& stream, const MessageHelloAcknowledge& message) AUTOMATIQPROTOCOL_NO_EXPORT;
QDataStream & operator>> (QDataStream& stream, MessageHelloAcknowledge& message) AUTOMATIQPROTOCOL_NO_EXPORT;

#endif
