/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messageacknowledge.h"

#include <QtCore/QDataStream>
#include <QtCore/QString>
#include <QtCore/QTime>

class MessageAcknowledgePrivate
{
public:

    MessageAcknowledgePrivate() : mIsValid(false), mAcknowledge(ACKNOWLEDGE_TYPE::KO), mSequenceNumber()
    {
    }

    bool mIsValid;

    ACKNOWLEDGE_TYPE mAcknowledge;

    quint64 mSequenceNumber;
};

MessageAcknowledge::MessageAcknowledge()
    : d(new MessageAcknowledgePrivate)
{
}

MessageAcknowledge::MessageAcknowledge(const MessageAcknowledge &aOther)
    : d(new MessageAcknowledgePrivate(*aOther.d))
{
}

MessageAcknowledge::MessageAcknowledge(MessageAcknowledge &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageAcknowledge::~MessageAcknowledge()
{
    delete d;
}

MessageAcknowledge& MessageAcknowledge::operator=(const MessageAcknowledge &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mAcknowledge = aOther.d->mAcknowledge;
        d->mSequenceNumber = aOther.d->mSequenceNumber;
    }

    return *this;
}

MessageAcknowledge& MessageAcknowledge::operator=(MessageAcknowledge &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageAcknowledge::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageAcknowledge::isValid() const
{
    return d->mIsValid;
}

bool& MessageAcknowledge::isValid()
{
    return d->mIsValid;
}
void MessageAcknowledge::setAcknowledge(ACKNOWLEDGE_TYPE ack) const
{
    d->mAcknowledge = ack;
}

ACKNOWLEDGE_TYPE MessageAcknowledge::acknowledge() const
{
    return d->mAcknowledge;
}

ACKNOWLEDGE_TYPE& MessageAcknowledge::acknowledge()
{
    return d->mAcknowledge;
}

void MessageAcknowledge::setSequenceNumber(quint64 aSequenceNumber) const
{
    d->mSequenceNumber = aSequenceNumber;
}

quint64 MessageAcknowledge::sequenceNumber() const
{
    return d->mSequenceNumber;
}

quint64& MessageAcknowledge::sequenceNumber()
{
    return d->mSequenceNumber;
}

QDataStream & operator<< (QDataStream& stream, const MessageAcknowledge& message)
{
    stream << message.isValid() << static_cast<quint32>(message.acknowledge()) << message.sequenceNumber();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageAcknowledge& message)
{
    quint32 acknowledgeReadValue;
    stream >> message.isValid() >> acknowledgeReadValue >> message.sequenceNumber();
    message.acknowledge() = static_cast<ACKNOWLEDGE_TYPE>(acknowledgeReadValue);

    return stream;
}

bool MessageAcknowledge::operator==(const MessageAcknowledge &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mAcknowledge == aOther.d->mAcknowledge && d->mSequenceNumber == aOther.d->mSequenceNumber;
}
