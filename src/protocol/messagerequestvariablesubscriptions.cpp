/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagerequestvariablesubscriptions.h"

#include "datavariablesubscriptionrequest.h"

#include <QtCore/QDataStream>

class MessageRequestVariableSubscriptionsPrivate
{
public:

    bool mIsValid;

    MessageRequestVariableSubscriptions::SubscribeRequests mSubscribeRequests;

    MessageRequestVariableSubscriptionsPrivate() : mIsValid(false), mSubscribeRequests()
    {
    }

    explicit MessageRequestVariableSubscriptionsPrivate(const MessageRequestVariableSubscriptions::SubscribeRequests &aSubscribeRequests)
        : mIsValid(true), mSubscribeRequests(aSubscribeRequests)
    {
    }
};

MessageRequestVariableSubscriptions::MessageRequestVariableSubscriptions()
    : d(new MessageRequestVariableSubscriptionsPrivate)
{
}

MessageRequestVariableSubscriptions::MessageRequestVariableSubscriptions(const SubscribeRequests &aSubscribeRequests)
    : d(new MessageRequestVariableSubscriptionsPrivate(aSubscribeRequests))
{
}

MessageRequestVariableSubscriptions::MessageRequestVariableSubscriptions(const MessageRequestVariableSubscriptions &aOther)
    : d(new MessageRequestVariableSubscriptionsPrivate(*aOther.d))
{
}

MessageRequestVariableSubscriptions::MessageRequestVariableSubscriptions(MessageRequestVariableSubscriptions &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageRequestVariableSubscriptions::~MessageRequestVariableSubscriptions()
{
    delete d;
}

MessageRequestVariableSubscriptions& MessageRequestVariableSubscriptions::operator=(const MessageRequestVariableSubscriptions &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mSubscribeRequests = aOther.d->mSubscribeRequests;
    }

    return *this;
}

MessageRequestVariableSubscriptions& MessageRequestVariableSubscriptions::operator=(MessageRequestVariableSubscriptions &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageRequestVariableSubscriptions::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageRequestVariableSubscriptions::isValid() const
{
    return d->mIsValid;
}

bool& MessageRequestVariableSubscriptions::isValid()
{
    return d->mIsValid;
}

void MessageRequestVariableSubscriptions::setSubscribeRequests(const MessageRequestVariableSubscriptions::SubscribeRequests &requests)
{
    d->mSubscribeRequests = requests;
}

const MessageRequestVariableSubscriptions::SubscribeRequests &MessageRequestVariableSubscriptions::subscribeRequests() const
{
    return d->mSubscribeRequests;
}

MessageRequestVariableSubscriptions::SubscribeRequests &MessageRequestVariableSubscriptions::subscribeRequests()
{
    return d->mSubscribeRequests;
}

QDataStream & operator<< (QDataStream& stream, const MessageRequestVariableSubscriptions& message)
{
    stream << message.isValid() << message.subscribeRequests();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageRequestVariableSubscriptions& message)
{
    stream >> message.isValid() >> message.subscribeRequests();

    return stream;
}

bool MessageRequestVariableSubscriptions::operator==(const MessageRequestVariableSubscriptions &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mSubscribeRequests == aOther.d->mSubscribeRequests;
}

