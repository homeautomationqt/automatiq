/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagelistvariablevalues.h"

#include "datavariablewithvalue.h"

#include <QtCore/QDataStream>

class MessageListVariableValuesPrivate
{
public:

    bool mIsValid;

    MessageListVariableValues::VariableValues mVariableValues;

    MessageListVariableValuesPrivate() : mIsValid(false), mVariableValues()
    {
    }

    explicit MessageListVariableValuesPrivate(const MessageListVariableValues::VariableValues &aVariableValues)
        : mIsValid(true), mVariableValues(aVariableValues)
    {
    }
};

MessageListVariableValues::MessageListVariableValues()
    : d(new MessageListVariableValuesPrivate)
{
}

MessageListVariableValues::MessageListVariableValues(const VariableValues &aVariableValues)
    : d(new MessageListVariableValuesPrivate(aVariableValues))
{
}

MessageListVariableValues::MessageListVariableValues(const MessageListVariableValues &aOther)
    : d(new MessageListVariableValuesPrivate(*aOther.d))
{
}

MessageListVariableValues::MessageListVariableValues(MessageListVariableValues &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageListVariableValues::~MessageListVariableValues()
{
    delete d;
}

MessageListVariableValues& MessageListVariableValues::operator=(const MessageListVariableValues &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mVariableValues = aOther.d->mVariableValues;
    }

    return *this;
}

MessageListVariableValues& MessageListVariableValues::operator=(MessageListVariableValues &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageListVariableValues::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageListVariableValues::isValid() const
{
    return d->mIsValid;
}

bool& MessageListVariableValues::isValid()
{
    return d->mIsValid;
}

void MessageListVariableValues::setVariableValues(const MessageListVariableValues::VariableValues &aVariableValues)
{
    d->mVariableValues = aVariableValues;
}

const MessageListVariableValues::VariableValues &MessageListVariableValues::variableValues() const
{
    return d->mVariableValues;
}

MessageListVariableValues::VariableValues &MessageListVariableValues::variableValues()
{
    return d->mVariableValues;
}

QDataStream & operator<< (QDataStream& stream, const MessageListVariableValues& message)
{
    stream << message.isValid() << message.variableValues();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageListVariableValues& message)
{
    stream >> message.isValid() >> message.variableValues();

    return stream;
}

bool MessageListVariableValues::operator==(const MessageListVariableValues &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mVariableValues == aOther.d->mVariableValues;
}

