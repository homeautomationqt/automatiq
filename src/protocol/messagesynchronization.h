/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_MESSAGE_SYNCHRONIZATION_H
#define AUTOMATIQ_MESSAGE_SYNCHRONIZATION_H

#include "automatiqprotocol_export.h"

#include "uniqueidentifier.h"

#include "commondeclarations.h"

class QDataStream;

class MessageSynchronizationPrivate;

class AUTOMATIQPROTOCOL_NO_EXPORT MessageSynchronization
{
public:

    static const quint64 messageId = "MessageSynchronization"_uniqueId;

    static const quint64 protocolId = "SynchronizationProtocol"_uniqueId;

    MessageSynchronization();

    MessageSynchronization(const MessageSynchronization &aOther);

    MessageSynchronization(MessageSynchronization &&aOther);

    ~MessageSynchronization();

    MessageSynchronization& operator=(const MessageSynchronization &aOther);

    MessageSynchronization& operator=(MessageSynchronization &&aOther);

    void setValid(bool aIsValid);

    bool isValid() const;

    bool& isValid();

    void setSynchronizationStep(Automatiq::DataProcessingSteps step);

    Automatiq::DataProcessingSteps synchronizationStep() const;

    Automatiq::DataProcessingSteps& synchronizationStep();

    bool operator==(const MessageSynchronization &aOther) const;

private:

    MessageSynchronizationPrivate *d;

};

QDataStream & operator<< (QDataStream& stream, const MessageSynchronization& message) AUTOMATIQPROTOCOL_NO_EXPORT;
QDataStream & operator>> (QDataStream& stream, MessageSynchronization& message) AUTOMATIQPROTOCOL_NO_EXPORT;

#endif
