/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "dataprotocol.h"

#include "messagegetvariables.h"
#include "messagelistvariables.h"
#include "messagelistvariablevalues.h"
#include "messagecontainer.h"
#include "messageacknowledge.h"
#include "messagesubscribevariables.h"
#include "messagerequestvariablesubscriptions.h"

#include "datavariablesubscriptionrequest.h"
#include "datavariableidentifier.h"
#include "datavariablewithvalue.h"

#include <QtCore/QDebug>

class DataProtocolPrivate
{
public:

    DataProtocolPrivate() : mPeerOwnName()
    {
    }

    QString mPeerOwnName;
};

DataProtocol::DataProtocol(const QString &aPeerOwnName, Automatiq::peerId aRemotePeerId, QObject *parent)
    : AbstractDataProtocol(aRemotePeerId, parent), d(new DataProtocolPrivate)
{
    d->mPeerOwnName = aPeerOwnName;
}

DataProtocol::~DataProtocol()
{
    delete d;
}

quint64 DataProtocol::protocolId()
{
    return DataProtocol::valueProtocolId;
}

void DataProtocol::sendAcknowledgement(ACKNOWLEDGE_TYPE ack, quint64 sequenceNumber)
{
    MessageAcknowledge myAck;
    myAck.setValid(true);
    myAck.setAcknowledge(ack);
    myAck.setSequenceNumber(sequenceNumber);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);

    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myAck);

    Q_EMIT newMessageToSend(myContainer);
}

void DataProtocol::protocolReceivedAcknowledge(QSharedPointer<const MessageContainer> acknowledge, QSharedPointer<const MessageContainer> originalMessage)
{
    if (originalMessage->protocolIdentifier() == DataProtocol::valueProtocolId) {
        switch (originalMessage->identifier())
        {
        case MessageGetVariables::messageId:
            break;
        case MessageListVariables::messageId:
            break;
        case MessageListVariableValues::messageId:
            break;
        case MessageRequestVariableSubscriptions::messageId:
            break;
        case MessageSubscribeVariables::messageId:
            break;
        }
    }
}

void DataProtocol::protocolReceivedMessage(QSharedPointer<const MessageContainer> receivedMessage)
{
    if (receivedMessage->protocolIdentifier() == DataProtocol::valueProtocolId) {
        switch (receivedMessage->identifier())
        {
        case MessageGetVariables::messageId:
        {
            QScopedPointer<MessageGetVariables> newMsg(receivedMessage->specificMessage<MessageGetVariables>());
            sendAcknowledgement(ACKNOWLEDGE_TYPE::KO, receivedMessage->sequenceNumber());

            Q_EMIT receivedRequestForListVariables(peerId());

            break;
        }
        case MessageListVariables::messageId:
        {
            QScopedPointer<MessageListVariables> newMsg(receivedMessage->specificMessage<MessageListVariables>());
            sendAcknowledgement(ACKNOWLEDGE_TYPE::KO, receivedMessage->sequenceNumber());

            Q_EMIT receivedVariablesList(peerId(), newMsg->variableDescriptors());

            break;
        }
        case MessageListVariableValues::messageId:
        {
            QScopedPointer<MessageListVariableValues> newMsg(receivedMessage->specificMessage<MessageListVariableValues>());
            sendAcknowledgement(ACKNOWLEDGE_TYPE::OK, receivedMessage->sequenceNumber());

            Q_EMIT receivedVariableValues(peerId(), newMsg->variableValues());

            break;
        }
        case MessageSubscribeVariables::messageId:
        {
            QScopedPointer<MessageSubscribeVariables> newMsg(receivedMessage->specificMessage<MessageSubscribeVariables>());
            sendAcknowledgement(ACKNOWLEDGE_TYPE::OK, receivedMessage->sequenceNumber());

            Q_EMIT receivedVariableSubscriptionsList(peerId(), newMsg->variableSubscriptions());

            break;
        }
        case MessageRequestVariableSubscriptions::messageId:
        {
            QScopedPointer<MessageRequestVariableSubscriptions> newMsg(receivedMessage->specificMessage<MessageRequestVariableSubscriptions>());
            sendAcknowledgement(ACKNOWLEDGE_TYPE::OK, receivedMessage->sequenceNumber());

            Q_EMIT receivedVariableSubscriptionRequestsList(peerId(), newMsg->subscribeRequests());

            break;
        }
        }
    }
}

void DataProtocol::askVariablesList()
{
    MessageGetVariables myMessage;
    myMessage.setValid(true);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);
    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myMessage);

    Q_EMIT newMessageToSend(myContainer);
}

void DataProtocol::sendVariablesList(const QList<DataVariableDescription> &variables)
{
    MessageListVariables myMessage;
    myMessage.setValid(true);
    myMessage.setVariableDescriptors(variables);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);
    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myMessage);

    Q_EMIT newMessageToSend(myContainer);
}

void DataProtocol::sendVariableValues(const QList<DataVariableWithValue> &values)
{
    MessageListVariableValues myMessage;
    myMessage.setValid(true);
    myMessage.setVariableValues(values);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);
    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myMessage);

    Q_EMIT newMessageToSend(myContainer);
}

void DataProtocol::sendVariableSubscriptions(const QList<DataVariableSubscription> &variables)
{
    MessageSubscribeVariables myMessage;
    myMessage.setValid(true);
    myMessage.setVariableSubscriptions(variables);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);
    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myMessage);

    Q_EMIT newMessageToSend(myContainer);
}

void DataProtocol::sendVariableSubscriptionRequests(const QList<DataVariableSubscriptionRequest> &requests)
{
    MessageRequestVariableSubscriptions myMessage;
    myMessage.setValid(true);
    myMessage.setSubscribeRequests(requests);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);
    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myMessage);

    Q_EMIT newMessageToSend(myContainer);
}

#include "moc_dataprotocol.cpp"
