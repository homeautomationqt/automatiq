/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messageconfiguration.h"

#include <QtCore/QDataStream>
#include <QtCore/QSettings>

class MessageConfigurationPrivate
{
public:

    MessageConfigurationPrivate() : mIsValid(false), mConfiguration()
    {
    }

    bool mIsValid;

    MessageConfiguration::configuration_type mConfiguration;
};

MessageConfiguration::MessageConfiguration()
    : d(new MessageConfigurationPrivate)
{
}

MessageConfiguration::MessageConfiguration(const MessageConfiguration &aOther)
    : d(new MessageConfigurationPrivate(*aOther.d))
{
}

MessageConfiguration::MessageConfiguration(MessageConfiguration &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageConfiguration::~MessageConfiguration()
{
    delete d;
}

MessageConfiguration& MessageConfiguration::operator=(const MessageConfiguration &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mConfiguration = aOther.d->mConfiguration;
    }

    return *this;
}

MessageConfiguration& MessageConfiguration::operator=(MessageConfiguration &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageConfiguration::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageConfiguration::isValid() const
{
    return d->mIsValid;
}

bool& MessageConfiguration::isValid()
{
    return d->mIsValid;
}

void MessageConfiguration::setConfiguration(const configuration_type &aConfiguration)
{
    d->mConfiguration = aConfiguration;
}

const MessageConfiguration::configuration_type &MessageConfiguration::configuration() const
{
    return d->mConfiguration;
}

MessageConfiguration::configuration_type &MessageConfiguration::configuration()
{
    return d->mConfiguration;
}

QDataStream & operator<< (QDataStream& stream, const MessageConfiguration& message)
{
    stream << message.isValid() << message.configuration();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageConfiguration& message)
{
    stream >> message.isValid() >> message.configuration();

    return stream;
}

bool MessageConfiguration::operator==(const MessageConfiguration &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mConfiguration == aOther.d->mConfiguration;
}
