/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "configprotocol.h"

#include "messagegetconfiguration.h"
#include "messageconfiguration.h"
#include "messageacknowledge.h"

#include "messagecontainer.h"

#include <QtCore/QDebug>

class ConfigProtocolPrivate
{
public:

    ConfigProtocolPrivate() : mPeerOwnName()
    {
    }

    QString mPeerOwnName;
};

ConfigProtocol::ConfigProtocol(const QString &aPeerOwnName, Automatiq::peerId aRemotePeerId, QObject *parent) :
    AbstractConfigProtocol(aRemotePeerId, parent), d(new ConfigProtocolPrivate)
{
    d->mPeerOwnName = aPeerOwnName;
}

ConfigProtocol::~ConfigProtocol()
{
    delete d;
}

quint64 ConfigProtocol::protocolId()
{
    return ConfigProtocol::valueProtocolId;
}

void ConfigProtocol::sendAcknowledgement(ACKNOWLEDGE_TYPE ack, quint64 sequenceNumber)
{
    MessageAcknowledge myAck;
    myAck.setValid(true);
    myAck.setAcknowledge(ack);
    myAck.setSequenceNumber(sequenceNumber);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);

    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myAck);

    Q_EMIT newMessageToSend(myContainer);
}

void ConfigProtocol::protocolReceivedAcknowledge(QSharedPointer<const MessageContainer> acknowledge, QSharedPointer<const MessageContainer> originalMessage)
{
    if (originalMessage->protocolIdentifier() == protocolId()) {
        switch (originalMessage->identifier())
        {
        case MessageConfiguration::messageId:
            break;
        case MessageGetConfiguration::messageId:
            break;
        }
    }
}

void ConfigProtocol::protocolReceivedMessage(QSharedPointer<const MessageContainer> receivedMessage)
{
    if (receivedMessage->protocolIdentifier() == protocolId()) {
        switch (receivedMessage->identifier())
        {
        case MessageConfiguration::messageId:
        {
            QScopedPointer<MessageConfiguration> newMsg(receivedMessage->specificMessage<MessageConfiguration>());
            sendAcknowledgement(ACKNOWLEDGE_TYPE::OK, receivedMessage->sequenceNumber());

            Q_EMIT receivedConfiguration(peerId(), newMsg->configuration());

            break;
        }
        case MessageGetConfiguration::messageId:
            sendAcknowledgement(ACKNOWLEDGE_TYPE::KO, receivedMessage->sequenceNumber());

            Q_EMIT requestConfiguration(peerId(), receivedMessage->specificMessage<MessageGetConfiguration>()->sendUpdateForChange());

            break;
        }
    }
}

void ConfigProtocol::askRemotePeerConfiguration(bool sendUpdateOnChange)
{
    MessageGetConfiguration myMessage;
    myMessage.setValid(true);
    myMessage.setSendUpdateForChange(sendUpdateOnChange);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);
    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myMessage);

    Q_EMIT newMessageToSend(myContainer);
}

void ConfigProtocol::sendConfigurationToRemotePeer(const Automatiq::configuration_type &aConfiguration)
{
    MessageConfiguration myMessage;
    myMessage.setConfiguration(aConfiguration);
    myMessage.setValid(true);

    QSharedPointer<MessageContainer> myContainer(new MessageContainer);
    myContainer->setVersion(1);
    myContainer->setSpecificMessage(myMessage);

    Q_EMIT newMessageToSend(myContainer);
}

#include "moc_configprotocol.cpp"
