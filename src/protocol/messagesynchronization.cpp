/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagesynchronization.h"

#include <QtCore/QDataStream>

class MessageSynchronizationPrivate
{
public:

    MessageSynchronizationPrivate() : mIsValid(false), mStep(Automatiq::DataProcessingSteps::Waiting)
    {
    }

    bool mIsValid;

    Automatiq::DataProcessingSteps mStep;
};

MessageSynchronization::MessageSynchronization()
    : d(new MessageSynchronizationPrivate)
{
}

MessageSynchronization::MessageSynchronization(const MessageSynchronization &aOther)
    : d(new MessageSynchronizationPrivate(*aOther.d))
{
}

MessageSynchronization::MessageSynchronization(MessageSynchronization &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageSynchronization::~MessageSynchronization()
{
    delete d;
}

MessageSynchronization& MessageSynchronization::operator=(const MessageSynchronization &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
        d->mStep = aOther.d->mStep;
    }

    return *this;
}

MessageSynchronization& MessageSynchronization::operator=(MessageSynchronization &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageSynchronization::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageSynchronization::isValid() const
{
    return d->mIsValid;
}

bool& MessageSynchronization::isValid()
{
    return d->mIsValid;
}

void MessageSynchronization::setSynchronizationStep(Automatiq::DataProcessingSteps step)
{
    d->mStep = step;
}

Automatiq::DataProcessingSteps MessageSynchronization::synchronizationStep() const
{
    return d->mStep;
}

Automatiq::DataProcessingSteps &MessageSynchronization::synchronizationStep()
{
    return d->mStep;
}

QDataStream & operator<< (QDataStream& stream, const MessageSynchronization& message)
{
    stream << message.isValid();
    stream << int32_t(message.synchronizationStep());

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageSynchronization& message)
{
    stream >> message.isValid();
    int32_t step;
    stream >> step;
    message.setSynchronizationStep(static_cast<Automatiq::DataProcessingSteps>(step));

    return stream;
}

bool MessageSynchronization::operator==(const MessageSynchronization &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mStep == aOther.d->mStep;
}
