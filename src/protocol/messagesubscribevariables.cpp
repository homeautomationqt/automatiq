/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "messagesubscribevariables.h"

#include "datavariablesubscription.h"

#include <QtCore/QDataStream>

class MessageSubscribeVariablesPrivate
{
public:

    bool mIsValid;

    MessageSubscribeVariables::VariableSubscriptions mSubscriptions;

    MessageSubscribeVariablesPrivate() : mIsValid(false), mSubscriptions()
    {
    }

    MessageSubscribeVariablesPrivate(bool valid, MessageSubscribeVariables::VariableSubscriptions subscriptions) : mIsValid(valid), mSubscriptions(subscriptions)
    {
    }
};

MessageSubscribeVariables::MessageSubscribeVariables()
    : d(new MessageSubscribeVariablesPrivate)
{
}

MessageSubscribeVariables::MessageSubscribeVariables(const VariableSubscriptions &subscriptions)
    : d(new MessageSubscribeVariablesPrivate(true, subscriptions))
{
}

MessageSubscribeVariables::MessageSubscribeVariables(const MessageSubscribeVariables &aOther)
    : d(new MessageSubscribeVariablesPrivate(*aOther.d))
{
}

MessageSubscribeVariables::MessageSubscribeVariables(MessageSubscribeVariables &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

MessageSubscribeVariables::~MessageSubscribeVariables()
{
    delete d;
}

MessageSubscribeVariables& MessageSubscribeVariables::operator=(const MessageSubscribeVariables &aOther)
{
    if (this != &aOther) {
        d->mIsValid = aOther.d->mIsValid;
    }

    return *this;
}

MessageSubscribeVariables& MessageSubscribeVariables::operator=(MessageSubscribeVariables &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void MessageSubscribeVariables::setValid(bool aIsValid)
{
    d->mIsValid = aIsValid;
}

bool MessageSubscribeVariables::isValid() const
{
    return d->mIsValid;
}

bool& MessageSubscribeVariables::isValid()
{
    return d->mIsValid;
}

void MessageSubscribeVariables::setVariableSubscriptions(const MessageSubscribeVariables::VariableSubscriptions &subscriptions)
{
    d->mSubscriptions = subscriptions;
}

const MessageSubscribeVariables::VariableSubscriptions &MessageSubscribeVariables::variableSubscriptions() const
{
    return d->mSubscriptions;
}

MessageSubscribeVariables::VariableSubscriptions &MessageSubscribeVariables::variableSubscriptions()
{
    return d->mSubscriptions;
}

QDataStream & operator<< (QDataStream& stream, const MessageSubscribeVariables& message)
{
    stream << message.isValid() << message.variableSubscriptions();

    return stream;
}

QDataStream & operator>> (QDataStream& stream, MessageSubscribeVariables& message)
{
    stream >> message.isValid() >> message.variableSubscriptions();

    return stream;
}

bool MessageSubscribeVariables::operator==(const MessageSubscribeVariables &aOther) const
{
    return d->mIsValid == aOther.d->mIsValid && d->mSubscriptions == aOther.d->mSubscriptions;
}

