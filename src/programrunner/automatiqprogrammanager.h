/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_AUTOMATIQ_PROGRAM_MANAGER_H
#define AUTOMATIQ_AUTOMATIQ_PROGRAM_MANAGER_H

#include "automatiqprogramrunner_export.h"

#include <QtQml/QQmlListProperty>

#include <QtCore/QObject>

class AutomatiqProgramManagerPrivate;
class AutomatiqProgramRunner;

class AUTOMATIQPROGRAMRUNNER_EXPORT AutomatiqProgramManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<AutomatiqProgramRunner> runners READ runners)
    Q_CLASSINFO("DefaultProperty", "runners")

public:

    explicit AutomatiqProgramManager(QObject *parent = 0);

    ~AutomatiqProgramManager();

    Q_INVOKABLE void initialize();

    Q_INVOKABLE void start();

    QQmlListProperty<AutomatiqProgramRunner> runners();

Q_SIGNALS:

    void programExecutionIsFinished();

    void triggerRunnerExecution();

public Q_SLOTS:

    void launchProgramExecution();

    void runnerExecutionIsFinished();

private:

    AutomatiqProgramManagerPrivate *d;

};

#endif
