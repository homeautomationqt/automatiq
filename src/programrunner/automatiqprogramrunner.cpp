/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "automatiqprogramrunner.h"
#include "automatiqprogram.h"

#include <QtCore/QMap>
#include <QtCore/QDebug>
#include <QtCore/QDateTime>

class AutomatiqProgramRunnerPrivate
{
public:

    QList<AutomatiqProgram*> mPrograms;

    QString mName;

    int mPeriod;
};

AutomatiqProgramRunner::AutomatiqProgramRunner(QObject *parent) : QObject(parent), d(new AutomatiqProgramRunnerPrivate)
{
}

AutomatiqProgramRunner::~AutomatiqProgramRunner()
{
    delete d;
}

void AutomatiqProgramRunner::setName(const QString &runnerName)
{
    d->mName = runnerName;
    Q_EMIT nameChanged();
}

const QString &AutomatiqProgramRunner::name() const
{
    return d->mName;
}

void AutomatiqProgramRunner::setPeriod(int milliseconds)
{
    d->mPeriod = milliseconds;
    Q_EMIT periodChanged();
}

int AutomatiqProgramRunner::period() const
{
    return d->mPeriod;
}

void AutomatiqProgramRunner::initialize()
{
    for (QList<AutomatiqProgram*>::iterator itProgram = d->mPrograms.begin(); itProgram != d->mPrograms.end(); ++itProgram) {
        (*itProgram)->init();
    }
}

QQmlListProperty<AutomatiqProgram> AutomatiqProgramRunner::programs()
{
    return QQmlListProperty<AutomatiqProgram>(this, d->mPrograms);
}

void AutomatiqProgramRunner::runPrograms()
{
    for (QList<AutomatiqProgram*>::iterator itProgram = d->mPrograms.begin(); itProgram != d->mPrograms.end(); ++itProgram) {
        (*itProgram)->run();
    }

    Q_EMIT runnerExecutionIsFinished();
}

#include "moc_automatiqprogramrunner.cpp"
