/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_AUTOMATIQ_PROGRAM_RUNNER_H
#define AUTOMATIQ_AUTOMATIQ_PROGRAM_RUNNER_H

#include "automatiqprogramrunner_export.h"

#include <QtQml/QQmlListProperty>

#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

class AutomatiqProgramRunnerPrivate;
class AutomatiqProgram;

class AUTOMATIQPROGRAMRUNNER_EXPORT AutomatiqProgramRunner : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name
               READ name
               WRITE setName
               NOTIFY nameChanged)

    Q_PROPERTY(int period
               READ period
               WRITE setPeriod
               NOTIFY periodChanged)

    Q_PROPERTY(QQmlListProperty<AutomatiqProgram> programs READ programs)
    Q_CLASSINFO("DefaultProperty", "programs")

public:
    explicit AutomatiqProgramRunner(QObject *parent = 0);

    virtual ~AutomatiqProgramRunner();

    void setName(const QString &runnerName);

    const QString& name() const;

    void setPeriod(int milliseconds);

    int period() const;

    Q_INVOKABLE virtual void initialize();

    QQmlListProperty<AutomatiqProgram> programs();

Q_SIGNALS:

    void nameChanged();

    void periodChanged();

    void runnerExecutionIsFinished();

public Q_SLOTS:

    void runPrograms();

private:

    AutomatiqProgramRunnerPrivate *d;
};

#endif
