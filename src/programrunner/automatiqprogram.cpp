/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "automatiqprogram.h"
#include "automatiqprogramobject.h"

#include <QtCore/QPointer>
#include <QtCore/QSet>
#include <QtCore/QList>
#include <QtCore/QSharedPointer>

class AutomatiqProgramPrivate
{
public:

    QList<AutomatiqProgramObject*> mProgramObjects;

    QList<QList<AutomatiqProgramObject*> > mExecutionStack;

    QString mName;
};

AutomatiqProgram::AutomatiqProgram(QObject *parent) : QObject(parent), d(new AutomatiqProgramPrivate)
{
}

AutomatiqProgram::~AutomatiqProgram()
{
    delete d;
}

void AutomatiqProgram::init()
{
    computeExecutionOrder();
}

void AutomatiqProgram::run()
{
    for (QList<QList<AutomatiqProgramObject*> >::iterator itObjectList = d->mExecutionStack.begin(); itObjectList != d->mExecutionStack.end(); ++itObjectList) {
        for (QList<AutomatiqProgramObject*>::iterator itObject = itObjectList->begin(); itObject != itObjectList->end(); ++itObject) {
            (*itObject)->run();
        }
    }
}

void AutomatiqProgram::setName(const QString &value)
{
    d->mName = value;
}

const QString &AutomatiqProgram::name() const
{
    return d->mName;
}

QQmlListProperty<AutomatiqProgramObject> AutomatiqProgram::objects()
{
    return QQmlListProperty<AutomatiqProgramObject>(this, d->mProgramObjects);
}

void AutomatiqProgram::computeExecutionOrder()
{
    QHash<QString, int> depth;
    for (int currentDepth = 0; currentDepth < d->mProgramObjects.size(); ++currentDepth) {
        int countObjectsWithoutDepth = 0;
        for (QList<AutomatiqProgramObject*>::iterator itObject = d->mProgramObjects.begin(); itObject != d->mProgramObjects.end(); ++itObject) {
            if ((*itObject)->depth() == AutomatiqProgramObject::INVALID_DEPTH) {
                ++countObjectsWithoutDepth;
                const auto &allInterfaceVariables = (*itObject)->inputInterfaceVariables();
                if (currentDepth >= allInterfaceVariables.size()) {
                    int countInvalid = 0;
                    for (QList<AbstractInterfaceVariable *>::const_iterator itVariable = allInterfaceVariables.begin(); itVariable != allInterfaceVariables.end(); ++itVariable) {
                        if ((*itVariable)->remoteVariableHolder()->depth() == AutomatiqProgramObject::INVALID_DEPTH) {
                            ++countInvalid;
                        }
                    }
                    if (countInvalid == 0) {
                        (*itObject)->setDepth(currentDepth);

                        if (d->mExecutionStack.size() < currentDepth + 1) {
                            d->mExecutionStack.append(QList<AutomatiqProgramObject*>());
                        }

                        d->mExecutionStack[currentDepth].push_back(*itObject);
                    }
                }
            }
        }

        if (countObjectsWithoutDepth == 0) {
            break;
        }
    }
}

#include "moc_automatiqprogram.cpp"
