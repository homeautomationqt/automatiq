/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_AUTOMATIQ_PROGRAM_OBJECT_H
#define AUTOMATIQ_AUTOMATIQ_PROGRAM_OBJECT_H

#include "automatiqprogramrunner_export.h"

#include "simpledatavariableidentifier.h"

#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QVariant>

#include <functional>

class AutomatiqProgramObjectPrivate;
class AutomatiqProgramObject;

class AbstractVariableHolder
{
public:

    explicit AbstractVariableHolder(const AutomatiqProgramObject *aRemoteVariableHolder)
        : mRemoteVariableHolder(aRemoteVariableHolder)
    {
    }

    const AutomatiqProgramObject* remoteVariableHolder() const
    {
        return mRemoteVariableHolder;
    }

private:

    const AutomatiqProgramObject *mRemoteVariableHolder;
};


template <typename DATA>
class VariableHolder : public AbstractVariableHolder
{
public:

    explicit VariableHolder(const AutomatiqProgramObject *aRemoteVariableHolder)
        : AbstractVariableHolder(aRemoteVariableHolder)
    {
    }

    DATA operator++()
    {
        DATA oldValue(mData);
        ++mData;
        return oldValue;
    }

    DATA& operator++(int)
    {
        ++mData;
        return mData;
    }

    DATA data() const
    {
        return mData;
    }

    void setData(DATA value)
    {
        mData = value;
    }

private:

    DATA mData;
};

class AbstractInterfaceVariable
{
public:

    AbstractInterfaceVariable()
        : mIsConnected(false), mName()
    {
    }

    explicit AbstractInterfaceVariable(const QString &aName)
        : mIsConnected(false), mName(aName)
    {
    }

    bool isConnected() const
    {
        return mIsConnected;
    }

    const QString& name() const
    {
        return mName;
    }

    const AutomatiqProgramObject* remoteVariableHolder() const
    {
        return mRemoteVariableHolder->remoteVariableHolder();
    }

protected:

    void setIsConnected(bool value)
    {
        mIsConnected = value;
    }

    void setRemoteVariableHolder(const AbstractVariableHolder *aRemoteVariableHolder)
    {
        mRemoteVariableHolder = aRemoteVariableHolder;
    }

private:

    bool mIsConnected;

    QString mName;

    const AbstractVariableHolder *mRemoteVariableHolder;

};

template <typename DATA>
class InterfaceVariable : public AbstractInterfaceVariable
{
public:

    InterfaceVariable()
        : AbstractInterfaceVariable(), mGetter()
    {
    }

    explicit InterfaceVariable(const QString &aName)
        : AbstractInterfaceVariable(aName), mGetter()
    {
    }

    DATA data() const
    {
        return mGetter();
    }

    const AutomatiqProgramObject* remoteVariableHolder() const
    {
        return mRemoteVariableHolder;
    }

    template <typename OBJECT>
    void connectToInput(const OBJECT &outputVariable)
    {
        setRemoteVariableHolder(&outputVariable);
        std::function<DATA(const OBJECT*)> getter = &OBJECT::data;
        mGetter = std::bind(getter, &outputVariable);
        setIsConnected(true);
    }

private:

    std::function<DATA()> mGetter;

};

Q_DECLARE_METATYPE(InterfaceVariable<int>)
Q_DECLARE_METATYPE(InterfaceVariable<double>)
Q_DECLARE_METATYPE(InterfaceVariable<bool>)

class AUTOMATIQPROGRAMRUNNER_EXPORT AutomatiqProgramObject : public QObject
{
    Q_OBJECT

public:

    static const int INVALID_DEPTH = -1;

    explicit AutomatiqProgramObject(QObject *parent = 0);

    explicit AutomatiqProgramObject(const QList<AbstractInterfaceVariable *> &myInputVariables, QObject *parent = 0);

    virtual ~AutomatiqProgramObject();

    virtual void run();

    const QList<AbstractInterfaceVariable *> &inputInterfaceVariables() const;

    int depth() const;

    void setDepth(int value);

private:

    AutomatiqProgramObjectPrivate *d;
};

#endif
