/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_AUTOMATIQ_PROGRAM_H
#define AUTOMATIQ_AUTOMATIQ_PROGRAM_H

#include "automatiqprogramrunner_export.h"

#include "simpledatavariableidentifier.h"
#include "simpledatavariablewithvalue.h"

#include <QtQml/QQmlListProperty>

#include <QtCore/QList>
#include <QtCore/QVariant>

class AutomatiqProgramPrivate;
class AutomatiqProgramObject;

class AUTOMATIQPROGRAMRUNNER_EXPORT AutomatiqProgram : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name
               READ name
               WRITE setName
               NOTIFY nameChanged)

    Q_PROPERTY(QQmlListProperty<AutomatiqProgramObject> objects READ objects)
    Q_CLASSINFO("DefaultProperty", "objects")

public:
    explicit AutomatiqProgram(QObject *parent = 0);

    virtual ~AutomatiqProgram();

    void init();

    void run();

    void setName(const QString &runnerName);

    const QString& name() const;

    QQmlListProperty<AutomatiqProgramObject> objects();

Q_SIGNALS:

    void nameChanged();

private:

    void computeExecutionOrder();

    AutomatiqProgramPrivate *d;
};

#endif
