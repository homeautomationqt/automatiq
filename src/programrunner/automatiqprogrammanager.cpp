/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "automatiqprogrammanager.h"
#include "automatiqprogramrunner.h"

#include <QtCore/QMap>
#include <QtCore/QSharedPointer>
#include <QtCore/QThread>
#include <QtCore/QTimer>
#include <QtCore/QScopedPointer>

#include <QtCore/QDebug>

class AutomatiqProgramManagerPrivate
{
public:

    explicit AutomatiqProgramManagerPrivate()
        : mRunners(), mThreads(), mTimers()
    {
    }

    QList<AutomatiqProgramRunner*> mRunners;

    QMap<QString, QThread*> mThreads;

    QMap<QString, QTimer*> mTimers;
};

AutomatiqProgramManager::AutomatiqProgramManager(QObject *parent)
    : QObject(parent), d(new AutomatiqProgramManagerPrivate)
{
}

AutomatiqProgramManager::~AutomatiqProgramManager()
{
    delete d;
}

void AutomatiqProgramManager::initialize()
{
    for (auto itRunner = d->mRunners.begin(); itRunner != d->mRunners.end(); ++itRunner) {
        (*itRunner)->initialize();
        if (d->mThreads.find((*itRunner)->name()) == d->mThreads.end()) {
            d->mThreads[(*itRunner)->name()] = new QThread(this);
            (*itRunner)->setParent(nullptr);
            (*itRunner)->moveToThread(d->mThreads[(*itRunner)->name()]);
            d->mTimers[(*itRunner)->name()] = new QTimer;
            d->mTimers[(*itRunner)->name()]->moveToThread(d->mThreads[(*itRunner)->name()]);
            d->mTimers[(*itRunner)->name()]->setInterval((*itRunner)->period());
            d->mTimers[(*itRunner)->name()]->setSingleShot(false);
            connect(d->mTimers[(*itRunner)->name()], &QTimer::timeout, *itRunner, &AutomatiqProgramRunner::runPrograms);
        }
    }
}

void AutomatiqProgramManager::start()
{
    for (auto itRunner = d->mRunners.begin(); itRunner != d->mRunners.end(); ++itRunner) {
        d->mThreads[(*itRunner)->name()]->start();
        QTimer::singleShot(0, d->mTimers[(*itRunner)->name()], SLOT(start()));
    }
}

QQmlListProperty<AutomatiqProgramRunner> AutomatiqProgramManager::runners()
{
    return QQmlListProperty<AutomatiqProgramRunner>(this, d->mRunners);
}

void AutomatiqProgramManager::launchProgramExecution()
{
    Q_EMIT triggerRunnerExecution();
}

void AutomatiqProgramManager::runnerExecutionIsFinished()
{
    Q_EMIT programExecutionIsFinished();
}


#include "moc_automatiqprogrammanager.cpp"
