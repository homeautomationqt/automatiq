/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "datavariablesubscription.h"

#include "datavariableidentifier.h"

#include <QtCore/QDataStream>
#include <QtCore/QVariant>
#include <QtCore/QDebug>

struct DataVariableSubscriptionPrivate
{
    DataVariableIdentifier mIdentifier;

    bool mIsSubscribed;

    DataVariableSubscriptionPrivate()
        : mIdentifier(), mIsSubscribed(false)
    {
    }

    explicit DataVariableSubscriptionPrivate(const DataVariableIdentifier &aIdentifier)
        : mIdentifier(aIdentifier), mIsSubscribed(false)
    {
    }

    DataVariableSubscriptionPrivate(const DataVariableIdentifier &aIdentifier, bool subscribedWithSuccess)
        : mIdentifier(aIdentifier), mIsSubscribed(subscribedWithSuccess)
    {
    }
};

DataVariableSubscription::DataVariableSubscription() : d(new DataVariableSubscriptionPrivate)
{
}

DataVariableSubscription::DataVariableSubscription(const DataVariableIdentifier &aIdentifier)
    : d(new DataVariableSubscriptionPrivate(aIdentifier))
{
}

DataVariableSubscription::DataVariableSubscription(const DataVariableIdentifier &aIdentifier, bool subscribedWithSuccess)
    : d(new DataVariableSubscriptionPrivate(aIdentifier, subscribedWithSuccess))
{
}

DataVariableSubscription::DataVariableSubscription(const DataVariableSubscription &aOther)
    : d(new DataVariableSubscriptionPrivate(*aOther.d))
{
}

DataVariableSubscription::DataVariableSubscription(DataVariableSubscription &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

DataVariableSubscription::~DataVariableSubscription()
{
    delete d;
}

DataVariableSubscription& DataVariableSubscription::operator=(const DataVariableSubscription &aOther)
{
    if (this != &aOther) {
        d->mIdentifier = aOther.d->mIdentifier;
        d->mIsSubscribed = aOther.d->mIsSubscribed;
    }

    return *this;
}

DataVariableSubscription& DataVariableSubscription::operator=(DataVariableSubscription &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void DataVariableSubscription::setIdentifier(const DataVariableIdentifier &stringIdentifier)
{
    d->mIdentifier = stringIdentifier;
}

const DataVariableIdentifier &DataVariableSubscription::identifier() const
{
    return d->mIdentifier;
}

DataVariableIdentifier &DataVariableSubscription::identifier()
{
    return d->mIdentifier;
}

void DataVariableSubscription::setSubscribed(bool value)
{
    d->mIsSubscribed = value;
}

bool DataVariableSubscription::isSubscribed() const
{
    return d->mIsSubscribed;
}

bool& DataVariableSubscription::isSubscribed()
{
    return d->mIsSubscribed;
}

bool DataVariableSubscription::operator<(const DataVariableSubscription &aOther) const
{
    return d->mIdentifier < aOther.d->mIdentifier || (d->mIdentifier == aOther.d->mIdentifier && d->mIsSubscribed < aOther.d->mIsSubscribed);
}

bool DataVariableSubscription::operator==(const DataVariableSubscription &aOther) const
{
    return d->mIdentifier == aOther.d->mIdentifier && d->mIsSubscribed == aOther.d->mIsSubscribed;
}

QDataStream& operator <<(QDataStream &aStream, const DataVariableSubscription &aVariable)
{
    aStream << aVariable.identifier() << aVariable.isSubscribed();

    return aStream;
}

QDataStream& operator >>(QDataStream &aStream, DataVariableSubscription &aVariable)
{
    aStream >> aVariable.identifier() >> aVariable.isSubscribed();

    return aStream;
}


QDebug operator<<(QDebug dbg, const DataVariableSubscription &variable)
{
    dbg << variable.identifier() << (variable.isSubscribed() ? " is subscribed" : " is not subscribed");
    return dbg;
}
