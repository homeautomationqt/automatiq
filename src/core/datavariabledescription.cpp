/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "datavariabledescription.h"

#include "uniqueidentifier.h"

#include <QtCore/QDataStream>
#include <QtCore/QDebug>

struct DataVariableDescriptionPrivate
{
    QString mSourceObjectName;

    QString mSourceVariableName;

    DataVariableDescriptionPrivate()
        : mSourceObjectName(), mSourceVariableName()
    {
    }

    DataVariableDescriptionPrivate(const QString &objectName, const QString &variableName)
        : mSourceObjectName(objectName), mSourceVariableName(variableName)
    {
    }
};

DataVariableDescription::DataVariableDescription() : d(new DataVariableDescriptionPrivate)
{
}

DataVariableDescription::DataVariableDescription(const QString &objectName, const QString &variableName)
    : d(new DataVariableDescriptionPrivate(objectName, variableName))
{
}

DataVariableDescription::DataVariableDescription(const DataVariableDescription &aOther)
    : d(new DataVariableDescriptionPrivate(*aOther.d))
{
}

DataVariableDescription::DataVariableDescription(DataVariableDescription &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

DataVariableDescription::~DataVariableDescription()
{
    delete d;
}

DataVariableDescription& DataVariableDescription::operator=(const DataVariableDescription &aOther)
{
    if (this != &aOther) {
        d->mSourceObjectName = aOther.d->mSourceObjectName;
        d->mSourceVariableName = aOther.d->mSourceVariableName;
    }

    return *this;
}

DataVariableDescription& DataVariableDescription::operator=(DataVariableDescription &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

DataVariableIdentifier DataVariableDescription::toIdentifier() const
{
    return DataVariableIdentifier(d->mSourceObjectName, d->mSourceVariableName);
}

void DataVariableDescription::setSourceObjectName(const QString &name)
{
    d->mSourceObjectName = name;
}

const QString &DataVariableDescription::sourceObjectName() const
{
    return d->mSourceObjectName;
}

QString &DataVariableDescription::sourceObjectName()
{
    return d->mSourceObjectName;
}

void DataVariableDescription::setSourceVariableName(const QString &name)
{
    d->mSourceVariableName = name;
}

const QString &DataVariableDescription::sourceVariableName() const
{
    return d->mSourceVariableName;
}

QString &DataVariableDescription::sourceVariableName()
{
    return d->mSourceVariableName;
}

bool DataVariableDescription::operator==(const DataVariableDescription &aOther) const
{
    return d->mSourceObjectName == aOther.d->mSourceObjectName && d->mSourceVariableName == aOther.d->mSourceVariableName;
}

QDataStream& operator <<(QDataStream &aStream, const DataVariableDescription &aVariable)
{
    aStream << aVariable.sourceObjectName() << aVariable.sourceVariableName();

    return aStream;
}

QDataStream& operator >>(QDataStream &aStream, DataVariableDescription &aVariable)
{
    aStream >> aVariable.sourceObjectName() >> aVariable.sourceVariableName();

    return aStream;
}


QDebug operator<<(QDebug dbg, const DataVariableDescription &variable)
{
    dbg << variable.sourceObjectName() << ":" << variable.sourceVariableName();
    return dbg;
}
