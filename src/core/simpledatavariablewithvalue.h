/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_SIMPLE_DATA_VARIABLE_WITH_VALUE_H
#define AUTOMATIQ_SIMPLE_DATA_VARIABLE_WITH_VALUE_H

#include "automatiqcore_export.h"

#include "datavariableidentifier.h"
#include "datavariablewithvalue.h"
#include "simpledatavariableidentifier.h"

#include <QtCore/QtGlobal>

template <typename T>
class AUTOMATIQCORE_EXPORT SimpleDataVariableWithValue
{
public:

    SimpleDataVariableWithValue()
        : mIdentifier(0), mData()
    {
    }

    SimpleDataVariableWithValue(const DataVariableIdentifier &aIdentifier, const QVariant &aValue)
        : mIdentifier(aIdentifier), mData(aValue.value<T>())
    {
    }

    SimpleDataVariableWithValue(const DataVariableIdentifier &aIdentifier, const T &aValue)
        : mIdentifier(aIdentifier), mData(aValue)
    {
    }

    SimpleDataVariableWithValue(const SimpleDataVariableIdentifier &aIdentifier, const QVariant &aValue)
        : mIdentifier(aIdentifier), mData(aValue.value<T>())
    {
    }

    SimpleDataVariableWithValue(const SimpleDataVariableIdentifier &aIdentifier, const T &aValue)
        : mIdentifier(aIdentifier), mData(aValue)
    {
    }

    explicit SimpleDataVariableWithValue(const QString &sourceObjectName, const QString &sourceVariableName)
        : mIdentifier(qHash(sourceObjectName + sourceVariableName))
    {
    }

    void setIdentifier(quint64 identifier)
    {
        mIdentifier = identifier;
    }

    quint64 identifier() const
    {
        return mIdentifier;
    }

    quint64& identifier()
    {
        return mIdentifier;
    }

    void setValue(const QVariant &newValue)
    {
        mData;
    }

    const QVariant& value() const
    {
        return mData;
    }

    QVariant& value()
    {
        return mData;
    }

    bool operator==(const DataVariableWithValue &aOther) const
    {
        return mIdentifier == aOther.identifier().identifier() && mData == aOther.value().value<T>();
    }

private:

    quint64 mIdentifier;

    T mData;
};

#endif // SIMPLEDATAVARIABLEWITHVALUE_H
