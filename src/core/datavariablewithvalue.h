/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_DATA_VARIABLE_WITH_VALUE_H
#define AUTOMATIQ_DATA_VARIABLE_WITH_VALUE_H

#include "automatiqcore_export.h"

class QDataStream;
class QVariant;
class QDebug;

class DataVariableWithValuePrivate;
class DataVariableIdentifier;

class AUTOMATIQCORE_EXPORT DataVariableWithValue
{
public:
    DataVariableWithValue();

    DataVariableWithValue(const DataVariableIdentifier &aIdentifier, const QVariant &aValue);

    DataVariableWithValue(const DataVariableWithValue &aOther);

    DataVariableWithValue(DataVariableWithValue &&aOther);

    ~DataVariableWithValue();

    DataVariableWithValue& operator=(const DataVariableWithValue &aOther);

    DataVariableWithValue& operator=(DataVariableWithValue &&aOther);

    void setIdentifier(const DataVariableIdentifier &id);

    const DataVariableIdentifier& identifier() const;

    DataVariableIdentifier& identifier();

    void setValue(const QVariant &newValue);

    const QVariant& value() const;

    QVariant& value();

    bool operator==(const DataVariableWithValue &aOther) const;

private:

    DataVariableWithValuePrivate *d;
};

QDataStream& operator<<(QDataStream &aStream, const DataVariableWithValue &aVariable) AUTOMATIQCORE_EXPORT;

QDataStream& operator>>(QDataStream &aStream, DataVariableWithValue &aVariable) AUTOMATIQCORE_EXPORT;

QDebug operator<<(QDebug dbg, const DataVariableWithValue &variable) AUTOMATIQCORE_EXPORT;

#endif
