#include "subscribedvariablerecord.h"

#include <QtCore/QVariant>
#include <QtCore/QString>
#include <QtCore/QDebug>

class SubscribedVariableRecordPrivate
{
public:

    SubscribedVariableRecordPrivate()
        : mSourcePeerId(Automatiq::INVALID_PEER_ID), mSourceObjectName(), mSourceVariableName(), mValue(),
          mIsSubscribed(false), mAutoSubscribe(false)
    {
    }

    explicit SubscribedVariableRecordPrivate(const DataVariableDescription &variableDescription)
        : mSourcePeerId(Automatiq::INVALID_PEER_ID), mSourceObjectName(variableDescription.sourceObjectName()),
          mSourceVariableName(variableDescription.sourceVariableName()), mValue(), mIsSubscribed(false),
          mAutoSubscribe(false)
    {
    }

    Automatiq::peerId mSourcePeerId;
    QString mSourceObjectName;
    QString mSourceVariableName;
    QVariant mValue;
    bool mIsSubscribed;
    bool mAutoSubscribe;
};

SubscribedVariableRecord::SubscribedVariableRecord(QObject *parent)
    : QObject(parent), d(new SubscribedVariableRecordPrivate)
{
}

SubscribedVariableRecord::SubscribedVariableRecord(const DataVariableDescription &variableDescription, QObject *parent)
    : QObject(parent), d(new SubscribedVariableRecordPrivate(variableDescription))
{
}

SubscribedVariableRecord::~SubscribedVariableRecord()
{
}

void SubscribedVariableRecord::setSourcePeerId(Automatiq::peerId peerId)
{
    d->mSourcePeerId = peerId;
    Q_EMIT peerIdChanged(d->mSourcePeerId);
}

Automatiq::peerId SubscribedVariableRecord::sourcePeerId() const
{
    return d->mSourcePeerId;
}

const QString &SubscribedVariableRecord::sourceObjectName() const
{
    return d->mSourceObjectName;
}

const QString &SubscribedVariableRecord::sourceVariableName() const
{
    return d->mSourceVariableName;
}

void SubscribedVariableRecord::setValue(const QVariant &variableValue)
{
    d->mValue = variableValue;
    Q_EMIT valueChanged(d->mSourceObjectName, d->mSourceVariableName, d->mValue);
}

const QVariant &SubscribedVariableRecord::value() const
{
    return d->mValue;
}

bool SubscribedVariableRecord::isSubscribed() const
{
    return d->mIsSubscribed;
}

void SubscribedVariableRecord::setAutoSubscribe(bool value)
{
    d->mAutoSubscribe = value;
}

bool SubscribedVariableRecord::autoSubscribe() const
{
    return d->mAutoSubscribe;
}

void SubscribedVariableRecord::subscribe()
{
    d->mIsSubscribed = true;
    Q_EMIT subscriptionChanged(true);
}

void SubscribedVariableRecord::unsubscribe()
{
    d->mIsSubscribed = false;
    Q_EMIT subscriptionChanged(false);
}

DataVariableDescription SubscribedVariableRecord::toVariableDescription() const
{
    return DataVariableDescription(d->mSourceObjectName, d->mSourceVariableName);
}

DataVariableIdentifier SubscribedVariableRecord::toVariableIdentifier() const
{
    return DataVariableIdentifier(d->mSourceObjectName, d->mSourceVariableName);
}

#include "moc_subscribedvariablerecord.cpp"
