#include "simpledatavariableidentifier.h"

#include <QtCore/QHash>

uint qHash(SimpleDataVariableIdentifier t, uint seed)
{
    return qHash(t.identifier(), seed);
}
