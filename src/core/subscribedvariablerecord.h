/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_SUBSCRIBED_VARIABLE_RECORD_H
#define AUTOMATIQ_SUBSCRIBED_VARIABLE_RECORD_H

#include "automatiqcore_export.h"

#include "datavariabledescription.h"
#include "datavariableidentifier.h"

#include "commondeclarations.h"

#include <QtCore/QString>
#include <QtCore/QVariant>
#include <QtCore/QObject>

class SubscribedVariableRecordPrivate;

class AUTOMATIQCORE_EXPORT SubscribedVariableRecord : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Automatiq::peerId sourcePeerId
               READ sourcePeerId
               WRITE setSourcePeerId
               NOTIFY peerIdChanged)

    Q_PROPERTY(QString sourceObjectName
               READ sourceObjectName)

    Q_PROPERTY(QString sourceVariableName
               READ sourceVariableName)

    Q_PROPERTY(QVariant value
               READ value
               WRITE setValue
               NOTIFY valueChanged)

    Q_PROPERTY(bool isSubscribed
               READ isSubscribed
               NOTIFY subscriptionChanged)

    Q_PROPERTY(bool autoSubscribe
               READ autoSubscribe
               WRITE setAutoSubscribe)

public:

    explicit SubscribedVariableRecord(QObject *parent = 0);

    explicit SubscribedVariableRecord(const DataVariableDescription &variableDescription, QObject *parent = 0);

    virtual ~SubscribedVariableRecord();

    void setSourcePeerId(Automatiq::peerId peerId);

    Automatiq::peerId sourcePeerId() const;

    const QString& sourceObjectName() const;

    const QString& sourceVariableName() const;

    void setValue(const QVariant &variableValue);

    const QVariant& value() const;

    bool isSubscribed() const;

    void setAutoSubscribe(bool value);

    bool autoSubscribe() const;

    void subscribe();

    void unsubscribe();

    DataVariableDescription toVariableDescription() const;

    DataVariableIdentifier toVariableIdentifier() const;

Q_SIGNALS:

    void valueChanged(const QString &sourceObjectName, const QString &sourceVariableName, const QVariant &value);

    void subscriptionChanged(bool isSubscribed);

    void peerIdChanged(Automatiq::peerId peerId);

private:

    SubscribedVariableRecordPrivate *d;
};

#endif
