/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef AUTOMATIQ_COMMON_DECLARATIONS_H
#define AUTOMATIQ_COMMON_DECLARATIONS_H

#include <QtCore/QtGlobal>
#include <QtCore/QMap>
#include <QtCore/QVariant>

namespace Automatiq
{

enum class ERROR_TYPE
{
    Invalid = 0,
    UnexpectedMessage = 1,
};

typedef int peerId;

static const peerId INVALID_PEER_ID = -1;

/**
 * @brief The DataProcessingSteps enum allows to know the state of the data processing loop
 *
 * @value Waiting Between two steps there is a delay to wait before syncing again.
 * @value BeforeSynchronization The delay has elapsed and we will perform a synchronization before exchanging data.
 * @value SendingData We are sending data to remote peers.
 * @value AfterSendingDataSynchronization There is a sync point after data have been exchanged.
 * @value ProcessingData We are processing the newly received data.
 * @value AfterProcessingDataSynchronization We sync everybody after the data have been processed. After this step, we will sleep until next step.
 * @value Invalid Special value to detect invalid synchronization step.
 */
enum class DataProcessingSteps
{
    Waiting,
    BeforeSynchronization,
    SendingData,
    AfterSendingDataSynchronization,
    ProcessingData,
    AfterProcessingDataSynchronization,
    Invalid,
};

typedef QMap<QString, QVariant> configuration_type;

}

#endif
