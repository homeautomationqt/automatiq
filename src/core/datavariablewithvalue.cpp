/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "datavariablewithvalue.h"

#include "datavariableidentifier.h"

#include <QtCore/QDataStream>
#include <QtCore/QVariant>
#include <QtCore/QDebug>

struct DataVariableWithValuePrivate
{
    DataVariableIdentifier mIdentifier;

    QVariant mValue;

    DataVariableWithValuePrivate()
        : mIdentifier(), mValue()
    {
    }

    DataVariableWithValuePrivate(const DataVariableIdentifier &aIdentifier, const QVariant &aValue)
        : mIdentifier(aIdentifier), mValue(aValue)
    {
    }
};

DataVariableWithValue::DataVariableWithValue() : d(new DataVariableWithValuePrivate)
{
}

DataVariableWithValue::DataVariableWithValue(const DataVariableIdentifier &aIdentifier, const QVariant &aValue)
    : d(new DataVariableWithValuePrivate(aIdentifier, aValue))
{
}

DataVariableWithValue::DataVariableWithValue(const DataVariableWithValue &aOther)
    : d(new DataVariableWithValuePrivate(*aOther.d))
{
}

DataVariableWithValue::DataVariableWithValue(DataVariableWithValue &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

DataVariableWithValue::~DataVariableWithValue()
{
    delete d;
}

DataVariableWithValue& DataVariableWithValue::operator=(const DataVariableWithValue &aOther)
{
    if (this != &aOther) {
        d->mIdentifier = aOther.d->mIdentifier;
        d->mValue = aOther.d->mValue;
    }

    return *this;
}

DataVariableWithValue& DataVariableWithValue::operator=(DataVariableWithValue &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void DataVariableWithValue::setIdentifier(const DataVariableIdentifier &id)
{
    d->mIdentifier = id;
}

const DataVariableIdentifier& DataVariableWithValue::identifier() const
{
    return d->mIdentifier;
}

DataVariableIdentifier& DataVariableWithValue::identifier()
{
    return d->mIdentifier;
}

void DataVariableWithValue::setValue(const QVariant &newValue)
{
    d->mValue = newValue;
}

const QVariant& DataVariableWithValue::value() const
{
    return d->mValue;
}

QVariant& DataVariableWithValue::value()
{
    return d->mValue;
}

bool DataVariableWithValue::operator==(const DataVariableWithValue &aOther) const
{
    return d->mIdentifier == aOther.d->mIdentifier && d->mValue == aOther.d->mValue;
}

QDataStream& operator <<(QDataStream &aStream, const DataVariableWithValue &aVariable)
{
    aStream << aVariable.identifier() << aVariable.value();

    return aStream;
}

QDataStream& operator >>(QDataStream &aStream, DataVariableWithValue &aVariable)
{
    aStream >> aVariable.identifier() >> aVariable.value();

    return aStream;
}

QDebug operator<<(QDebug dbg, const DataVariableWithValue &variable)
{
    dbg << variable.identifier() << "==" << variable.value();
    return dbg;
}
