/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef AUTOMATIQ_UNIQUE_IDENTIFIER_H
#define AUTOMATIQ_UNIQUE_IDENTIFIER_H

#include <QtCore/QtGlobal>
#include <QtCore/QString>

namespace Automatiq
{

constexpr quint64 Hash(const char *aText, std::size_t aSize)
{
    return aSize == 0 ? 0xcbf29ce484222325 : (aText[0] ^ Hash(aText + 1, aSize - 1)) * 0x100000001b3;
}

inline quint64 Hash(const QStringRef &aText)
{
    return aText.size() == 0 ? 0xcbf29ce484222325 : (aText.at(0).unicode() ^ Hash(aText.size() == 1 ? QStringRef() : aText.right(aText.size() - 1))) * 0x100000001b3;
}

inline quint64 Hash(const QString &aText)
{
    return aText.size() == 0 ? 0xcbf29ce484222325 : (aText.at(0).unicode() ^ Hash(aText.size() == 1 ? QStringRef() : aText.rightRef(aText.size() - 1))) * 0x100000001b3;
}

}

constexpr quint64 operator "" _uniqueId(const char *aText, std::size_t aSize)
{
    return Automatiq::Hash(aText, aSize);
}

#endif
