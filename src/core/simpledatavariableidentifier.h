/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_SIMPLE_DATA_VARIABLE_IDENTIFIER_H
#define AUTOMATIQ_SIMPLE_DATA_VARIABLE_IDENTIFIER_H

#include "automatiqcore_export.h"

#include "datavariableidentifier.h"

#include <QtCore/QString>
#include <QtCore/QHash>
#include <QtCore/QtGlobal>

class AUTOMATIQCORE_EXPORT SimpleDataVariableIdentifier
{
public:
    SimpleDataVariableIdentifier()
        : mIdentifier(0)
    {
    }

    explicit SimpleDataVariableIdentifier(const DataVariableIdentifier &id)
        : mIdentifier(id.identifier())
    {
    }

    SimpleDataVariableIdentifier(const QString &sourceObjectName, const QString &sourceVariableName)
        : mIdentifier(qHash(sourceObjectName + sourceVariableName))
    {
    }

    void setIdentifier(quint64 identifier)
    {
        mIdentifier = identifier;
    }

    quint64 identifier() const
    {
        return mIdentifier;
    }

    quint64& identifier()
    {
        return mIdentifier;
    }

    bool operator<(const SimpleDataVariableIdentifier &aOther) const
    {
        return mIdentifier < aOther.mIdentifier;
    }

    bool operator==(const SimpleDataVariableIdentifier &aOther) const
    {
        return mIdentifier == aOther.mIdentifier;
    }

private:

    quint64 mIdentifier;
};

uint AUTOMATIQCORE_EXPORT qHash(SimpleDataVariableIdentifier t, uint seed = 0);

#endif
