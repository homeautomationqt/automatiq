/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "datavariablesubscriptionrequest.h"

#include "datavariableidentifier.h"

#include <QtCore/QDataStream>
#include <QtCore/QVariant>
#include <QtCore/QDebug>

struct DataVariableSubscriptionRequestPrivate
{
    DataVariableIdentifier mIdentifier;

    DataVariableSubscriptionRequestType mSubscriptionRequestType;

    DataVariableSubscriptionRequestPrivate()
        : mIdentifier(), mSubscriptionRequestType()
    {
    }

    explicit DataVariableSubscriptionRequestPrivate(const DataVariableIdentifier &aIdentifier)
        : mIdentifier(aIdentifier), mSubscriptionRequestType()
    {
    }

    explicit DataVariableSubscriptionRequestPrivate(const DataVariableIdentifier &aIdentifier, DataVariableSubscriptionRequestType requestType)
        : mIdentifier(aIdentifier), mSubscriptionRequestType(requestType)
    {
    }
};

DataVariableSubscriptionRequest::DataVariableSubscriptionRequest() : d(new DataVariableSubscriptionRequestPrivate)
{
}

DataVariableSubscriptionRequest::DataVariableSubscriptionRequest(const DataVariableIdentifier &aIdentifier, DataVariableSubscriptionRequestType requestType)
    : d(new DataVariableSubscriptionRequestPrivate(aIdentifier, requestType))
{
}

DataVariableSubscriptionRequest::DataVariableSubscriptionRequest(const DataVariableSubscriptionRequest &aOther)
    : d(new DataVariableSubscriptionRequestPrivate(*aOther.d))
{
}

DataVariableSubscriptionRequest::DataVariableSubscriptionRequest(DataVariableSubscriptionRequest &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

DataVariableSubscriptionRequest::~DataVariableSubscriptionRequest()
{
    delete d;
}

DataVariableSubscriptionRequest& DataVariableSubscriptionRequest::operator=(const DataVariableSubscriptionRequest &aOther)
{
    if (this != &aOther) {
        d->mIdentifier = aOther.d->mIdentifier;
        d->mSubscriptionRequestType = aOther.d->mSubscriptionRequestType;
    }

    return *this;
}

DataVariableSubscriptionRequest& DataVariableSubscriptionRequest::operator=(DataVariableSubscriptionRequest &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void DataVariableSubscriptionRequest::setIdentifier(const DataVariableIdentifier &stringIdentifier)
{
    d->mIdentifier = stringIdentifier;
}

const DataVariableIdentifier &DataVariableSubscriptionRequest::identifier() const
{
    return d->mIdentifier;
}

DataVariableIdentifier &DataVariableSubscriptionRequest::identifier()
{
    return d->mIdentifier;
}

void DataVariableSubscriptionRequest::setSubscriptionRequestType(DataVariableSubscriptionRequestType requestType)
{
    d->mSubscriptionRequestType = requestType;
}

DataVariableSubscriptionRequestType DataVariableSubscriptionRequest::subscriptionRequestType() const
{
    return d->mSubscriptionRequestType;
}

DataVariableSubscriptionRequestType& DataVariableSubscriptionRequest::subscriptionRequestType()
{
    return d->mSubscriptionRequestType;
}

bool DataVariableSubscriptionRequest::operator<(const DataVariableSubscriptionRequest &aOther) const
{
    return d->mIdentifier < aOther.d->mIdentifier || (d->mIdentifier == aOther.d->mIdentifier && d->mSubscriptionRequestType < aOther.d->mSubscriptionRequestType);
}

bool DataVariableSubscriptionRequest::operator==(const DataVariableSubscriptionRequest &aOther) const
{
    return d->mIdentifier == aOther.d->mIdentifier && d->mSubscriptionRequestType == aOther.d->mSubscriptionRequestType;
}

QDataStream& operator <<(QDataStream &aStream, const DataVariableSubscriptionRequest &aVariable)
{
    aStream << aVariable.identifier() << qint32(aVariable.subscriptionRequestType());

    return aStream;
}

QDataStream& operator >>(QDataStream &aStream, DataVariableSubscriptionRequest &aVariable)
{
    qint32 tempSubscriptionRequestType;
    aStream >> aVariable.identifier() >> tempSubscriptionRequestType;
    aVariable.setSubscriptionRequestType(static_cast<DataVariableSubscriptionRequestType>(tempSubscriptionRequestType));

    return aStream;
}


QDebug operator<<(QDebug dbg, const DataVariableSubscriptionRequest &variable)
{
    dbg << variable.identifier() << (variable.subscriptionRequestType() == DataVariableSubscriptionRequestType::Subscribe ? " subscribe request" : " unsubscribe request");
    return dbg;
}
