/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_DATA_VARIABLE_IDENTIFIER_H
#define AUTOMATIQ_DATA_VARIABLE_IDENTIFIER_H

#include "automatiqcore_export.h"

#include "commondeclarations.h"

#include <QtCore/QString>

class QDataStream;
class DataVariableIdentifierPrivate;
class QDebug;

class AUTOMATIQCORE_EXPORT DataVariableIdentifier
{
public:
    DataVariableIdentifier();

    explicit DataVariableIdentifier(quint64 aVariableIdentifier);

    explicit DataVariableIdentifier(const QString &sourceObjectName, const QString &sourceVariableName);

    DataVariableIdentifier(const DataVariableIdentifier &aOther);

    DataVariableIdentifier(DataVariableIdentifier &&aOther);

    ~DataVariableIdentifier();

    DataVariableIdentifier& operator=(const DataVariableIdentifier &aOther);

    DataVariableIdentifier& operator=(DataVariableIdentifier &&aOther);

    void setIdentifier(quint64 identifier);

    quint64 identifier() const;

    quint64& identifier();

    bool operator<(const DataVariableIdentifier &aOther) const;

    bool operator==(const DataVariableIdentifier &aOther) const;

private:

    DataVariableIdentifierPrivate *d;
};

QDataStream& operator<<(QDataStream &aStream, const DataVariableIdentifier &aVariable);

QDataStream& operator>>(QDataStream &aStream, DataVariableIdentifier &aVariable);

QDebug operator<<(QDebug dbg, const DataVariableIdentifier &variable);

#endif
