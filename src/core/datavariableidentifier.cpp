/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "datavariableidentifier.h"

#include "uniqueidentifier.h"

#include <QtCore/QDataStream>
#include <QtCore/QDebug>
#include <QtCore/QHash>

struct DataVariableIdentifierPrivate
{
    quint64 mIdentifier;

    DataVariableIdentifierPrivate()
        : mIdentifier(0)
    {
    }

    DataVariableIdentifierPrivate(const quint64 &aIdentifier)
        : mIdentifier(aIdentifier)
    {
    }
};

DataVariableIdentifier::DataVariableIdentifier() : d(new DataVariableIdentifierPrivate)
{
}

DataVariableIdentifier::DataVariableIdentifier(quint64 aVariableIdentifier)
    : d(new DataVariableIdentifierPrivate(aVariableIdentifier))
{
}

DataVariableIdentifier::DataVariableIdentifier(const QString &sourceObjectName, const QString &sourceVariableName)
    : d(new DataVariableIdentifierPrivate(qHash(sourceObjectName + sourceVariableName)))
{
}

DataVariableIdentifier::DataVariableIdentifier(const DataVariableIdentifier &aOther)
    : d(new DataVariableIdentifierPrivate(*aOther.d))
{
}

DataVariableIdentifier::DataVariableIdentifier(DataVariableIdentifier &&aOther)
    : d(aOther.d)
{
    aOther.d = nullptr;
}

DataVariableIdentifier::~DataVariableIdentifier()
{
    delete d;
}

DataVariableIdentifier& DataVariableIdentifier::operator=(const DataVariableIdentifier &aOther)
{
    if (this != &aOther) {
        d->mIdentifier = aOther.d->mIdentifier;
    }

    return *this;
}

DataVariableIdentifier& DataVariableIdentifier::operator=(DataVariableIdentifier &&aOther)
{
    if (this != &aOther) {
        qSwap(d, aOther.d);
    }

    return *this;
}

void DataVariableIdentifier::setIdentifier(quint64 identifier)
{
    d->mIdentifier = identifier;
}

quint64 DataVariableIdentifier::identifier() const
{
    return d->mIdentifier;
}

quint64& DataVariableIdentifier::identifier()
{
    return d->mIdentifier;
}

bool DataVariableIdentifier::operator<(const DataVariableIdentifier &aOther) const
{
    return d->mIdentifier < aOther.d->mIdentifier;
}

bool DataVariableIdentifier::operator==(const DataVariableIdentifier &aOther) const
{
    return d->mIdentifier == aOther.d->mIdentifier;
}

QDataStream& operator <<(QDataStream &aStream, const DataVariableIdentifier &aVariable)
{
    aStream << aVariable.identifier();

    return aStream;
}

QDataStream& operator >>(QDataStream &aStream, DataVariableIdentifier &aVariable)
{
    aStream >> aVariable.identifier();

    return aStream;
}


QDebug operator<<(QDebug dbg, const DataVariableIdentifier &variable)
{
    dbg << variable.identifier();
    return dbg;
}
