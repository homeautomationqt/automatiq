project(AutomatiqCore)

set(CMAKE_CXX_STANDARD 11)

set(automatiqCore_LIB_SRCS
  datavariabledescription.cpp
  datavariableidentifier.cpp
  datavariablewithvalue.cpp
  commondeclarations.h
  subscribedvariablerecord.cpp
  simpledatavariableidentifier.cpp
  simpledatavariablewithvalue.cpp
  datavariablesubscription.cpp
  datavariablesubscriptionrequest.cpp
  uniqueidentifier.h
)

ecm_generate_headers(automatiqCore_HEADERS
  HEADER_NAMES
  DataVariableIdentifier
  DataVariableWithValue
  CommonDeclarations
  SimpleDataVariableIdentifier
  SimpleDataVariableWithValue
  UniqueIdentifier

  REQUIRED_HEADERS automatiqCore_HEADERS
)

ecm_setup_version(0.1.0 VARIABLE_PREFIX AUTOMATIQ_CORE
                        VERSION_HEADER "${CMAKE_CURRENT_BINARY_DIR}/automatiqcore_version.h"
                        PACKAGE_VERSION_FILE "${CMAKE_CURRENT_BINARY_DIR}/AutomatiqCoreConfigVersion.cmake")

add_library(AutomatiqCore SHARED ${automatiqCore_LIB_SRCS})

generate_export_header(AutomatiqCore BASE_NAME AutomatiqCore
  EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/automatiqcore_export.h
)
add_library(Automatiq::Core ALIAS AutomatiqCore)

target_link_libraries(AutomatiqCore PUBLIC Qt5::Core)

target_include_directories(AutomatiqCore INTERFACE "$<INSTALL_INTERFACE:${INCLUDE_INSTALL_DIR}/Automatiq>")

set_target_properties(AutomatiqCore PROPERTIES VERSION   ${AUTOMATIQ_VERSION_STRING}
                                               SOVERSION ${AUTOMATIQ_SOVERSION}
                                               LINK_FLAGS "-Wl,--no-undefined")
 
install(TARGETS AutomatiqCore EXPORT AutomatiqTargets ${INSTALL_TARGETS_DEFAULT_ARGS})

install(FILES
  ${AutomatiqCore_BINARY_DIR}/automatiqcore_export.h
  ${automatiqCore_HEADERS}
  DESTINATION  ${INCLUDE_INSTALL_DIR}/Automatiq/automatiq/ COMPONENT Devel
)
