/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_DATA_VARIABLE_DESCRIPTION_H
#define AUTOMATIQ_DATA_VARIABLE_DESCRIPTION_H

#include "automatiqcore_export.h"

#include "datavariableidentifier.h"

#include "commondeclarations.h"

#include <QtCore/QString>

class QDataStream;
class DataVariableDescriptionPrivate;
class QDebug;

class AUTOMATIQCORE_EXPORT DataVariableDescription
{
public:
    DataVariableDescription();

    DataVariableDescription(const QString &sourceObjectName, const QString &sourceVariableName);

    DataVariableDescription(const DataVariableDescription &aOther);

    DataVariableDescription(DataVariableDescription &&aOther);

    ~DataVariableDescription();

    DataVariableDescription& operator=(const DataVariableDescription &aOther);

    DataVariableDescription& operator=(DataVariableDescription &&aOther);

    DataVariableIdentifier toIdentifier() const;

    void setSourceObjectName(const QString &name);

    const QString& sourceObjectName() const;

    QString& sourceObjectName();

    void setSourceVariableName(const QString &name);

    const QString& sourceVariableName() const;

    QString& sourceVariableName();

    bool operator<(const DataVariableDescription &aOther) const;

    bool operator==(const DataVariableDescription &aOther) const;

private:

    DataVariableDescriptionPrivate *d;
};

QDataStream& operator<<(QDataStream &aStream, const DataVariableDescription &aVariable) AUTOMATIQCORE_EXPORT;

QDataStream& operator>>(QDataStream &aStream, DataVariableDescription &aVariable) AUTOMATIQCORE_EXPORT;

QDebug operator<<(QDebug dbg, const DataVariableDescription &variable) AUTOMATIQCORE_EXPORT;

#endif
