/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_DATA_VARIABLE_SUBSCRIPTION_H
#define AUTOMATIQ_DATA_VARIABLE_SUBSCRIPTION_H

#include "automatiqcore_export.h"

#include <QtCore/QString>

class QDataStream;
class QVariant;
class QDebug;

class DataVariableSubscriptionPrivate;
class DataVariableIdentifier;

class AUTOMATIQCORE_EXPORT DataVariableSubscription
{
public:
    DataVariableSubscription();

    explicit DataVariableSubscription(const DataVariableIdentifier &aIdentifier);

    DataVariableSubscription(const DataVariableIdentifier &aIdentifier, bool subscribedWithSuccess);

    DataVariableSubscription(const DataVariableSubscription &aOther);

    DataVariableSubscription(DataVariableSubscription &&aOther);

    ~DataVariableSubscription();

    DataVariableSubscription& operator=(const DataVariableSubscription &aOther);

    DataVariableSubscription& operator=(DataVariableSubscription &&aOther);

    void setIdentifier(const DataVariableIdentifier &stringIdentifier);

    const DataVariableIdentifier& identifier() const;

    DataVariableIdentifier& identifier();

    void setSubscribed(bool value);

    bool isSubscribed() const;

    bool& isSubscribed();

    bool operator<(const DataVariableSubscription &aOther) const;

    bool operator==(const DataVariableSubscription &aOther) const;

private:

    DataVariableSubscriptionPrivate *d;
};

QDataStream& operator<<(QDataStream &aStream, const DataVariableSubscription &aVariable) AUTOMATIQCORE_EXPORT;

QDataStream& operator>>(QDataStream &aStream, DataVariableSubscription &aVariable) AUTOMATIQCORE_EXPORT;

QDebug operator<<(QDebug dbg, const DataVariableSubscription &variable) AUTOMATIQCORE_EXPORT;

#endif
