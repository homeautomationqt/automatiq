/*
 * Copyright 2014 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined AUTOMATIQ_DATA_VARIABLE_SUBSCRIPTION_REQUEST_H
#define AUTOMATIQ_DATA_VARIABLE_SUBSCRIPTION_REQUEST_H

#include "automatiqcore_export.h"

#include <QtCore/QString>

class QDataStream;
class QVariant;
class QDebug;

class DataVariableSubscriptionRequestPrivate;
class DataVariableIdentifier;

enum class DataVariableSubscriptionRequestType
{
    Subscribe,
    Unsubscribe,
};

class AUTOMATIQCORE_EXPORT DataVariableSubscriptionRequest
{
public:

    DataVariableSubscriptionRequest();

    DataVariableSubscriptionRequest(const DataVariableIdentifier &aIdentifier, DataVariableSubscriptionRequestType requestType);

    DataVariableSubscriptionRequest(const DataVariableSubscriptionRequest &aOther);

    DataVariableSubscriptionRequest(DataVariableSubscriptionRequest &&aOther);

    ~DataVariableSubscriptionRequest();

    DataVariableSubscriptionRequest& operator=(const DataVariableSubscriptionRequest &aOther);

    DataVariableSubscriptionRequest& operator=(DataVariableSubscriptionRequest &&aOther);

    void setIdentifier(const DataVariableIdentifier &stringIdentifier);

    const DataVariableIdentifier& identifier() const;

    DataVariableIdentifier& identifier();

    void setSubscriptionRequestType(DataVariableSubscriptionRequestType requestType);

    DataVariableSubscriptionRequestType subscriptionRequestType() const;

    DataVariableSubscriptionRequestType& subscriptionRequestType();

    bool operator<(const DataVariableSubscriptionRequest &aOther) const;

    bool operator==(const DataVariableSubscriptionRequest &aOther) const;

private:

    DataVariableSubscriptionRequestPrivate *d;
};

QDataStream& operator<<(QDataStream &aStream, const DataVariableSubscriptionRequest &aVariable) AUTOMATIQCORE_EXPORT;

QDataStream& operator>>(QDataStream &aStream, DataVariableSubscriptionRequest &aVariable) AUTOMATIQCORE_EXPORT;

QDebug operator<<(QDebug dbg, const DataVariableSubscriptionRequest &variable) AUTOMATIQCORE_EXPORT;

#endif
